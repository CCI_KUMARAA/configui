﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.ServiceModel;
using CCI.MIP.JobConfig.APIService;




namespace WindowsServiceHost
{
    public partial class WinServiceHost : ServiceBase
    {
        public  static System.ServiceModel.ServiceHost serviceHost = null;
        public WinServiceHost()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                if (serviceHost != null)
                {
                    serviceHost.Close();
                }
                serviceHost = new System.ServiceModel.ServiceHost(typeof(JobConfigService));
                serviceHost.Open();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected override void OnStop()
        {
            if (serviceHost != null)
            {
                serviceHost.Close();
                serviceHost = null;
            } 
        }
    }
}
