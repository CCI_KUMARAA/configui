﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Data;

namespace CCI.MIP.JobConfig.APIService
{
    [ServiceContract]
    public interface IJobConfigService
    {
        #region Mailing

        [OperationContract]
        DataSet GetJobMailGroup(JobConfigPlugin objJobConfig);
        [OperationContract]
        int InsertJobMailGroup(JobConfigPlugin objJobConfig);
        [OperationContract]
        int UpdateJobMailGroup(JobConfigPlugin objJobConfig);
        [OperationContract]
        int UpdateJobMailingGroup(JobConfigPlugin objJobConfig);
        [OperationContract]
        DataSet GetMailingList(JobConfigPlugin objJobConfig);
        [OperationContract]
        DataSet GetMailingListWithJobId(JobConfigPlugin objJobConfig);
        [OperationContract]
        int InsertMailingList(JobConfigPlugin objJobConfig);
        [OperationContract]
        int UpdateMailingList(JobConfigPlugin objJobConfig);
        [OperationContract]
        int CheckMailGroup(JobConfigPlugin objJobConfig);
        [OperationContract]
        int CheckMailGroupWithJobId(JobConfigPlugin objJobConfig);
        [OperationContract]
        DataSet GetMailingNameWithStatus(JobConfigPlugin objJobConfig);
        #endregion
        #region JobPlugin
        [OperationContract]
        DataSet GetJobMaster();
        [OperationContract]
        DataSet GetJobMasterWithDateTime();
        [OperationContract]
        int InsertJobMaster(JobConfigPlugin objJobConfig);
        [OperationContract]
        int UpdateJobMaster(JobConfigPlugin objJobConfig);

        [OperationContract]
        int InsertJobSchedule(JobConfigPlugin objJobCofig);
        [OperationContract]
        int UpdateJobSchedule(JobConfigPlugin objJobConfig);
        [OperationContract]
        DataSet GetCalendar();
        [OperationContract]
        int GetJobId(JobConfigPlugin objJobConfig);
        [OperationContract]
        DataSet GetJobStatus(JobConfigPlugin objJobConfig);
        [OperationContract]
        DataSet GetJobLog(JobConfigPlugin objJobConfig);
        [OperationContract]
        DataSet GetJobPluginConfigWithPlugin(JobConfigPlugin objJobConfig);
        [OperationContract]
        int InsertJobPluginConfig(JobConfigPlugin objJobConfig);
        [OperationContract]
        int UpdateJobPluginConfig(JobConfigPlugin objJobConfig);
        [OperationContract]
        DataSet GetJobPluginConfigWithJobId(JobConfigPlugin objJobConfig);
        [OperationContract]
        DataSet GetJobPluginProp(JobConfigPlugin objJobConfig);
        [OperationContract]
        DataSet GetJobUpdatewitName(JobConfigPlugin objJobConfig);
        [OperationContract]
        DataSet GetJobPluginPropWitPluginId(JobConfigPlugin objJobConfig);
        [OperationContract]
        int GetConfigReposGUIDVersion(JobConfigPlugin objJobConfig);
        [OperationContract]
        int InsertJobPluginProp(JobConfigPlugin objJobConfig);
        [OperationContract]
        int UpdateJobPluginProp(JobConfigPlugin objJobConfig);
        [OperationContract]
        int UpdateJobUpdate(JobConfigPlugin objJobConfig);
        [OperationContract]
        int InsertJobUpdate(JobConfigPlugin objJobConfig);
        [OperationContract]
        DataSet GetConfigReposGUID(JobConfigPlugin objJobConfig);
        [OperationContract]
        int InsertNewGUID(JobConfigPlugin objJobConfig);
        [OperationContract]
        int UpdateGUID(JobConfigPlugin objJobConfig);
        [OperationContract]
        int InsertJobStatus(JobConfigPlugin objJobConfig);

        [OperationContract]
        int DeleteJobPluginProp(JobConfigPlugin objJobConfig);

        [OperationContract]
        int DeleteJobUpdate(JobConfigPlugin objJobConfig);

        [OperationContract]
        DataSet GetTrigger(JobConfigPlugin objJobConfig);
        [OperationContract]
        int InsertTrigger(JobConfigPlugin objJobConfig);
        [OperationContract]
        int UpdateTrigger(JobConfigPlugin objJobConfig);
        [OperationContract]
        int DeleteTrigger(JobConfigPlugin objJobConfig);
        //rerun
        [OperationContract]
        int UpdateReRunBin(JobConfigPlugin objJobConfig);
        [OperationContract]
        int UpdateReRunWeb(JobConfigPlugin objJobConfig);
        //seriesdeatils
        [OperationContract]
        DataSet GetSeriesName(JobConfigPlugin objJobConfig);
        [OperationContract]
        int UpdateSeriesName(JobConfigPlugin objJobConfig);
        //product
        [OperationContract]
        DataSet GetProduct();
        [OperationContract]
        int InsertProduct(JobConfigPlugin objJobConfig);
        [OperationContract]
        int UpdateProduct(JobConfigPlugin objJobConfig);
        //Report
        [OperationContract]
        DataSet GetReport();
        [OperationContract]
        int InsertReport(JobConfigPlugin objJobConfig);
        [OperationContract]
        int UpdateReport(JobConfigPlugin objJobConfig);

        [OperationContract]
        DataSet GetSource();

        [OperationContract]
        int InsertSource(JobConfigPlugin objJobConfig);

        [OperationContract]
        int UpdateSource(JobConfigPlugin objJobConfig);

        #endregion
    }
    [DataContract]
 
    public class JobConfigPlugin
    {
        //jobschedule
        int jobId;
        string jobName;
        string jobdescription;
        string desk;
        int duration;
        int frequency;
        string calendar;
        DateTime starttime, endtime, lastUpdateDate;
        string active;
        //mailing
        string mailgroupname;
        string mailgroupstatus;
        int mailGrpId;
        string userMailId;
        int userId;
        //status
        int statusId;

        // plugin
        string plugin;
        int pluginId;
        int seqnum;
        string engineType;
        int pluginConfigId;
        string propName;
        string propValue;
        int propId;
        //job update
        int updateId;
      
        string jobLastUpdate;
        string description;
        string previousvalue;
        string configGUIDkey;
        string config;
        int configId;
        string configGUIDKeyVersion;
        //Trigger
        int trigger_Job_Id;
        string status;
        int trig_Id;
        //seriesdetails
        string series_name;
        int series_key;

        string product_name;
        string product_description;
        string product_group;
        int product_key;
        string display_product;

            string report_name;
            string report_description;
            string report_priority;
            int report_key;

            string _source_name;
            string _source_description;
            string _display_source;
            int _source_key;

          [DataMember (IsRequired=true)]
        public string MailGroupName
        {
            get { return mailgroupname; }
            set { mailgroupname = value; }
        }
          [DataMember(IsRequired = true)]
          public string MailGroupStatus
          {
              get { return mailgroupstatus;  }
              set { mailgroupstatus = value; }
          }
          [DataMember (IsRequired=true)]
        public string UserMailId
        {
            get { return userMailId; }
            set { userMailId = value; }
        }
        [DataMember (IsRequired=true)]
        public int MailGrpId
        {
            get { return mailGrpId; }
            set { mailGrpId = value; }
        }
          [DataMember (IsRequired=true)]
        public int UserId
        {
            get { return userId; }
            set { userId = value; }
        }

          [DataMember (IsRequired=true)]
        public int StatusId
        {
            get { return statusId; }
            set { statusId = value; }
        }

          [DataMember (IsRequired=true)]
        public int PluginId
        {
            get { return pluginId; }
            set { pluginId = value; }
        }
          [DataMember (IsRequired=true)]
        public string Plugin
        {
            get { return plugin; }
            set { plugin = value; }
        }
          [DataMember (IsRequired=true)]
        public int Seqnum
        {
            get { return seqnum; }
            set { seqnum = value; }
        }

          [DataMember (IsRequired=true)]
        public string EngineType
        {
            get { return engineType; }
            set { engineType = value; }
        }
          [DataMember (IsRequired=true)]
        public int PluginConfigId
        {
            get { return pluginConfigId; }
            set { pluginConfigId = value; }
        }
          [DataMember (IsRequired=true)]
        public string PropName
        {
            get { return propName; }
            set { propName = value; }
        }
          [DataMember (IsRequired=true)]
        public string PropValue
        {
            get { return propValue; }
            set { propValue = value; }
        }
          [DataMember (IsRequired=true)]
        public int PropId
        {
            get { return propId; }
            set { propId = value; }
        }

          [DataMember (IsRequired=true)]
        public string Jobname
        {
            get { return jobName; }
            set { jobName = value; }
        }
          [DataMember (IsRequired=true)]
        public int UpdateId
        {
            get { return updateId; }
            set { updateId = value; }
        }
          [DataMember (IsRequired=true)]
        public string Description
        {
            get { return description; }
            set { description = value; }
        }
          [DataMember (IsRequired=true)]
        public string JobLastUpdate
        {
            get { return jobLastUpdate; }
            set { jobLastUpdate = value; }
        }

          [DataMember (IsRequired=true)]
        public string Previousvalue
        {
            get { return previousvalue; }
            set { previousvalue = value; }
        }

          [DataMember (IsRequired=true)]
        public string Config
        {
            get { return config; }
            set { config = value; }
        }

          [DataMember (IsRequired=true)]
        public string ConfigGUIDkey
        {
            get { return configGUIDkey; }
            set { configGUIDkey = value; }
        }
          [DataMember (IsRequired=true)]
        public int ConfigId
        {
            get { return configId; }
            set { configId = value; }
        }
          [DataMember(IsRequired = true)]
          public string ConfigGUIDkeyVersion
          {
              get { return configGUIDKeyVersion; }
              set { configGUIDKeyVersion = value; }
          }


          [DataMember (IsRequired=true)]
        public int JobId
        {
            get { return jobId; }
            set { jobId = value; }
        }
          [DataMember (IsRequired=true)]
        public string Calendar
        {
            get { return calendar; }
            set { calendar = value; }
        }
          [DataMember (IsRequired=true)]
        public DateTime StartTime
        {
            get { return starttime; }
            set { starttime = value; }
        }
          [DataMember (IsRequired=true)]
        public DateTime EndTime
        {
            get { return endtime; }
            set { endtime = value; }
        }
          [DataMember (IsRequired=true)]
        public int Duration
        {
            get { return duration; }
            set { duration = value; }
        }
          [DataMember (IsRequired=true)]
        public int Frequency
        {
            get { return frequency; }
            set { frequency = value; }
        }
          [DataMember (IsRequired=true)]
        public DateTime LastUpdateDate
        {
            get { return lastUpdateDate; }
            set { lastUpdateDate = value; }
        }
          [DataMember (IsRequired=true)]
        public string Active
        {
            get { return active; }
            set { active = value; }
        }
          [DataMember(IsRequired = true)]
          public string JobDescription
          {
              get { return jobdescription; }
              set { jobdescription = value; }
          }
          [DataMember(IsRequired = true)]
          public string Desk
          {
              get { return desk; }
              set { desk = value; }
          }
          [DataMember(IsRequired = true)]
          public string Status
          {
              get { return status; }
              set { status = value; }
          }
          [DataMember(IsRequired = true)]
          public int Trig_Id
          {
              get { return trig_Id; }
              set { trig_Id = value; }
          }
          [DataMember(IsRequired = true)]
          public int Trigger_Job_Id
          {
              get { return trigger_Job_Id; }
              set { trigger_Job_Id = value; }
          }

          [DataMember(IsRequired = true)]
          public string SeriesName
          {
              get { return series_name; }
              set { series_name = value; }
          }
          [DataMember(IsRequired = true)]
          public int Series_Key
          {
              get { return series_key; }
              set { series_key = value; }
          }


          [DataMember(IsRequired = true)]
          public string Product_Group
          {
              get { return product_group; }
              set { product_group = value; }
          }
          [DataMember(IsRequired = true)]
          public string Product_Description
          {
              get { return product_description; }
              set { product_description = value; }
          }
          [DataMember(IsRequired = true)]
          public string Product_Name
          {
              get { return product_name; }
              set { product_name = value; }
          }
          [DataMember(IsRequired = true)]
          public int Product_Key
          {
              get { return product_key; }
              set { product_key = value; }
          }
          [DataMember(IsRequired = true)]
          public string Display_Product
          {
              get { return display_product; }
              set { display_product = value; }
          }

          [DataMember(IsRequired = true)]
          public string Report_Name
          {
              get { return report_name; }
              set { report_name = value; }
          }
          [DataMember(IsRequired = true)]
          public string Report_Description
          {
              get { return report_description; }
              set { report_description = value; }
          }
          [DataMember(IsRequired = true)]
          public string Report_Priority
          {
              get { return report_priority; }
              set { report_priority = value; }
          }
          [DataMember(IsRequired = true)]
          public int Report_key
          {
              get { return report_key; }
              set { report_key = value; }
          }

          [DataMember(IsRequired = true)]
          public string Source_Name
          {
              get { return _source_name; }
              set { _source_name = value;}
          }

          [DataMember(IsRequired = true)]
          public string Source_Description
          {
              get { return _source_description; }
              set { _source_description = value; }
          }

          [DataMember(IsRequired = true)]
          public string Display_Source
          {
              get { return _display_source; }
              set { _display_source = value; }
          }

          [DataMember(IsRequired = true)]
          public int Source_Key
          {
              get { return _source_key; }
              set { _source_key = value; }
          }
    }
    public class JobPlugin
    {

    }

    
}