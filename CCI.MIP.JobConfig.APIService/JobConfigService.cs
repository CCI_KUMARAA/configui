﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Oracle.DataAccess.Client;
using System.Configuration;
using CCI.MIP.JobConfig.APIService.Properties;

namespace CCI.MIP.JobConfig.APIService
{

    public class JobConfigService : IJobConfigService
    {
        private static readonly log4net.ILog _log = log4net.LogManager.GetLogger(typeof(JobConfigService));
        public DataSet GetMailingListWithJobId(JobConfigPlugin objJobConfig)
        {
            DataSet ds = new DataSet();
            //var list = new List<IJobMailGroupdetails>();
            try
            {
                _log.Info("calling GetMailingListWithJobId");
                using (OracleConnection con = new OracleConnection(Settings.Default.ConnectionStringAPP_MIDB))
                {
                    con.Open();
                    using (var com = con.CreateCommand())
                    {
                        com.Connection = con;
                        var sql = Settings.Default.QueryGetMailingListWithJobId;
                        com.CommandText = sql;
                        com.CommandType = CommandType.Text;
                        com.BindByName = true;
                        com.Parameters.Add(":jobId", OracleDbType.Int32).Value = objJobConfig.JobId;

                        OracleDataAdapter adap = new OracleDataAdapter(com);
                        adap.Fill(ds);

                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
            }
            return ds;
        }
        public DataSet GetSeriesName(JobConfigPlugin objJobConfig)
        {
            DataSet ds = new DataSet();
            //var list = new List<IJobMailGroupdetails>();
            try
            {
                _log.Info("calling GetSeriesName");
                using (OracleConnection con = new OracleConnection(Settings.Default.ConnectionStringAPP_MIDW))
                {
                    con.Open();
                    using (var com = con.CreateCommand())
                    {
                        com.Connection = con;
                        var sql = Settings.Default.QueryGetSeriesName;
                        com.CommandText = sql;
                        com.CommandType = CommandType.Text;
                        com.BindByName = true;
                        com.Parameters.Add(":seriesname", OracleDbType.Varchar2).Value = String.Format("%{0}%", objJobConfig.SeriesName.ToUpper());

                        OracleDataAdapter adap = new OracleDataAdapter(com);
                        adap.Fill(ds);

                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
            }
            return ds;
        }

        public int UpdateSeriesName(JobConfigPlugin objJobConfig)
        {
            try
            {
                _log.Info("calling UpdateSeriesName");
                using (OracleConnection con = new OracleConnection(Settings.Default.ConnectionStringAPP_MIDW))
                {
                    con.Open();
                    using (var com = con.CreateCommand())
                    {
                        com.Connection = con;
                        var sql1 = Settings.Default.QueryUpdateSeriesName;
                        com.CommandText = sql1;
                        com.CommandType = CommandType.Text;
                        com.BindByName = true;
                        com.Parameters.Add(":seriesname", OracleDbType.Varchar2).Value = objJobConfig.SeriesName.ToUpper();
                        com.Parameters.Add(":serieskey", OracleDbType.Varchar2).Value = objJobConfig.Series_Key;
                        
                        int iresult = com.ExecuteNonQuery();
                        return iresult;
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
                if (ex.Message.Contains("unique constraint"))
                {
                    return 2;
                }
                return -1;
            }
        }
        public DataSet GetMailingList(JobConfigPlugin objJobConfig)
        {
            DataSet ds = new DataSet();
            //var list = new List<IJobMailGroupdetails>();
            try
            {
                _log.Info("calling GetMailingList");
                using (OracleConnection con = new OracleConnection(Settings.Default.ConnectionStringAPP_MIDB))
                {
                    con.Open();
                    using (var com = con.CreateCommand())
                    {
                        com.Connection = con;
                        var sql = Settings.Default.QuerySelectMailingList;
                        com.CommandText = sql;
                        com.CommandType = CommandType.Text;
                        com.BindByName = true;

                        com.Parameters.Add(":mailgroup", OracleDbType.Varchar2).Value = objJobConfig.MailGroupName.ToUpper();
                        OracleDataAdapter adap = new OracleDataAdapter(com);
                        adap.Fill(ds);

                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
            }
            return ds;
        }
        public int InsertMailingList(JobConfigPlugin objJobConfig)
        {
            try
            {
                _log.Info("calling InsertMailingList");
                using (OracleConnection con = new OracleConnection(Settings.Default.ConnectionStringAPP_MIDB))
                {
                    con.Open();
                    using (var com = con.CreateCommand())
                    {
                        com.Connection = con;
                        //var sql = Settings.Default.QuerySelectMaxUserId;
                        //com.CommandText = sql;
                        //com.CommandType = CommandType.Text;
                        //com.BindByName = true;
                        //var UserId = com.ExecuteScalar();
                        //var newUserId = Convert.ToInt32(UserId) + 1;
                        var sql1 = Settings.Default.QueryInsertMailingList;
                        com.CommandText = sql1;
                        com.CommandType = CommandType.Text;
                        com.BindByName = true;

                        com.Parameters.Add(":mailgroupname", OracleDbType.Varchar2).Value = objJobConfig.MailGroupName.ToUpper();
                        com.Parameters.Add(":usermailId", OracleDbType.Varchar2).Value = objJobConfig.UserMailId;
                        //com.Parameters.Add(":userId", OracleDbType.Varchar2).Value = newUserId;
                        int iresult = com.ExecuteNonQuery();
                        return iresult;
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
                return -1;

            }
        }
        public int UpdateMailingList(JobConfigPlugin objJobConfig)
        {
            try
            {
                _log.Info("calling UpdateMailingList");
                using (OracleConnection con = new OracleConnection(Settings.Default.ConnectionStringAPP_MIDB))
                {
                    con.Open();
                    using (var com = con.CreateCommand())
                    {
                        com.Connection = con;
                        var sql1 = Settings.Default.QueryUpdateMailingList;
                        com.CommandText = sql1;
                        com.CommandType = CommandType.Text;
                        com.BindByName = true;
                        com.Parameters.Add(":mailgroupname", OracleDbType.Varchar2).Value = objJobConfig.MailGroupName.ToUpper();
                        com.Parameters.Add(":usermailId", OracleDbType.Varchar2).Value = objJobConfig.UserMailId;
                        com.Parameters.Add(":userId", OracleDbType.Varchar2).Value = objJobConfig.UserId;
                        int iresult = com.ExecuteNonQuery();
                        return iresult;
                    }
                }

            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
                return -1;
            }
        }
        public int CheckMailGroupWithJobId(JobConfigPlugin objJobConfig)
        {
            try
            {
                _log.Info("calling CheckMailGroupWithJobId");
                using (OracleConnection con = new OracleConnection(Settings.Default.ConnectionStringAPP_MIDB))
                {
                    con.Open();
                    using (var com = con.CreateCommand())
                    {
                        com.Connection = con;
                        var sql1 = Settings.Default.QueryCheckMailGroupWithJobId;
                        com.CommandText = sql1;
                        com.CommandType = CommandType.Text;
                        com.BindByName = true;
                        com.Parameters.Add(":mailgroupname", OracleDbType.Varchar2).Value = objJobConfig.MailGroupName.ToUpper();
                        com.Parameters.Add(":jobmailstatus", OracleDbType.Varchar2).Value = objJobConfig.MailGroupStatus;
                        com.Parameters.Add(":jobId", OracleDbType.Int32).Value = objJobConfig.JobId;
                        int iresult = Convert.ToInt32(com.ExecuteScalar());
                        return iresult;
                    }
                }

            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
                return -1;
            }
        }

        public int CheckMailGroup(JobConfigPlugin objJobConfig)
        {
            try
            {
                _log.Info("calling CheckMailGroup");
                using (OracleConnection con = new OracleConnection(Settings.Default.ConnectionStringAPP_MIDB))
                {
                    con.Open();
                    using (var com = con.CreateCommand())
                    {
                        com.Connection = con;
                        var sql1 = Settings.Default.QueryCheckMailGroup;
                        com.CommandText = sql1;
                        com.CommandType = CommandType.Text;
                        com.BindByName = true;
                        com.Parameters.Add(":mailgroupname", OracleDbType.Varchar2).Value = objJobConfig.MailGroupName.ToUpper();

                        int iresult = Convert.ToInt32(com.ExecuteScalar());
                        return iresult;
                    }
                }

            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
                return -1;
            }
        }
        public DataSet GetJobMaster()
        {
            DataSet ds = new DataSet();

            try
            {
                _log.Info("calling GetJobMaster");
                //ConfigurationManager.AppSettings["ConnectionString"];
                //using (OracleConnection con = new OracleConnection(Settings.Default.ConnectionString))
                using (OracleConnection con = new OracleConnection(Settings.Default.ConnectionStringAPP_MIDB))
                {
                    con.Open();
                    using (var com = con.CreateCommand())
                    {
                        com.Connection = con;
                        var sql = Settings.Default.QueryGetJobMaster;
                        com.CommandText = sql;
                        com.CommandType = CommandType.Text;
                        com.BindByName = true;
                        OracleDataAdapter adapter = new OracleDataAdapter(com);
                        adapter.Fill(ds);

                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
            }
            return ds;
        }

        public DataSet GetJobMasterWithDateTime()
        {
            DataSet ds = new DataSet();

            try
            {
                _log.Info("calling GetJobMaster");
                //ConfigurationManager.AppSettings["ConnectionString"];
                //using (OracleConnection con = new OracleConnection(Settings.Default.ConnectionString))
                using (OracleConnection con = new OracleConnection(Settings.Default.ConnectionStringAPP_MIDB))
                {
                    con.Open();
                    using (var com = con.CreateCommand())
                    {
                        com.Connection = con;
                        var sql = Settings.Default.QueryGetJobMasterWithDateTime;
                        com.CommandText = sql;
                        com.CommandType = CommandType.Text;
                        com.BindByName = true;
                        OracleDataAdapter adapter = new OracleDataAdapter(com);
                        adapter.Fill(ds);

                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
            }
            return ds;
        }

        public DataSet GetCalendar()
        {
            DataSet ds = new DataSet();

            try
            {
                _log.Info("calling GetCalendar");
                using (OracleConnection con = new OracleConnection(Settings.Default.ConnectionStringAPP_MIDB))
                {
                    con.Open();
                    using (var com = con.CreateCommand())
                    {
                        com.Connection = con;
                        var sql = Settings.Default.QueryGetCalendar;
                        com.CommandText = sql;
                        com.CommandType = CommandType.Text;
                        com.BindByName = true;
                        OracleDataAdapter adapter = new OracleDataAdapter(com);
                        adapter.Fill(ds);

                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
            }
            return ds;
        }
        public int GetJobId(JobConfigPlugin objJobConfig)
        {
            int jobid = 0;
            try
            {
                _log.Info("calling GetJobId");
                using (OracleConnection con = new OracleConnection(Settings.Default.ConnectionStringAPP_MIDB))
                {
                    con.Open();
                    using (var com = con.CreateCommand())
                    {
                        com.Connection = con;
                        var sql = Settings.Default.QueryGetJobId;
                        com.CommandText = sql;
                        com.CommandType = CommandType.Text;
                        com.BindByName = true;

                        com.Parameters.Add(":jobname", OracleDbType.Varchar2).Value = objJobConfig.Jobname;
                        jobid = Convert.ToInt32(com.ExecuteScalar());
                        return jobid;
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
            }
            return jobid;
        }
        public int InsertJobMaster(JobConfigPlugin objJobConfig)
        {
            try
            {
                _log.Info("calling InsertJobMaster");
                using (OracleConnection con = new OracleConnection(Settings.Default.ConnectionStringAPP_MIDB))
                {
                    con.Open();
                    using (var com = con.CreateCommand())
                    {
                        com.Connection = con;

                        var sql1 = Settings.Default.QueryInsertJobMaster;
                        com.CommandText = sql1;

                        com.CommandType = CommandType.Text;
                        com.BindByName = true;
                        com.Parameters.Add(":jobname", OracleDbType.Varchar2).Value = objJobConfig.Jobname;
                        com.Parameters.Add(":description", OracleDbType.Varchar2).Value = objJobConfig.JobDescription;
                        com.Parameters.Add(":desk", OracleDbType.Varchar2).Value = objJobConfig.Desk;

                        int iresult = com.ExecuteNonQuery();
                        return iresult;
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
                return -1;
            }
        }
        public int UpdateJobMaster(JobConfigPlugin objJobConfig)
        {
            try
            {
                _log.Info("calling UpdateJobMaster");
                using (OracleConnection con = new OracleConnection(Settings.Default.ConnectionStringAPP_MIDB))
                {
                    con.Open();
                    using (var com = con.CreateCommand())
                    {
                        com.Connection = con;
                        var sql1 = Settings.Default.QueryUpdateJobMaster;
                        com.CommandText = sql1;
                        com.CommandType = CommandType.Text;
                        com.BindByName = true;
                        com.Parameters.Add(":jobname", OracleDbType.Varchar2).Value = objJobConfig.Jobname; ;
                        com.Parameters.Add(":jobId", OracleDbType.Varchar2).Value = objJobConfig.JobId;
                        com.Parameters.Add(":description", OracleDbType.Varchar2).Value = objJobConfig.JobDescription;
                        com.Parameters.Add(":desk", OracleDbType.Varchar2).Value = objJobConfig.Desk;
                        int iresult = com.ExecuteNonQuery();
                        return iresult;
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
                return -1;
            }
        }


        public int InsertJobSchedule(JobConfigPlugin objJobConfig)
        {
            try
            {
                _log.Info("calling InsertJobSchedule");
                DataSet ds = new DataSet();

                using (OracleConnection con = new OracleConnection(Settings.Default.ConnectionStringAPP_MIDB))
                {
                    con.Open();
                    using (var com = con.CreateCommand())
                    {
                        com.Connection = con;

                        var sql1 = Settings.Default.QueryInsertJobSchedule;
                        com.CommandText = sql1;

                        com.CommandType = CommandType.Text;
                        com.BindByName = true;
                        com.Parameters.Add(":jobId", OracleDbType.Int32).Value = objJobConfig.JobId;
                        com.Parameters.Add(":calendar", OracleDbType.Varchar2).Value = objJobConfig.Calendar;
                        com.Parameters.Add(":starttime", OracleDbType.Date).Value = objJobConfig.StartTime;
                        com.Parameters.Add(":endtime", OracleDbType.Date).Value = objJobConfig.EndTime;
                        com.Parameters.Add(":duration", OracleDbType.Int32).Value = objJobConfig.Duration;
                        com.Parameters.Add(":frequency", OracleDbType.Int32).Value = objJobConfig.Frequency;
                        com.Parameters.Add(":lastupdatedate", OracleDbType.Date).Value = objJobConfig.LastUpdateDate;
                        com.Parameters.Add(":jobflag", OracleDbType.Varchar2).Value = objJobConfig.Active;
                        int iresult = com.ExecuteNonQuery();
                        return iresult;
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
                return -1;
            }
        }
        public int UpdateJobSchedule(JobConfigPlugin objJobConfig)
        {
            try
            {
                _log.Info("calling UpdateJobSchedule");
                using (OracleConnection con = new OracleConnection(Settings.Default.ConnectionStringAPP_MIDB))
                {
                    con.Open();
                    using (var com = con.CreateCommand())
                    {
                        com.Connection = con;
                        var sql = Settings.Default.QueryUpdateJobSchedule;
                        com.CommandText = sql;
                        com.CommandType = CommandType.Text;
                        com.BindByName = true;
                        com.Parameters.Add(":jobId", OracleDbType.Int32).Value = objJobConfig.JobId;
                        com.Parameters.Add(":calendar", OracleDbType.Varchar2).Value = objJobConfig.Calendar;
                        com.Parameters.Add(":starttime", OracleDbType.Date).Value = objJobConfig.StartTime;
                        com.Parameters.Add(":endtime", OracleDbType.Date).Value = objJobConfig.EndTime;
                        com.Parameters.Add(":duration", OracleDbType.Int32).Value = objJobConfig.Duration;
                        com.Parameters.Add(":frequency", OracleDbType.Int32).Value = objJobConfig.Frequency;
                        com.Parameters.Add(":lastupdatedate", OracleDbType.Date).Value = objJobConfig.LastUpdateDate;
                        com.Parameters.Add(":jobflag", OracleDbType.Varchar2).Value = objJobConfig.Active;


                        int iresult = com.ExecuteNonQuery();
                        return iresult;
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
                return -1;
            }
        }
        public DataSet GetJobStatus(JobConfigPlugin objJobConfig)
        {
            DataSet ds = new DataSet();
            try
            {
                _log.Info("calling GetJobStatus");
                using (OracleConnection con = new OracleConnection(Settings.Default.ConnectionStringAPP_MIDB))
                {
                    con.Open();
                    using (var com = con.CreateCommand())
                    {
                        com.Connection = con;
                        var sql = Settings.Default.QueryGetJobStatus;
                        com.CommandText = sql;
                        com.CommandType = CommandType.Text;
                        com.BindByName = true;
                        com.Parameters.Add(":jobId", OracleDbType.Int32).Value = objJobConfig.JobId;
                        OracleDataAdapter adapter = new OracleDataAdapter(com);
                        adapter.Fill(ds);
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
            }
            return ds;
        }
        public DataSet GetJobLog(JobConfigPlugin objJobConfig)
        {
            DataSet ds = new DataSet();
            try
            {
                _log.Info("calling GetJobLog");
                using (OracleConnection con = new OracleConnection(Settings.Default.ConnectionStringAPP_MIDB))
                {
                    con.Open();
                    using (var com = con.CreateCommand())
                    {
                        com.Connection = con;
                        var sql = Settings.Default.QueryGetJobLog;
                        com.CommandText = sql;
                        com.CommandType = CommandType.Text;
                        com.BindByName = true;
                        com.Parameters.Add(":statusId", OracleDbType.Int32).Value = objJobConfig.StatusId;
                        OracleDataAdapter adapter = new OracleDataAdapter(com);
                        adapter.Fill(ds);
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
            }
            return ds;
        }
        public DataSet GetJobPluginConfigWithPlugin(JobConfigPlugin objJobConfig)
        {
            DataSet ds = new DataSet();

            try
            {
                _log.Info("calling GetJobPluginConfigWithPlugin");
                using (OracleConnection con = new OracleConnection(Settings.Default.ConnectionStringAPP_MIDB))
                {
                    con.Open();
                    using (var com = con.CreateCommand())
                    {
                        com.Connection = con;
                        var sql = Settings.Default.QueryGetJobPluginConfigWithPlugin;
                        com.CommandText = sql;
                        com.CommandType = CommandType.Text;
                        com.BindByName = true;
                        com.Parameters.Add(":jobId", OracleDbType.Int32).Value = objJobConfig.JobId;
                        com.Parameters.Add(":plugin", OracleDbType.Varchar2).Value = objJobConfig.Plugin;
                        com.Parameters.Add(":seqnum", OracleDbType.Int32).Value = objJobConfig.Seqnum;
                        OracleDataAdapter adapter = new OracleDataAdapter(com);
                        adapter.Fill(ds);

                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
            }
            return ds;
        }
        public DataSet GetJobPluginConfigWithJobId(JobConfigPlugin objJobConfig)
        {
            DataSet ds = new DataSet();

            try
            {
                _log.Info("calling GetJobPluginConfigWithJobId");
                using (OracleConnection con = new OracleConnection(Settings.Default.ConnectionStringAPP_MIDB))
                {
                    con.Open();
                    using (var com = con.CreateCommand())
                    {
                        com.Connection = con;
                        var sql = Settings.Default.QueryGetJobPluginConfiWitJobId;
                        com.CommandText = sql;
                        com.CommandType = CommandType.Text;
                        com.BindByName = true;
                        com.Parameters.Add(":jobId", OracleDbType.Int32).Value = objJobConfig.JobId;

                        OracleDataAdapter adapter = new OracleDataAdapter(com);
                        adapter.Fill(ds);

                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
            }
            return ds;
        }
        public int InsertJobPluginConfig(JobConfigPlugin objJobConfig)
        {
            try
            {
                _log.Info("calling InsertJobPluginConfig");
                using (OracleConnection con = new OracleConnection(Settings.Default.ConnectionStringAPP_MIDB))
                {
                    con.Open();
                    using (var com = con.CreateCommand())
                    {
                        com.Connection = con;

                        var sql1 = Settings.Default.QueryInsertJobPluginConfig;
                        com.CommandText = sql1;

                        com.CommandType = CommandType.Text;
                        com.BindByName = true;

                        com.Parameters.Add(":jobId", OracleDbType.Varchar2).Value = objJobConfig.JobId;
                        com.Parameters.Add(":seqnum", OracleDbType.Varchar2).Value = objJobConfig.Seqnum;
                        com.Parameters.Add(":enginetype", OracleDbType.Varchar2).Value = objJobConfig.EngineType;
                        com.Parameters.Add(":plugin", OracleDbType.Varchar2).Value = objJobConfig.Plugin;
                        int iresult = com.ExecuteNonQuery();
                        return iresult;
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
                return -1;
            }
        }
        public int UpdateJobPluginConfig(JobConfigPlugin objJobConfig)
        {

            try
            {
                _log.Info("calling UpdateJobPluginConfig");
                using (OracleConnection con = new OracleConnection(Settings.Default.ConnectionStringAPP_MIDB))
                {
                    con.Open();
                    using (var com = con.CreateCommand())
                    {
                        com.Connection = con;
                        var sql1 = Settings.Default.QueryUpdateJobPluginConfig;
                        com.CommandText = sql1;
                        com.CommandType = CommandType.Text;
                        com.BindByName = true;
                        com.Parameters.Add(":jobId", OracleDbType.Varchar2).Value = objJobConfig.JobId;
                        com.Parameters.Add(":seqnum", OracleDbType.Varchar2).Value = objJobConfig.Seqnum;
                        com.Parameters.Add(":enginetype", OracleDbType.Varchar2).Value = objJobConfig.EngineType;
                        com.Parameters.Add(":plugin", OracleDbType.Varchar2).Value = objJobConfig.Plugin;
                        com.Parameters.Add(":pluginConfigId", OracleDbType.Varchar2).Value = objJobConfig.PluginConfigId;
                        int iresult = com.ExecuteNonQuery();
                        return iresult;
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
                return -1;
            }
        }
        public DataSet GetJobPluginProp(JobConfigPlugin objJobConfig)
        {
            DataSet ds = new DataSet();

            try
            {
                _log.Info("calling GetJobPluginProp");
                using (OracleConnection con = new OracleConnection(Settings.Default.ConnectionStringAPP_MIDB))
                {
                    con.Open();
                    using (var com = con.CreateCommand())
                    {
                        com.Connection = con;
                        var sql = Settings.Default.QueryGetJobPluginProp;
                        com.CommandText = sql;
                        com.CommandType = CommandType.Text;
                        com.BindByName = true;
                        com.Parameters.Add(":plugin", OracleDbType.Varchar2).Value = String.Format("%{0}%", objJobConfig.Plugin);
                        com.Parameters.Add(":enginetype", OracleDbType.Varchar2).Value = String.Format("%{0}%", objJobConfig.EngineType);
                        com.Parameters.Add(":jobId", OracleDbType.Int32).Value = objJobConfig.JobId;
                        OracleDataAdapter adapter = new OracleDataAdapter(com);
                        adapter.Fill(ds);

                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
            }
            return ds;
        }
        public DataSet GetJobPluginPropWitPluginId(JobConfigPlugin objJobConfig)
        {
            DataSet ds = new DataSet();

            try
            {
                _log.Info("calling GetJobPluginPropWitPluginId");
                using (OracleConnection con = new OracleConnection(Settings.Default.ConnectionStringAPP_MIDB))
                {
                    con.Open();
                    using (var com = con.CreateCommand())
                    {
                        com.Connection = con;
                        var sql = Settings.Default.QueryGetJobPluginPropWitPluginId;
                        com.CommandText = sql;
                        com.CommandType = CommandType.Text;
                        com.BindByName = true;

                        com.Parameters.Add(":pluginId", OracleDbType.Int32).Value = objJobConfig.PluginId;


                        OracleDataAdapter adapter = new OracleDataAdapter(com);
                        adapter.Fill(ds);

                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
            }
            return ds;
        }
        public DataSet GetJobUpdatewitName(JobConfigPlugin objJobConfig)
        {
            DataSet ds = new DataSet();

            try
            {
                _log.Info("calling GetJobUpdatewitName");
                using (OracleConnection con = new OracleConnection(Settings.Default.ConnectionStringAPP_MIDB))
                {
                    con.Open();
                    using (var com = con.CreateCommand())
                    {
                        com.Connection = con;
                        var sql = Settings.Default.QueryGetJobUpdate;
                        com.CommandText = sql;
                        com.CommandType = CommandType.Text;
                        com.BindByName = true;

                        com.Parameters.Add(":jobname", OracleDbType.Varchar2).Value = objJobConfig.Jobname;


                        OracleDataAdapter adapter = new OracleDataAdapter(com);
                        adapter.Fill(ds);

                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
            }
            return ds;
        }
        public int InsertJobPluginProp(JobConfigPlugin objJobConfig)
        {
            try
            {
                _log.Info("calling InsertJobPluginProp");
                using (OracleConnection con = new OracleConnection(Settings.Default.ConnectionStringAPP_MIDB))
                {
                    con.Open();
                    using (var com = con.CreateCommand())
                    {
                        com.Connection = con;

                        var sql1 = Settings.Default.QueryInsertJobPluginProp;
                        com.CommandText = sql1;

                        com.CommandType = CommandType.Text;
                        com.BindByName = true;
                        com.Parameters.Add(":pluginConfigId", OracleDbType.Varchar2).Value = objJobConfig.PluginConfigId;
                        com.Parameters.Add(":propName", OracleDbType.Varchar2).Value = objJobConfig.PropName;
                        com.Parameters.Add(":propValue", OracleDbType.Varchar2).Value = objJobConfig.PropValue;
                        int iresult = com.ExecuteNonQuery();
                        return iresult;
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
                return -1;
                throw ex;
            }
        }
        public int UpdateJobPluginProp(JobConfigPlugin objJobConfig)
        {
            try
            {
                _log.Info("calling UpdateJobPluginProp");
                using (OracleConnection con = new OracleConnection(Settings.Default.ConnectionStringAPP_MIDB))
                {
                    con.Open();
                    using (var com = con.CreateCommand())
                    {
                        com.Connection = con;
                        if (objJobConfig.PropId != 0)
                        {
                            var sql1 = Settings.Default.QueryUpdateJobPluginProp;
                            com.CommandText = sql1;
                            com.CommandType = CommandType.Text;
                            com.BindByName = true;
                            com.Parameters.Add(":pluginConfigId", OracleDbType.Varchar2).Value = objJobConfig.PluginConfigId;
                            com.Parameters.Add(":propName", OracleDbType.Varchar2).Value = objJobConfig.PropName;
                            com.Parameters.Add(":propValue", OracleDbType.Varchar2).Value = objJobConfig.PropValue;
                            com.Parameters.Add(":propId", OracleDbType.Varchar2).Value = objJobConfig.PropId;
                        }
                        else
                        {
                            var sql1 = Settings.Default.QueryUpdateJobPropwithPropName;
                            com.CommandText = sql1;
                            com.CommandType = CommandType.Text;
                            com.BindByName = true;

                            com.Parameters.Add(":propName", OracleDbType.Varchar2).Value = objJobConfig.PropName;
                            com.Parameters.Add(":propValue", OracleDbType.Varchar2).Value = objJobConfig.PropValue;

                        }

                        int iresult = com.ExecuteNonQuery();
                        return iresult;
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
                return -1;
            }
        }

        public int InsertJobUpdate(JobConfigPlugin objJobConfig)
        {
            try
            {
                _log.Info("calling InsertJobUpdate");
                using (OracleConnection con = new OracleConnection(Settings.Default.ConnectionStringAPP_MIDB))
                {
                    con.Open();
                    using (var com = con.CreateCommand())
                    {
                        com.Connection = con;

                        var sql1 = Settings.Default.QueryInsertJobUpdate;
                        com.CommandText = sql1;

                        com.CommandType = CommandType.Text;
                        com.BindByName = true;
                        com.Parameters.Add(":jobname", OracleDbType.Varchar2).Value = objJobConfig.Jobname;
                        com.Parameters.Add(":lastupdate", OracleDbType.Varchar2).Value = objJobConfig.JobLastUpdate;
                        com.Parameters.Add(":description", OracleDbType.Varchar2).Value = objJobConfig.Description;
                        com.Parameters.Add(":previousvalue", OracleDbType.Varchar2).Value = objJobConfig.Previousvalue;
                        int iresult = com.ExecuteNonQuery();
                        return iresult;
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
                return -1;
            }
        }
        public int DeleteJobPluginProp(JobConfigPlugin objJobConfig)
        {
            try
            {
                _log.Info("calling DeleteJobPluginProp");
                using (OracleConnection con = new OracleConnection(Settings.Default.ConnectionStringAPP_MIDB))
                {
                    con.Open();
                    using (var com = con.CreateCommand())
                    {
                        com.Connection = con;

                        var sql1 = Settings.Default.QueryDeleteJobPropPlugin;
                        com.CommandText = sql1;

                        com.CommandType = CommandType.Text;
                        com.BindByName = true;
                        com.Parameters.Add(":propId", OracleDbType.Varchar2).Value = objJobConfig.PropId;

                        int iresult = com.ExecuteNonQuery();
                        return iresult;
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
                return -1;
            }
        }


        public int DeleteJobUpdate(JobConfigPlugin objJobConfig)
        {
            try
            {
                _log.Info("calling DeleteJobUpdate");
                using (OracleConnection con = new OracleConnection(Settings.Default.ConnectionStringAPP_MIDB))
                {
                    con.Open();
                    using (var com = con.CreateCommand())
                    {
                        com.Connection = con;

                        var sql1 = Settings.Default.QueryDeleteJobUpdate;
                        com.CommandText = sql1;

                        com.CommandType = CommandType.Text;
                        com.BindByName = true;
                        com.Parameters.Add(":jobname", OracleDbType.Varchar2).Value = objJobConfig.Jobname;

                        int iresult = com.ExecuteNonQuery();
                        return iresult;
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
                return -1;
            }

        }
        public int UpdateJobUpdate(JobConfigPlugin objJobConfig)
        {
            try
            {
                _log.Info("calling UpdateJobUpdate");
                using (OracleConnection con = new OracleConnection(Settings.Default.ConnectionStringAPP_MIDB))
                {
                    con.Open();
                    using (var com = con.CreateCommand())
                    {
                        com.Connection = con;
                        var sql1 = Settings.Default.QueryUpdateJobUpdate;
                        com.CommandText = sql1;
                        com.CommandType = CommandType.Text;
                        com.BindByName = true;
                        com.Parameters.Add(":updateid", OracleDbType.Varchar2).Value = objJobConfig.UpdateId;
                        com.Parameters.Add(":jobname", OracleDbType.Varchar2).Value = objJobConfig.Jobname;
                        com.Parameters.Add(":lastupdate", OracleDbType.Varchar2).Value = objJobConfig.JobLastUpdate;
                        com.Parameters.Add(":description", OracleDbType.Varchar2).Value = objJobConfig.Description;
                        int iresult = com.ExecuteNonQuery();
                        return iresult;
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
                return -1;
            }
        }
        public DataSet GetConfigReposGUID(JobConfigPlugin objJobConfig)
        {

            DataSet ds = new DataSet();

            try
            {
                _log.Info("calling GetConfigReposGUID");
                using (OracleConnection con = new OracleConnection(Settings.Default.ConnectionStringAPP_MIDB))
                {
                    con.Open();
                    using (var com = con.CreateCommand())
                    {
                        com.Connection = con;
                        var sql = Settings.Default.QueryGetConfig_Repos;
                        com.CommandText = sql;
                        com.CommandType = CommandType.Text;
                        com.BindByName = true;
                        com.Parameters.Add(":Config_GUID", OracleDbType.Varchar2).Value = objJobConfig.ConfigGUIDkey.ToUpper();
                        OracleDataAdapter adapter = new OracleDataAdapter(com);
                        adapter.Fill(ds);

                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
            }
            return ds;
        }
        public int GetConfigReposGUIDVersion(JobConfigPlugin objJobConfig)
        {
            int version = 0;
            try
            {
                _log.Info("calling GetConfigReposGUIDVersion");
                using (OracleConnection con = new OracleConnection(Settings.Default.ConnectionStringAPP_MIDB))
                {
                    con.Open();
                    using (var com = con.CreateCommand())
                    {
                        com.Connection = con;
                        var sql = Settings.Default.QueryGetConfigReposGUIDVersion;
                        com.CommandText = sql;
                        com.CommandType = CommandType.Text;
                        com.BindByName = true;

                        com.Parameters.Add(":configGUIDkey", OracleDbType.Varchar2).Value = objJobConfig.ConfigGUIDkeyVersion+"%";
                        com.Parameters.Add(":configGUIDkeyparam", OracleDbType.Varchar2).Value = objJobConfig.ConfigGUIDkeyVersion;
                        version = Convert.ToInt32(com.ExecuteScalar());
                        return version;
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
            }
            return version;
        }
        public int InsertNewGUID(JobConfigPlugin objJobConfig)
        {
            try
            {
                _log.Info("calling InsertNewGUID");
                using (OracleConnection con = new OracleConnection(Settings.Default.ConnectionStringAPP_MIDB))
                {
                    con.Open();
                    using (var com = con.CreateCommand())
                    {
                        com.Connection = con;

                        var sql1 = Settings.Default.QueryInsertConfigRepos;
                        com.CommandText = sql1;
                        com.CommandType = CommandType.Text;
                        com.BindByName = true;
                        com.Parameters.Add(":configGUIDkey", OracleDbType.Varchar2).Value = objJobConfig.ConfigGUIDkey.ToUpper();
                        com.Parameters.Add(":config", OracleDbType.Varchar2).Value = objJobConfig.Config;

                        int iresult = com.ExecuteNonQuery();
                        return iresult;
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
                return -1;
            }

        }
        public int UpdateGUID(JobConfigPlugin objJobConfig)
        {
            try
            {
                _log.Info("calling UpdateGUID");
                using (OracleConnection con = new OracleConnection(Settings.Default.ConnectionStringAPP_MIDB))
                {
                    con.Open();
                    using (var com = con.CreateCommand())
                    {
                        com.Connection = con;

                        var sql1 = Settings.Default.QueryUpdateConfigRepos;
                        com.CommandText = sql1;
                        com.CommandType = CommandType.Text;
                        com.BindByName = true;
                        com.Parameters.Add(":configGUIDkey", OracleDbType.Varchar2).Value = objJobConfig.ConfigGUIDkey.ToUpper();
                        com.Parameters.Add(":config", OracleDbType.Varchar2).Value = objJobConfig.Config;
                        com.Parameters.Add(":configId", OracleDbType.Varchar2).Value = objJobConfig.ConfigId;
                        int iresult = com.ExecuteNonQuery();
                        return iresult;
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
                return -1;
            }
        }
        public DataSet GetJobMailGroup(JobConfigPlugin objJobConfig)
        {
            DataSet ds = new DataSet();
            //var list = new List<IJobMailGroupdetails>();
            try
            {
                _log.Info("calling GetJobMailGroup");
                using (OracleConnection con = new OracleConnection(Settings.Default.ConnectionStringAPP_MIDB))
                {
                    con.Open();
                    using (var com = con.CreateCommand())
                    {
                        com.Connection = con;
                        var sql = Settings.Default.QuerySelectJobMailGroup;
                        com.CommandText = sql;
                        com.CommandType = CommandType.Text;
                        com.BindByName = true;


                        OracleDataAdapter adap = new OracleDataAdapter(com);
                        adap.Fill(ds);


                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
            }
            return ds;
        }
        public int InsertJobMailGroup(JobConfigPlugin objJobConfig)
        {
            try
            {
                _log.Info("calling InsertJobMailGroup");
                using (OracleConnection con = new OracleConnection(Settings.Default.ConnectionStringAPP_MIDB))
                {
                    con.Open();
                    using (var com = con.CreateCommand())
                    {
                        com.Connection = con;
                        var sql1 = Settings.Default.QueryInsertJobMailGroup;
                        com.CommandText = sql1;
                        com.CommandType = CommandType.Text;
                        com.BindByName = true;
                        com.Parameters.Add(":jobId", OracleDbType.Varchar2).Value = objJobConfig.JobId;
                        com.Parameters.Add(":mailgroupname", OracleDbType.Varchar2).Value = objJobConfig.MailGroupName.ToUpper();
                        com.Parameters.Add(":jobmailstatus", OracleDbType.Varchar2).Value = objJobConfig.MailGroupStatus;
                        int iresult = com.ExecuteNonQuery();
                        return iresult;
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
                return -1;
            }
        }
        public int UpdateJobMailGroup(JobConfigPlugin objJobConfig)
        {
            try
            {
                _log.Info("calling UpdateJobMailGroup");
                using (OracleConnection con = new OracleConnection(Settings.Default.ConnectionStringAPP_MIDB))
                {
                    con.Open();
                    using (var com = con.CreateCommand())
                    {
                        com.Connection = con;
                        var sql1 = Settings.Default.QueryUpdateJobMailGroup;
                        com.CommandText = sql1;
                        com.CommandType = CommandType.Text;
                        com.BindByName = true;
                        com.Parameters.Add(":jobId", OracleDbType.Varchar2).Value = objJobConfig.JobId;
                        com.Parameters.Add(":mailgroupname", OracleDbType.Varchar2).Value = objJobConfig.MailGroupName.ToUpper();
                        com.Parameters.Add(":Id", OracleDbType.Varchar2).Value = objJobConfig.MailGrpId;
                        int iresult = com.ExecuteNonQuery();
                        return iresult;
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
                return -1;
            }
        }
        public int UpdateJobMailingGroup(JobConfigPlugin objJobConfig)
        {
            try
            {
                _log.Info("calling UpdateJobMailingGroup");
                using (OracleConnection con = new OracleConnection(Settings.Default.ConnectionStringAPP_MIDB))
                {
                    con.Open();
                    using (var com = con.CreateCommand())
                    {
                        com.Connection = con;
                        var sql1 = Settings.Default.QueryUpdateJobMailingGroup;
                        com.CommandText = sql1;
                        com.CommandType = CommandType.Text;
                        com.BindByName = true;
                        com.Parameters.Add(":mailgroupname", OracleDbType.Varchar2).Value = objJobConfig.MailGroupName.ToUpper();
                        com.Parameters.Add(":jobmailstatus", OracleDbType.Varchar2).Value = objJobConfig.MailGroupStatus;
                        com.Parameters.Add(":jobId", OracleDbType.Varchar2).Value = objJobConfig.JobId;
                        int iresult = com.ExecuteNonQuery();
                        return iresult;
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
                return -1;
            }
        }
        public int InsertJobStatus(JobConfigPlugin objJobConfig)
        {
            try
            {
                _log.Info("calling InsertJobStatus");
                using (OracleConnection con = new OracleConnection(Settings.Default.ConnectionStringAPP_MIDB))
                {
                    con.Open();
                    using (var com = con.CreateCommand())
                    {
                        com.Connection = con;

                        var sql1 = Settings.Default.QueryInsertJobStatus;

                        com.CommandText = sql1;

                        com.CommandType = CommandType.Text;
                        com.BindByName = true;
                        com.Parameters.Add(":jobname", OracleDbType.Varchar2).Value = objJobConfig.Jobname;

                        int iresult = com.ExecuteNonQuery();
                        return iresult;
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
                return 0;
            }
        }

        public int InsertTrigger(JobConfigPlugin objJobConfig)
        {
            try
            {
                _log.Info("calling InsertTrigger");
                using (OracleConnection con = new OracleConnection(Settings.Default.ConnectionStringAPP_MIDB))
                {
                    con.Open();
                    using (var com = con.CreateCommand())
                    {
                        com.Connection = con;

                        var sql1 = Settings.Default.QueryInsertTrigger;

                        com.CommandText = sql1;

                        com.CommandType = CommandType.Text;
                        com.BindByName = true;
                        com.Parameters.Add(":jobId", OracleDbType.Varchar2).Value = objJobConfig.JobId;
                        com.Parameters.Add(":Trigger_Job_Id", OracleDbType.Int32).Value = objJobConfig.Trigger_Job_Id;
                        com.Parameters.Add(":status", OracleDbType.Varchar2).Value = objJobConfig.Status;

                        int iresult = com.ExecuteNonQuery();
                        return iresult;
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
                return 0;
            }
        }

        public int UpdateTrigger(JobConfigPlugin objJobConfig)
        {
            try
            {
                _log.Info("calling UpdateTrigger");
                using (OracleConnection con = new OracleConnection(Settings.Default.ConnectionStringAPP_MIDB))
                {
                    con.Open();
                    using (var com = con.CreateCommand())
                    {
                        com.Connection = con;

                        var sql1 = Settings.Default.QueryUpdateTrigger;

                        com.CommandText = sql1;

                        com.CommandType = CommandType.Text;
                        com.BindByName = true;
                        com.Parameters.Add(":Trig_Id", OracleDbType.Int32).Value = objJobConfig.Trig_Id;
                        com.Parameters.Add(":jobId", OracleDbType.Int32).Value = objJobConfig.JobId;
                        com.Parameters.Add(":Trigger_Job_Id", OracleDbType.Int32).Value = objJobConfig.Trigger_Job_Id;
                        com.Parameters.Add(":status", OracleDbType.Varchar2).Value = objJobConfig.Status;

                        int iresult = com.ExecuteNonQuery();
                        return iresult;
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
                return 0;
            }
        }

        public int DeleteTrigger(JobConfigPlugin objJobConfig)
        {
            try
            {
                _log.Info("calling DeleteTrigger");
                using (OracleConnection con = new OracleConnection(Settings.Default.ConnectionStringAPP_MIDB))
                {
                    con.Open();
                    using (var com = con.CreateCommand())
                    {
                        com.Connection = con;

                        var sql1 = Settings.Default.QueryDeleteTrigger;
                        com.CommandText = sql1;

                        com.CommandType = CommandType.Text;
                        com.BindByName = true;
                        com.Parameters.Add(":Trig_Id", OracleDbType.Int32).Value = objJobConfig.Trig_Id;

                        int iresult = com.ExecuteNonQuery();
                        return iresult;
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
                return -1;
            }

        }

        public DataSet GetTrigger(JobConfigPlugin objJobConfig)
        {
            DataSet ds = new DataSet();

            try
            {
                _log.Info("calling GetTrigger");
                using (OracleConnection con = new OracleConnection(Settings.Default.ConnectionStringAPP_MIDB))
                {
                    con.Open();
                    using (var com = con.CreateCommand())
                    {
                        com.Connection = con;
                        var sql = Settings.Default.QueryGetTrigger;
                        com.CommandText = sql;
                        com.CommandType = CommandType.Text;
                        com.BindByName = true;
                        com.Parameters.Add(":jobId", OracleDbType.Int32).Value = objJobConfig.JobId;
                        OracleDataAdapter adapter = new OracleDataAdapter(com);
                        adapter.Fill(ds);

                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
            }
            return ds;
        }

        public int UpdateReRunBin(JobConfigPlugin objJobConfig)
        {
            try
            {
                _log.Info("calling UpdateReRun");
                using (OracleConnection con = new OracleConnection(Settings.Default.ConnectionStringAPP_MIDB))
                {
                    con.Open();
                    using (var com = con.CreateCommand())
                    {
                        com.Connection = con;

                        var sql1 = Settings.Default.QueryUpdateJobReRunBin;

                        com.CommandText = sql1;

                        com.CommandType = CommandType.Text;
                        com.BindByName = true;
                        
                        com.Parameters.Add(":jobId", OracleDbType.Int32).Value = objJobConfig.JobId;
                        

                        int iresult = com.ExecuteNonQuery();
                        return iresult;
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
                return 0;
            }
        }

        public int UpdateReRunWeb(JobConfigPlugin objJobConfig)
        {
            try
            {
                _log.Info("calling UpdateReRun");
                using (OracleConnection con = new OracleConnection(Settings.Default.ConnectionStringAPP_MIDB))
                {
                    con.Open();
                    using (var com = con.CreateCommand())
                    {
                        com.Connection = con;

                        var sql1 = Settings.Default.QueryUpdateJobReRunWeb;

                        com.CommandText = sql1;

                        com.CommandType = CommandType.Text;
                        com.BindByName = true;

                        com.Parameters.Add(":jobId", OracleDbType.Int32).Value = objJobConfig.JobId;


                        int iresult = com.ExecuteNonQuery();
                        return iresult;
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
                return 0;
            }
        }
        public DataSet GetProduct()
        {
            DataSet ds = new DataSet();

            try
            {
                _log.Info("calling GetProduct");
        
                using (OracleConnection con = new OracleConnection(Settings.Default.ConnectionStringAPP_MIDW))
                {
                    con.Open();
                    using (var com = con.CreateCommand())
                    {
                        com.Connection = con;
                        var sql = Settings.Default.QueryGetProduct;
                        com.CommandText = sql;
                        com.CommandType = CommandType.Text;
                        com.BindByName = true;
                        OracleDataAdapter adapter = new OracleDataAdapter(com);
                        adapter.Fill(ds);

                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
            }
            return ds;
        }

        public int InsertProduct(JobConfigPlugin objJobConfig)
        {
            try
            {
                _log.Info("calling InsertProduct");
                using (OracleConnection con = new OracleConnection(Settings.Default.ConnectionStringAPP_MIDW))
                {
                    con.Open();
                    using (var com = con.CreateCommand())
                    {
                        com.Connection = con;

                        var sql1 = Settings.Default.QueryInsertProduct;
                        com.CommandText = sql1;

                        com.CommandType = CommandType.Text;
                        com.BindByName = true;
                        com.Parameters.Add(":productname", OracleDbType.Varchar2).Value = objJobConfig.Product_Name;
                        com.Parameters.Add(":description", OracleDbType.Varchar2).Value = objJobConfig.Product_Description;
                        com.Parameters.Add(":prodgroup", OracleDbType.Varchar2).Value = objJobConfig.Product_Group;
                        com.Parameters.Add(":displayprod", OracleDbType.Varchar2).Value = objJobConfig.Display_Product;
                        
                        int iresult = com.ExecuteNonQuery();
                        return iresult;
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
                return -1;
            }
        }
        public int UpdateProduct(JobConfigPlugin objJobConfig)
        {
            try
            {
                _log.Info("calling UpdateProduct");
                using (OracleConnection con = new OracleConnection(Settings.Default.ConnectionStringAPP_MIDW))
                {
                    con.Open();
                    using (var com = con.CreateCommand())
                    {
                        com.Connection = con;
                        var sql1 = Settings.Default.QueryUpdateProduct;
                        com.CommandText = sql1;
                        com.CommandType = CommandType.Text;
                        com.BindByName = true;

                        com.Parameters.Add(":productkey", OracleDbType.Int32).Value = objJobConfig.Product_Key;
                        com.Parameters.Add(":productname", OracleDbType.Varchar2).Value = objJobConfig.Product_Name;
                        com.Parameters.Add(":description", OracleDbType.Varchar2).Value = objJobConfig.Product_Description;
                        com.Parameters.Add(":prodgroup", OracleDbType.Varchar2).Value = objJobConfig.Product_Group;
                        com.Parameters.Add(":displayprod", OracleDbType.Varchar2).Value = objJobConfig.Display_Product;
                        int iresult = com.ExecuteNonQuery();
                        return iresult;
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
                return -1;
            }
        }


        public DataSet GetReport()
        {
            DataSet ds = new DataSet();

            try
            {
                _log.Info("calling GetReport");

                using (OracleConnection con = new OracleConnection(Settings.Default.ConnectionStringAPP_MIDW))
                {
                    con.Open();
                    using (var com = con.CreateCommand())
                    {
                        com.Connection = con;
                        var sql = Settings.Default.QueryGetReport;
                        com.CommandText = sql;
                        com.CommandType = CommandType.Text;
                        com.BindByName = true;
                        OracleDataAdapter adapter = new OracleDataAdapter(com);
                        adapter.Fill(ds);

                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
            }
            return ds;
        }

        public int InsertReport(JobConfigPlugin objJobConfig)
        {
            try
            {
                _log.Info("calling InsertReport");
                using (OracleConnection con = new OracleConnection(Settings.Default.ConnectionStringAPP_MIDW))
                {
                    con.Open();
                    using (var com = con.CreateCommand())
                    {
                        com.Connection = con;

                        var sql1 = Settings.Default.QueryInsertReport;
                        com.CommandText = sql1;

                        com.CommandType = CommandType.Text;
                        com.BindByName = true;
                        com.Parameters.Add(":reportname", OracleDbType.Varchar2).Value = objJobConfig.Report_Name;
                        com.Parameters.Add(":description", OracleDbType.Varchar2).Value = objJobConfig.Report_Description;
                        com.Parameters.Add(":priority", OracleDbType.Varchar2).Value = objJobConfig.Report_Priority;

                        int iresult = com.ExecuteNonQuery();
                        return iresult;
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
                return -1;
            }
        }
        public int UpdateReport(JobConfigPlugin objJobConfig)
        {
            try
            {
                _log.Info("calling UpdateReport");
                using (OracleConnection con = new OracleConnection(Settings.Default.ConnectionStringAPP_MIDW))
                {
                    con.Open();
                    using (var com = con.CreateCommand())
                    {
                        com.Connection = con;
                        var sql1 = Settings.Default.QueryUpdateReport;
                        com.CommandText = sql1;
                        com.CommandType = CommandType.Text;
                        com.BindByName = true;

                        com.Parameters.Add(":reportkey", OracleDbType.Int32).Value = objJobConfig.Report_key;
                        com.Parameters.Add(":reportname", OracleDbType.Varchar2).Value = objJobConfig.Report_Name;
                        com.Parameters.Add(":description", OracleDbType.Varchar2).Value = objJobConfig.Report_Description;
                        com.Parameters.Add(":priority", OracleDbType.Varchar2).Value = objJobConfig.Report_Priority;

                        int iresult = com.ExecuteNonQuery();
                        return iresult;
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
                return -1;
            }
        }

        public DataSet GetSource()
        {
            DataSet ds = new DataSet();

            try
            {
                _log.Info("calling GetSource");

                using (OracleConnection con = new OracleConnection(Settings.Default.ConnectionStringAPP_MIDW))
                {
                    con.Open();
                    using (var com = con.CreateCommand())
                    {
                        com.Connection = con;
                        var sql = Settings.Default.QueryGetSource;
                        com.CommandText = sql;
                        com.CommandType = CommandType.Text;
                        com.BindByName = true;
                        OracleDataAdapter adapter = new OracleDataAdapter(com);
                        adapter.Fill(ds);

                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
            }
            return ds;
        }

        public int InsertSource(JobConfigPlugin objJobConfig)
        {
            try
            {
                _log.Info("calling InsertSource");
                using (OracleConnection con = new OracleConnection(Settings.Default.ConnectionStringAPP_MIDW))
                {
                    con.Open();
                    using (var com = con.CreateCommand())
                    {
                        com.Connection = con;

                        var sql1 = Settings.Default.QueryInsertSource;
                        com.CommandText = sql1;

                        com.CommandType = CommandType.Text;
                        com.BindByName = true;
                        com.Parameters.Add(":sourcename", OracleDbType.Varchar2).Value = objJobConfig.Source_Name; 
                        com.Parameters.Add(":description", OracleDbType.Varchar2).Value = objJobConfig.Source_Description;
                        com.Parameters.Add(":displaysource", OracleDbType.Varchar2).Value = objJobConfig.Display_Source;

                        int iresult = com.ExecuteNonQuery();
                        return iresult;
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
                return -1;
            }
        }
        public int UpdateSource(JobConfigPlugin objJobConfig)
        {
            try
            {
                _log.Info("calling UpdateSource");
                using (OracleConnection con = new OracleConnection(Settings.Default.ConnectionStringAPP_MIDW))
                {
                    con.Open();
                    using (var com = con.CreateCommand())
                    {
                        com.Connection = con;
                        var sql1 = Settings.Default.QueryUpdateSource;
                        com.CommandText = sql1;
                        com.CommandType = CommandType.Text;
                        com.BindByName = true;

                        com.Parameters.Add(":sourcekey", OracleDbType.Int32).Value = objJobConfig.Source_Key;
                        com.Parameters.Add(":sourcename", OracleDbType.Varchar2).Value = objJobConfig.Source_Name;
                        com.Parameters.Add(":description", OracleDbType.Varchar2).Value = objJobConfig.Source_Description;
                        com.Parameters.Add(":displaysource", OracleDbType.Varchar2).Value = objJobConfig.Display_Source;
                        int iresult = com.ExecuteNonQuery();
                        return iresult;
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
                return -1;
            }
        }
        public DataSet GetMailingNameWithStatus(JobConfigPlugin objJobConfig)
        {
            DataSet ds = new DataSet();
            //var list = new List<IJobMailGroupdetails>();
            try
            {
                _log.Info("calling GetMailingListWithJobId");
                using (OracleConnection con = new OracleConnection(Settings.Default.ConnectionStringAPP_MIDB))
                {
                    con.Open();
                    using (var com = con.CreateCommand())
                    {
                        com.Connection = con;
                        var sql = Settings.Default.QueryGetJobMailStatus;
                        com.CommandText = sql;
                        com.CommandType = CommandType.Text;
                        com.BindByName = true;
                        com.Parameters.Add(":jobId", OracleDbType.Int32).Value = objJobConfig.JobId;
                        OracleDataAdapter adap = new OracleDataAdapter(com);
                        adap.Fill(ds);

                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
            }
            return ds;
        }
    }
}




