﻿namespace CCI.MIP.JobPlugin.UI
{
    partial class frmConfiguration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmConfiguration));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rtbscript = new System.Windows.Forms.RichTextBox();
            this.rchEditControl = new DevExpress.XtraRichEdit.RichEditControl();
            this.cmbPropName = new System.Windows.Forms.ComboBox();
            this.txtPropName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtGUIDfullname = new System.Windows.Forms.TextBox();
            this.txtseqnumber = new System.Windows.Forms.TextBox();
            this.txtVersion = new System.Windows.Forms.TextBox();
            this.txtPropId_config = new System.Windows.Forms.TextBox();
            this.txtPluginId = new System.Windows.Forms.TextBox();
            this.txtconfigID = new System.Windows.Forms.TextBox();
            this.txtScript = new System.Windows.Forms.TextBox();
            this.txtVersionGUID = new System.Windows.Forms.TextBox();
            this.txtpropGUID = new System.Windows.Forms.TextBox();
            this.txtGUID = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btNew = new System.Windows.Forms.Button();
            this.btClear = new System.Windows.Forms.Button();
            this.btUpdate = new System.Windows.Forms.Button();
            this.btAdd = new System.Windows.Forms.Button();
            this.btSearch = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.groupBox1.Controls.Add(this.rtbscript);
            this.groupBox1.Controls.Add(this.rchEditControl);
            this.groupBox1.Controls.Add(this.cmbPropName);
            this.groupBox1.Controls.Add(this.txtPropName);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtGUIDfullname);
            this.groupBox1.Controls.Add(this.txtseqnumber);
            this.groupBox1.Controls.Add(this.txtVersion);
            this.groupBox1.Controls.Add(this.txtPropId_config);
            this.groupBox1.Controls.Add(this.txtPluginId);
            this.groupBox1.Controls.Add(this.txtconfigID);
            this.groupBox1.Controls.Add(this.btNew);
            this.groupBox1.Controls.Add(this.btClear);
            this.groupBox1.Controls.Add(this.btAdd);
            this.groupBox1.Controls.Add(this.btSearch);
            this.groupBox1.Controls.Add(this.txtScript);
            this.groupBox1.Controls.Add(this.txtVersionGUID);
            this.groupBox1.Controls.Add(this.txtpropGUID);
            this.groupBox1.Controls.Add(this.txtGUID);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btUpdate);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(565, 541);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // rtbscript
            // 
            this.rtbscript.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rtbscript.Location = new System.Drawing.Point(90, 114);
            this.rtbscript.Name = "rtbscript";
            this.rtbscript.Size = new System.Drawing.Size(376, 406);
            this.rtbscript.TabIndex = 7;
            this.rtbscript.Text = "";
            // 
            // rchEditControl
            // 
            this.rchEditControl.ActiveViewType = DevExpress.XtraRichEdit.RichEditViewType.Simple;
            this.rchEditControl.Appearance.Text.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rchEditControl.Appearance.Text.Options.UseFont = true;
            this.rchEditControl.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.rchEditControl.Location = new System.Drawing.Point(90, 518);
            this.rchEditControl.LookAndFeel.UseDefaultLookAndFeel = false;
            this.rchEditControl.Name = "rchEditControl";
            this.rchEditControl.Options.Fields.UseCurrentCultureDateTimeFormat = false;
            this.rchEditControl.Options.MailMerge.KeepLastParagraph = false;
            this.rchEditControl.Size = new System.Drawing.Size(376, 17);
            this.rchEditControl.TabIndex = 6;
            this.rchEditControl.Visible = false;
            // 
            // cmbPropName
            // 
            this.cmbPropName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPropName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbPropName.FormattingEnabled = true;
            this.cmbPropName.Items.AddRange(new object[] {
            "XSLT",
            "JAVA",
            "SELENIUM",
            "QUERY"});
            this.cmbPropName.Location = new System.Drawing.Point(91, 29);
            this.cmbPropName.Name = "cmbPropName";
            this.cmbPropName.Size = new System.Drawing.Size(134, 24);
            this.cmbPropName.TabIndex = 2;
            this.cmbPropName.SelectedIndexChanged += new System.EventHandler(this.cmbPropName_SelectedIndexChanged);
            // 
            // txtPropName
            // 
            this.txtPropName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPropName.Enabled = false;
            this.txtPropName.Location = new System.Drawing.Point(278, 29);
            this.txtPropName.Name = "txtPropName";
            this.txtPropName.Size = new System.Drawing.Size(82, 20);
            this.txtPropName.TabIndex = 0;
            this.txtPropName.Text = "XSLT";
            this.txtPropName.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Location = new System.Drawing.Point(7, 36);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 15);
            this.label3.TabIndex = 1;
            this.label3.Text = "PROP NAME";
            // 
            // txtGUIDfullname
            // 
            this.txtGUIDfullname.Location = new System.Drawing.Point(496, 256);
            this.txtGUIDfullname.Name = "txtGUIDfullname";
            this.txtGUIDfullname.Size = new System.Drawing.Size(39, 20);
            this.txtGUIDfullname.TabIndex = 5;
            this.txtGUIDfullname.Visible = false;
            // 
            // txtseqnumber
            // 
            this.txtseqnumber.Location = new System.Drawing.Point(496, 230);
            this.txtseqnumber.Name = "txtseqnumber";
            this.txtseqnumber.Size = new System.Drawing.Size(39, 20);
            this.txtseqnumber.TabIndex = 5;
            this.txtseqnumber.Visible = false;
            // 
            // txtVersion
            // 
            this.txtVersion.Location = new System.Drawing.Point(496, 204);
            this.txtVersion.Name = "txtVersion";
            this.txtVersion.Size = new System.Drawing.Size(39, 20);
            this.txtVersion.TabIndex = 5;
            this.txtVersion.Visible = false;
            // 
            // txtPropId_config
            // 
            this.txtPropId_config.Location = new System.Drawing.Point(525, 66);
            this.txtPropId_config.Name = "txtPropId_config";
            this.txtPropId_config.Size = new System.Drawing.Size(39, 20);
            this.txtPropId_config.TabIndex = 5;
            this.txtPropId_config.Visible = false;
            // 
            // txtPluginId
            // 
            this.txtPluginId.Location = new System.Drawing.Point(496, 45);
            this.txtPluginId.Name = "txtPluginId";
            this.txtPluginId.Size = new System.Drawing.Size(39, 20);
            this.txtPluginId.TabIndex = 5;
            this.txtPluginId.Visible = false;
            // 
            // txtconfigID
            // 
            this.txtconfigID.Location = new System.Drawing.Point(496, 19);
            this.txtconfigID.Name = "txtconfigID";
            this.txtconfigID.Size = new System.Drawing.Size(39, 20);
            this.txtconfigID.TabIndex = 5;
            this.txtconfigID.Visible = false;
            // 
            // txtScript
            // 
            this.txtScript.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtScript.Location = new System.Drawing.Point(472, 455);
            this.txtScript.Multiline = true;
            this.txtScript.Name = "txtScript";
            this.txtScript.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtScript.Size = new System.Drawing.Size(386, 65);
            this.txtScript.TabIndex = 3;
            this.txtScript.Visible = false;
            // 
            // txtVersionGUID
            // 
            this.txtVersionGUID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVersionGUID.Enabled = false;
            this.txtVersionGUID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVersionGUID.Location = new System.Drawing.Point(402, 73);
            this.txtVersionGUID.Name = "txtVersionGUID";
            this.txtVersionGUID.Size = new System.Drawing.Size(64, 22);
            this.txtVersionGUID.TabIndex = 1;
            // 
            // txtpropGUID
            // 
            this.txtpropGUID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtpropGUID.Enabled = false;
            this.txtpropGUID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtpropGUID.Location = new System.Drawing.Point(90, 73);
            this.txtpropGUID.Name = "txtpropGUID";
            this.txtpropGUID.Size = new System.Drawing.Size(97, 22);
            this.txtpropGUID.TabIndex = 1;
            // 
            // txtGUID
            // 
            this.txtGUID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtGUID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGUID.Location = new System.Drawing.Point(188, 73);
            this.txtGUID.Name = "txtGUID";
            this.txtGUID.Size = new System.Drawing.Size(213, 22);
            this.txtGUID.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label2.Location = new System.Drawing.Point(7, 117);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 15);
            this.label2.TabIndex = 0;
            this.label2.Text = "SCRIPT";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Location = new System.Drawing.Point(7, 73);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "GUID";
            // 
            // btNew
            // 
            this.btNew.BackColor = System.Drawing.Color.Transparent;
            this.btNew.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btNew.Image = global::CCI.MIP.JobPlugin.UI.Properties.Resources.newtask_16x16;
            this.btNew.Location = new System.Drawing.Point(486, 89);
            this.btNew.Name = "btNew";
            this.btNew.Size = new System.Drawing.Size(73, 26);
            this.btNew.TabIndex = 4;
            this.btNew.Text = "New";
            this.btNew.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btNew.UseVisualStyleBackColor = false;
            this.btNew.Click += new System.EventHandler(this.btNew_Click);
            // 
            // btClear
            // 
            this.btClear.BackColor = System.Drawing.Color.Transparent;
            this.btClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btClear.Image = global::CCI.MIP.JobPlugin.UI.Properties.Resources.cancel_16x16;
            this.btClear.Location = new System.Drawing.Point(486, 145);
            this.btClear.Name = "btClear";
            this.btClear.Size = new System.Drawing.Size(73, 26);
            this.btClear.TabIndex = 4;
            this.btClear.Text = "Clear";
            this.btClear.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btClear.UseVisualStyleBackColor = false;
            this.btClear.Visible = false;
            this.btClear.Click += new System.EventHandler(this.btClear_Click);
            // 
            // btUpdate
            // 
            this.btUpdate.BackColor = System.Drawing.Color.Transparent;
            this.btUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btUpdate.Image = global::CCI.MIP.JobPlugin.UI.Properties.Resources.save_16x16;
            this.btUpdate.Location = new System.Drawing.Point(486, 117);
            this.btUpdate.Name = "btUpdate";
            this.btUpdate.Size = new System.Drawing.Size(73, 26);
            this.btUpdate.TabIndex = 4;
            this.btUpdate.Text = "Update";
            this.btUpdate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btUpdate.UseVisualStyleBackColor = false;
            this.btUpdate.Visible = false;
            this.btUpdate.Click += new System.EventHandler(this.btUpdate_Click);
            // 
            // btAdd
            // 
            this.btAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btAdd.Image = global::CCI.MIP.JobPlugin.UI.Properties.Resources.saveandnew_16x16;
            this.btAdd.Location = new System.Drawing.Point(486, 117);
            this.btAdd.Name = "btAdd";
            this.btAdd.Size = new System.Drawing.Size(69, 22);
            this.btAdd.TabIndex = 4;
            this.btAdd.Text = "Save";
            this.btAdd.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btAdd.UseVisualStyleBackColor = true;
            this.btAdd.Visible = false;
            this.btAdd.Click += new System.EventHandler(this.btAdd_Click);
            // 
            // btSearch
            // 
            this.btSearch.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btSearch.Image = global::CCI.MIP.JobPlugin.UI.Properties.Resources.zoom_16x16;
            this.btSearch.Location = new System.Drawing.Point(466, 17);
            this.btSearch.Name = "btSearch";
            this.btSearch.Size = new System.Drawing.Size(69, 22);
            this.btSearch.TabIndex = 3;
            this.btSearch.Text = "Search";
            this.btSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btSearch.UseVisualStyleBackColor = true;
            this.btSearch.Visible = false;
            this.btSearch.Click += new System.EventHandler(this.btSearch_Click);
            // 
            // frmConfiguration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(565, 541);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmConfiguration";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Configuration";
            this.Load += new System.EventHandler(this.frmConfiguration_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }
        #endregion
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btSearch;
        private System.Windows.Forms.Button btClear;
        public System.Windows.Forms.TextBox txtScript;
        public System.Windows.Forms.TextBox txtGUID;
        public System.Windows.Forms.TextBox txtconfigID;
        public System.Windows.Forms.Button btUpdate;
        public System.Windows.Forms.Button btAdd;
        public System.Windows.Forms.TextBox txtPropName;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.TextBox txtPluginId;
        public System.Windows.Forms.TextBox txtPropId_config;
        public System.Windows.Forms.Button btNew;
        public System.Windows.Forms.TextBox txtVersionGUID;
        public System.Windows.Forms.TextBox txtpropGUID;
        public System.Windows.Forms.TextBox txtVersion;
        public System.Windows.Forms.ComboBox cmbPropName;
        public System.Windows.Forms.TextBox txtseqnumber;
        public System.Windows.Forms.TextBox txtGUIDfullname;
        public DevExpress.XtraRichEdit.RichEditControl rchEditControl;
        public System.Windows.Forms.RichTextBox rtbscript;
    }
}
