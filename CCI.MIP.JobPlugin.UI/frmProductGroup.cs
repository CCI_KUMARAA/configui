﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CCI.MIP.JobPlugin.UI
{
    
     
    public partial class frmProductGroup : Form
    {
        JobConfigWinService.JobConfigServiceClient jobref = new JobConfigWinService.JobConfigServiceClient();
        JobConfigWinService.JobConfigPlugin jobscd = new JobConfigWinService.JobConfigPlugin();
        private static readonly log4net.ILog _log = log4net.LogManager.GetLogger(typeof(frmProductGroup));

        public frmProductGroup()
        {
            InitializeComponent();
        }

        private void btNew_Click(object sender, EventArgs e)
        {
            try
            {
                _log.Info("calling btNew_Click");
                clearcontrol();
                
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred " + ex.Message, "Product", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);

            }
        }


        private void clearcontrol()
        {
            try
            {
                _log.Info("calling btNew_Click");
                txtProductKey.Text = "";
                txtProductName.Text = "";
                txtDescription.Text = "";
                cmbProductGrp.SelectedIndex = -1;
                txtDisplayProduct.Text = "";
                btSave.Visible = true;
                btUpdate.Visible = false;


            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred " + ex.Message, "Product", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);

            }
        }


        private void frmProductGroup_Load(object sender, EventArgs e)
        {
            try
            {
                _log.Info("calling frmProductGroup_Load");
                RefreshGrid();
                
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred " + ex.Message, "Product", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);

            }
        }


        private void RefreshGrid()
        {
            try
            {
                _log.Info("calling frmProductGroup_Load");
                DataSet dsprod = jobref.GetProduct();
                if (dsprod.Tables.Count > 0)
                {
                    if (dsprod.Tables[0].Rows.Count > 0)
                    {
                        Product_Key.FieldName = dsprod.Tables[0].Columns[0].ToString();
                        ProductName.FieldName = dsprod.Tables[0].Columns[1].ToString();
                        Display_Product.FieldName = dsprod.Tables[0].Columns[4].ToString();
                        Description.FieldName = dsprod.Tables[0].Columns[2].ToString();
                        Group.FieldName = dsprod.Tables[0].Columns[3].ToString();
                        grdconrolProduct.DataSource = dsprod.Tables[0];
                        btSave.Visible = false;
                        btUpdate.Visible = true;

                    }

                }


            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred " + ex.Message, "Product", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);

            }
        }
        private void btSave_Click(object sender, EventArgs e)
        {
            try
            {
                _log.Info("calling btSave_Click");
                if (txtProductName.Text != "" && cmbProductGrp.SelectedIndex!=-1)
                {
                    jobscd.Product_Group = cmbProductGrp.SelectedItem.ToString();
                    jobscd.Product_Name = txtProductName.Text;
                    jobscd.Product_Description = txtDescription.Text;
                    jobscd.Display_Product = txtDisplayProduct.Text;
                    int iresult = jobref.InsertProduct(jobscd);
                    if (iresult == 1)
                    {
                        MessageBox.Show("Record saved successfully.", "Product", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        RefreshGrid();
                        clearcontrol();
                        btSave.Visible = false;
                        btUpdate.Visible = true;
                    }
                    else
                    {
                        MessageBox.Show("Error occurs while saving.", "Product", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }

                }
                else
                {
                    MessageBox.Show("Please Enter Product Name.", "Product", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred " + ex.Message, "Product", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);

            }
        }

        private void btUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                _log.Info("calling btUpdate_Click");
                if (txtProductName.Text != "" && cmbProductGrp.SelectedIndex != -1 && txtProductKey.Text!="")
                {
                    jobscd.Product_Group = cmbProductGrp.SelectedItem.ToString();
                    jobscd.Product_Name = txtProductName.Text;
                    jobscd.Product_Description = txtDescription.Text;
                    jobscd.Product_Key = Convert.ToInt32( txtProductKey.Text);
                    jobscd.Display_Product = txtDisplayProduct.Text;
                    int iresult = jobref.UpdateProduct(jobscd);
                    if (iresult == 1)
                    {
                        MessageBox.Show("Record Updated successfully.", "Product", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        RefreshGrid();
                        clearcontrol();
                        btSave.Visible = false;
                        btUpdate.Visible = true;
                    }
                    else
                    {
                        MessageBox.Show("Error occurs while updating.", "Product", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }

                }
                else
                {
                    MessageBox.Show("Please Select Product.", "Product", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred " + ex.Message, "Product", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);

            }
        }

        private void grdViewProduct_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {

        }

        private void grdViewProduct_Click(object sender, EventArgs e)
        {
            txtProductKey.Text = (grdViewProduct.GetFocusedDataRow()["Product_Key"].ToString());
            txtProductName.Text = (grdViewProduct.GetFocusedDataRow()["Name"].ToString());
            cmbProductGrp.SelectedItem = (grdViewProduct.GetFocusedDataRow()["Product_Group"].ToString());
            txtDescription.Text = (grdViewProduct.GetFocusedDataRow()["Description"].ToString());
            txtDisplayProduct.Text = (grdViewProduct.GetFocusedDataRow()["Display_Product"].ToString());
        }
    }
}
