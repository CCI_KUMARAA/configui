﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows;
using DevExpress.XtraRichEdit;


namespace CCI.MIP.JobPlugin.UI
{
    public partial class frmConfiguration : Form
    {
        private static readonly log4net.ILog _log = log4net.LogManager.GetLogger(typeof(frmConfiguration));
        JobConfigWinService.JobConfigServiceClient jobref = new JobConfigWinService.JobConfigServiceClient();
        JobConfigWinService.JobConfigPlugin jobscd = new JobConfigWinService.JobConfigPlugin();
        public frmConfiguration()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void btSearch_Click(object sender, EventArgs e)
        {
            try
            {
                _log.Info("calling btSearch_Click");
                jobscd = new JobConfigWinService.JobConfigPlugin();
                jobscd.ConfigGUIDkey = txtGUID.Text;
                DataSet ds = jobref.GetConfigReposGUID(jobscd);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    txtScript.Text = ds.Tables[0].Rows[0].ItemArray[2].ToString();
                    txtconfigID.Text = ds.Tables[0].Rows[0].ItemArray[0].ToString();

                }
                else
                {
                    MessageBox.Show("GUID  Not found!", "GUID", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred while trying to save." + ex.Message, "Configuration", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);
            }
        }

        private void btAdd_Click(object sender, EventArgs e)
        {
            try
            {
                _log.Info("calling btAdd_Click");
                jobscd = new JobConfigWinService.JobConfigPlugin();
                UserControlCapture uc = new UserControlCapture();
                frmJobSchedule fm = new frmJobSchedule();


                if (txtGUID.Text != "" && rtbscript.Text!="")//rchEditControl.Text != "")
                {
                    string strpropvalue = txtpropGUID.Text + txtGUID.Text.ToUpper() + txtVersionGUID.Text;

                    jobscd.ConfigGUIDkey = strpropvalue;

                    jobscd.Config = rtbscript.Text;//rchEditControl.Text;
                    txtGUIDfullname.Text = strpropvalue;
                    int iresult = jobref.InsertNewGUID(jobscd);
                    if (iresult == 1)
                    {
                        jobscd.PluginId = Convert.ToInt32(txtPluginId.Text);
                        jobscd.PluginConfigId = Convert.ToInt32(txtPluginId.Text);
                        if (cmbPropName.SelectedItem.ToString() == "XSLT")
                        {
                            jobscd.PropName = "XSLT";
                        }
                        else if (cmbPropName.SelectedItem.ToString() == "JAVA" || cmbPropName.SelectedItem.ToString() == "SELENIUM")
                        {
                            jobscd.PropName = "CodeName";
                        }
                        else if (cmbPropName.SelectedItem.ToString() == "QUERY")
                        {
                            jobscd.PropName = "QUERY";
                        }

                        jobscd.PropValue = strpropvalue;
                        //int iresultprop = jobref.InsertJobPluginProp(jobscd);
                        int iresultprop = 0;
                        //DataSet ds = jobref.GetJobPluginPropWitPluginId(jobscd);

                        //if (ds.Tables.Count > 0)
                        //{
                        if (txtPropId_config.Text != "")
                        {
                            jobscd.PropId = Convert.ToInt32(txtPropId_config.Text);
                            iresultprop = jobref.UpdateJobPluginProp(jobscd);
                        }
                        else
                        {
                            iresultprop = jobref.InsertJobPluginProp(jobscd);
                        }
                        //}


                        if (iresultprop == 1)
                        {
                            MessageBox.Show("Record inserted successfully!", "Configuration", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            //OnClear();
                            this.Close();
                            //uc.lblConfig.Text = strGUID;
                        }
                    }
                }
                else
                {
                    if (txtGUID.Text == "")
                    {
                        MessageBox.Show("Please enter GUID", "Configuration", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else if (rtbscript.Text=="")//rchEditControl.Text == "")
                    {
                        MessageBox.Show("Please enter Script", "Configuration", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred while trying to save.-" + ex.Message, "Configuration", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);
            }

        }

        private void btUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                _log.Info("calling btUpdate_Click");
                jobscd = new JobConfigWinService.JobConfigPlugin();
                jobscd.PluginId = Convert.ToInt32(txtPluginId.Text);
                jobscd.PluginConfigId = Convert.ToInt32(txtPluginId.Text);
                jobscd.PropId = Convert.ToInt32(txtPropId_config.Text);
                //jobscd.PropName = txtPropName.Text;
                jobscd.ConfigGUIDkeyVersion=txtpropGUID.Text + txtGUID.Text.ToUpper() + "-" + txtseqnumber.Text + "-";
                int newversion = jobref.GetConfigReposGUIDVersion(jobscd);//Convert.ToInt32(txtVersion.Text) + 1;
                string strpropvalue = txtpropGUID.Text + txtGUID.Text.ToUpper() + "-" + txtseqnumber.Text + "-" + newversion;
                jobscd.PropValue = strpropvalue;
                txtGUIDfullname.Text = strpropvalue;

                jobscd.ConfigGUIDkey = strpropvalue;
                jobscd.Config = rtbscript.Text;//rchEditControl.Text;//txtScript.Text;
                jobscd.ConfigId = Convert.ToInt32(txtconfigID.Text);

                if (cmbPropName.SelectedItem.ToString() == "XSLT")
                {
                    jobscd.PropName = "XSLT";
                }
                else if (cmbPropName.SelectedItem.ToString() == "JAVA" || cmbPropName.SelectedItem.ToString() == "SELENIUM")
                {
                    jobscd.PropName = "CodeName";
                }
                else if (cmbPropName.SelectedItem.ToString() == "QUERY")
                {
                    jobscd.PropName = "QUERY";
                }

                if (strpropvalue != "" && rtbscript.Text!="")// rchEditControl.Text != "")
                {
                    //int iUpdateGUID = jobref.UpdateGUID(jobscd);
                    int iUpdateGUID = jobref.InsertNewGUID(jobscd);
                    int iupdateprop = jobref.UpdateJobPluginProp(jobscd);
                    if (iUpdateGUID == 1 && iupdateprop == 1)
                    {
                        MessageBox.Show("Record Updated successfully!", "Configuration", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        //OnClear();
                        this.Close();


                    }
                }
                else
                {
                    if (txtGUID.Text == "")
                    {
                        MessageBox.Show("Please enter GUID", "Configuration", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else if (rtbscript.Text=="")//rchEditControl.Text == "")
                    {
                        MessageBox.Show("Please enter Script", "Configuration", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred while trying to update." + ex.Message, "Configuration", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);

            }
        }
        public void OnClear()
        {
            txtconfigID.Text = string.Empty;
            txtGUID.Text = string.Empty;
            txtScript.Text = string.Empty;
            rchEditControl.Text = string.Empty;
            txtpropGUID.Text = string.Empty;
            txtVersionGUID.Text = string.Empty;
            rtbscript.Text = string.Empty;
        }

        private void btClear_Click(object sender, EventArgs e)
        {
            OnClear();
        }

        private void btNew_Click(object sender, EventArgs e)
        {

            OnClear();
            btUpdate.Visible = false;
            btAdd.Visible = true;
            txtVersion.Text = "1";
            var fm = frmJobSchedule.MainFormRef;
            string strjobname = Program.JobName;//fm.txtJobName.Text;
            strjobname = strjobname.Replace('-', '_');
            strjobname = strjobname.Replace(' ', '_');
            txtGUID.Text = strjobname;

        }

        private void cmbPropName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                _log.Info("calling btUpdate_Click");
                
                string[] strtab = Program.tabselectedvalue.Split('-');

                if (strtab[0] == "Capture")
                {
                    txtpropGUID.Text = "C-" + cmbPropName.SelectedItem.ToString() + "-";
                    txtVersionGUID.Text = "-" + strtab[1].TrimStart() + "-" + txtVersion.Text;


                }
                else if (strtab[0] == "Transform")
                {
                    txtpropGUID.Text = "T-" + cmbPropName.SelectedItem.ToString() + "-";
                    txtVersionGUID.Text = "-" + strtab[1].TrimStart() + "-" + txtVersion.Text;

                }
                else if (strtab[0] == "Load")
                {
                    txtpropGUID.Text = "L-" + cmbPropName.SelectedItem.ToString() + "-";
                    txtVersionGUID.Text = "-" + strtab[1].TrimStart() + "-" + txtVersion.Text;

                }
            }
            catch (Exception ex)
            {
            }
        }

        public class HighlightColors
        {
            public static Color HC_NODE = Color.Firebrick;
            public static Color HC_STRING = Color.Blue;
            public static Color HC_ATTRIBUTE = Color.Red;
            public static Color HC_COMMENT = Color.GreenYellow;
            public static Color HC_INNERTEXT = Color.Black;
        }

        public static void HighlightRTF(RichTextBox rtb)
        {
            int k = 0;

            string str = rtb.Text;

            int st, en;
            int lasten = -1;
            while (k < str.Length)
            {
                st = str.IndexOf('<', k);

                if (st < 0)
                    break;

                if (lasten > 0)
                {
                    rtb.Select(lasten + 1, st - lasten - 1);
                    rtb.SelectionColor = HighlightColors.HC_INNERTEXT;
                }

                en = str.IndexOf('>', st + 1);
                if (en < 0)
                    break;

                k = en + 1;
                lasten = en;

                if (str[st + 1] == '!')
                {
                    rtb.Select(st + 1, en - st - 1);
                    rtb.SelectionColor = HighlightColors.HC_COMMENT;
                    continue;

                }
                String nodeText = str.Substring(st + 1, en - st - 1);


                bool inString = false;

                int lastSt = -1;
                int state = 0;
                /* 0 = before node name
                 * 1 = in node name
                   2 = after node name
                   3 = in attribute
                   4 = in string
                   */
                int startNodeName = 0, startAtt = 0;
                for (int i = 0; i < nodeText.Length; ++i)
                {
                    if (nodeText[i] == '"')
                        inString = !inString;

                    if (inString && nodeText[i] == '"')
                        lastSt = i;
                    else
                        if (nodeText[i] == '"')
                        {
                            rtb.Select(lastSt + st + 2, i - lastSt - 1);
                            rtb.SelectionColor = HighlightColors.HC_STRING;
                        }

                    switch (state)
                    {
                        case 0:
                            if (!Char.IsWhiteSpace(nodeText, i))
                            {
                                startNodeName = i;
                                state = 1;
                            }
                            break;
                        case 1:
                            if (Char.IsWhiteSpace(nodeText, i))
                            {
                                rtb.Select(startNodeName + st, i - startNodeName + 1);
                                rtb.SelectionColor = HighlightColors.HC_NODE;
                                state = 2;
                            }
                            break;
                        case 2:
                            if (!Char.IsWhiteSpace(nodeText, i))
                            {
                                startAtt = i;
                                state = 3;
                            }
                            break;

                        case 3:
                            if (Char.IsWhiteSpace(nodeText, i) || nodeText[i] == '=')
                            {
                                rtb.Select(startAtt + st, i - startAtt + 1);
                                rtb.SelectionColor = HighlightColors.HC_ATTRIBUTE;
                                state = 4;
                            }
                            break;
                        case 4:
                            if (nodeText[i] == '"' && !inString)
                                state = 2;
                            break;


                    }

                }
                if (state == 1)
                {
                    rtb.Select(st + 1, nodeText.Length);
                    rtb.SelectionColor = HighlightColors.HC_NODE;
                }


            }
        }

        private void frmConfiguration_Load(object sender, EventArgs e)
        {
            HighlightRTF(rtbscript);
        }
    }
}
