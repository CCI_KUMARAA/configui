﻿namespace CCI.MIP.JobPlugin.UI
{
    partial class frmMailingScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMailingScreen));
            this.rdoExisting = new System.Windows.Forms.RadioButton();
            this.cmbMailGroup = new System.Windows.Forms.ComboBox();
            this.btSave = new System.Windows.Forms.Button();
            this.gvMailing = new System.Windows.Forms.DataGridView();
            this.rdoNew = new System.Windows.Forms.RadioButton();
            this.textBox1 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.gvMailing)).BeginInit();
            this.SuspendLayout();
            // 
            // rdoExisting
            // 
            this.rdoExisting.AutoSize = true;
            this.rdoExisting.BackColor = System.Drawing.Color.Transparent;
            this.rdoExisting.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdoExisting.ForeColor = System.Drawing.SystemColors.ControlText;
            this.rdoExisting.Location = new System.Drawing.Point(38, 28);
            this.rdoExisting.Name = "rdoExisting";
            this.rdoExisting.Size = new System.Drawing.Size(117, 19);
            this.rdoExisting.TabIndex = 0;
            this.rdoExisting.TabStop = true;
            this.rdoExisting.Text = "Existing group";
            this.rdoExisting.UseVisualStyleBackColor = false;
            this.rdoExisting.CheckedChanged += new System.EventHandler(this.rdoExisting_CheckedChanged);
            // 
            // cmbMailGroup
            // 
            this.cmbMailGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMailGroup.FormattingEnabled = true;
            this.cmbMailGroup.Location = new System.Drawing.Point(151, 27);
            this.cmbMailGroup.Name = "cmbMailGroup";
            this.cmbMailGroup.Size = new System.Drawing.Size(265, 21);
            this.cmbMailGroup.TabIndex = 2;
            this.cmbMailGroup.SelectedIndexChanged += new System.EventHandler(this.cmbMailGroup_SelectedIndexChanged);
            // 
            // btSave
            // 
            this.btSave.BackColor = System.Drawing.Color.Transparent;
            this.btSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btSave.Image = global::CCI.MIP.JobPlugin.UI.Properties.Resources.save_16x16;
            this.btSave.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btSave.Location = new System.Drawing.Point(364, 301);
            this.btSave.Name = "btSave";
            this.btSave.Size = new System.Drawing.Size(85, 30);
            this.btSave.TabIndex = 17;
            this.btSave.Text = "Save";
            this.btSave.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btSave.UseVisualStyleBackColor = false;
            this.btSave.Click += new System.EventHandler(this.btSave_Click);
            // 
            // gvMailing
            // 
            this.gvMailing.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.gvMailing.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvMailing.Location = new System.Drawing.Point(151, 130);
            this.gvMailing.MultiSelect = false;
            this.gvMailing.Name = "gvMailing";
            this.gvMailing.Size = new System.Drawing.Size(300, 150);
            this.gvMailing.TabIndex = 18;
            // 
            // rdoNew
            // 
            this.rdoNew.AutoSize = true;
            this.rdoNew.BackColor = System.Drawing.Color.Transparent;
            this.rdoNew.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdoNew.ForeColor = System.Drawing.SystemColors.ControlText;
            this.rdoNew.Location = new System.Drawing.Point(38, 76);
            this.rdoNew.Name = "rdoNew";
            this.rdoNew.Size = new System.Drawing.Size(96, 19);
            this.rdoNew.TabIndex = 19;
            this.rdoNew.TabStop = true;
            this.rdoNew.Text = "New Group";
            this.rdoNew.UseVisualStyleBackColor = false;
            this.rdoNew.CheckedChanged += new System.EventHandler(this.rdoNew_CheckedChanged);
            // 
            // textBox1
            // 
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox1.Location = new System.Drawing.Point(151, 76);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(265, 20);
            this.textBox1.TabIndex = 20;
            // 
            // frmMailingScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(461, 346);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.rdoNew);
            this.Controls.Add(this.gvMailing);
            this.Controls.Add(this.btSave);
            this.Controls.Add(this.cmbMailGroup);
            this.Controls.Add(this.rdoExisting);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMailingScreen";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Mailing";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMailingScreen_FormClosing);
            this.Load += new System.EventHandler(this.frmMailingScreen_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gvMailing)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion
        private System.Windows.Forms.RadioButton rdoExisting;
        private System.Windows.Forms.ComboBox cmbMailGroup;
        private System.Windows.Forms.Button btSave;
        private System.Windows.Forms.DataGridView gvMailing;
        private System.Windows.Forms.RadioButton rdoNew;
        private System.Windows.Forms.TextBox textBox1;
    }
}
