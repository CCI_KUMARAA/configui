﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
namespace CCI.MIP.JobPlugin.UI
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        public static int globaljobid = 0;
        public static int globalPlugin_ConfigId = 0;
         public static int globalPropId_config = 0;
        public static int globalPropId_binary = 0;
        public static int globalPropId_web = 0;
        public static string tabselectedvalue = "";
              public static string NewJobName = "";
              public static string JobName = "";
        [STAThread]
      
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmJobMain());
        }
    }
}
