﻿namespace CCI.MIP.JobPlugin.UI
{
    partial class frmJobLog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmJobLog));
            this.richTextJobLog = new System.Windows.Forms.RichTextBox();
            this.btCopy = new System.Windows.Forms.Button();
            this.lblcopy = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // richTextJobLog
            // 
            this.richTextJobLog.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.richTextJobLog.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextJobLog.Location = new System.Drawing.Point(12, 27);
            this.richTextJobLog.Name = "richTextJobLog";
            this.richTextJobLog.Size = new System.Drawing.Size(677, 483);
            this.richTextJobLog.TabIndex = 0;
            this.richTextJobLog.Text = "";
            this.richTextJobLog.WordWrap = false;
            // 
            // btCopy
            // 
            this.btCopy.BackColor = System.Drawing.Color.Transparent;
            this.btCopy.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btCopy.Image = global::CCI.MIP.JobPlugin.UI.Properties.Resources.copy_16x16;
            this.btCopy.Location = new System.Drawing.Point(700, 27);
            this.btCopy.Name = "btCopy";
            this.btCopy.Size = new System.Drawing.Size(70, 27);
            this.btCopy.TabIndex = 1;
            this.btCopy.Text = "Copy";
            this.btCopy.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btCopy.UseVisualStyleBackColor = false;
            this.btCopy.Click += new System.EventHandler(this.btCopy_Click);
            // 
            // lblcopy
            // 
            this.lblcopy.AutoSize = true;
            this.lblcopy.Location = new System.Drawing.Point(12, 513);
            this.lblcopy.Name = "lblcopy";
            this.lblcopy.Size = new System.Drawing.Size(0, 13);
            this.lblcopy.TabIndex = 2;
            // 
            // frmJobLog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(773, 532);
            this.Controls.Add(this.lblcopy);
            this.Controls.Add(this.btCopy);
            this.Controls.Add(this.richTextJobLog);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmJobLog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Job Log";
            this.Load += new System.EventHandler(this.frmJobLog_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion
        private System.Windows.Forms.RichTextBox richTextJobLog;
        private System.Windows.Forms.Button btCopy;
        private System.Windows.Forms.Label lblcopy;
    }
}