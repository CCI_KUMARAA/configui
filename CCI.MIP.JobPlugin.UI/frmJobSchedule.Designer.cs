﻿namespace CCI.MIP.JobPlugin.UI
{
    partial class frmJobSchedule
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmJobSchedule));
            this.tabJobsOptions = new System.Windows.Forms.TabControl();
            this.tabPageSchedule = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tmEditEndTime = new DevExpress.XtraEditors.TimeEdit();
            this.tmEditStartTime = new DevExpress.XtraEditors.TimeEdit();
            this.cmbCalendar = new System.Windows.Forms.ComboBox();
            this.numFrequency = new System.Windows.Forms.NumericUpDown();
            this.numDuration = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tabPageTrigger = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.btDelete = new System.Windows.Forms.Button();
            this.btAdd = new System.Windows.Forms.Button();
            this.cmbTriggerJobName = new System.Windows.Forms.ComboBox();
            this.cmbStatus = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.gridTrigger = new DevExpress.XtraGrid.GridControl();
            this.gridViewTrigger = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.TRIG_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TRIGGER_JOB_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TRIGGER_JOB_NAME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.STATUS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tabPageMailing = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cmbMailGrpStatus = new System.Windows.Forms.ComboBox();
            this.lblSelectStatus = new System.Windows.Forms.Label();
            this.cmbMailGroupName = new System.Windows.Forms.ComboBox();
            this.btnAddMail = new System.Windows.Forms.Button();
            this.txtEditMailGroup = new DevExpress.XtraEditors.TextEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.btnCreateMailGroup = new System.Windows.Forms.Button();
            this.gcMailing = new DevExpress.XtraGrid.GridControl();
            this.gvMailing = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.JOB_MAIL_STATUS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MAIL_GROUP_NAME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.USER_MAIL_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tabpageConfigPlugin = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btCreate = new System.Windows.Forms.Button();
            this.btRemove = new System.Windows.Forms.Button();
            this.numLoad = new System.Windows.Forms.NumericUpDown();
            this.numTransform = new System.Windows.Forms.NumericUpDown();
            this.numCapture = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.tabControlPlugin = new System.Windows.Forms.TabControl();
            this.tabPageHistory = new System.Windows.Forms.TabPage();
            this.label14 = new System.Windows.Forms.Label();
            this.grdcontrolHistory = new DevExpress.XtraGrid.GridControl();
            this.grdViewHistory = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STATUS_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.JOB_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SCHEDULE_TIME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.START_TIME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.END_TIME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CURRENT_STATUS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btUpdate = new System.Windows.Forms.Button();
            this.btSave = new System.Windows.Forms.Button();
            this.rdbNo = new System.Windows.Forms.RadioButton();
            this.rdbYes = new System.Windows.Forms.RadioButton();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.datagridconfighidden = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtDesk = new System.Windows.Forms.TextBox();
            this.txtJobName = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.lbljobId = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.timerJobStatus = new System.Windows.Forms.Timer(this.components);
            this.btnRefreshHistory = new System.Windows.Forms.Button();
            this.tabJobsOptions.SuspendLayout();
            this.tabPageSchedule.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tmEditEndTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tmEditStartTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numFrequency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDuration)).BeginInit();
            this.tabPageTrigger.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridTrigger)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTrigger)).BeginInit();
            this.tabPageMailing.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtEditMailGroup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcMailing)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvMailing)).BeginInit();
            this.tabpageConfigPlugin.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numLoad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTransform)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numCapture)).BeginInit();
            this.tabPageHistory.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdcontrolHistory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewHistory)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datagridconfighidden)).BeginInit();
            this.SuspendLayout();
            // 
            // tabJobsOptions
            // 
            this.tabJobsOptions.AccessibleRole = System.Windows.Forms.AccessibleRole.Application;
            this.tabJobsOptions.Controls.Add(this.tabPageSchedule);
            this.tabJobsOptions.Controls.Add(this.tabPageTrigger);
            this.tabJobsOptions.Controls.Add(this.tabPageMailing);
            this.tabJobsOptions.Controls.Add(this.tabpageConfigPlugin);
            this.tabJobsOptions.Controls.Add(this.tabPageHistory);
            this.tabJobsOptions.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabJobsOptions.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabJobsOptions.Location = new System.Drawing.Point(0, 118);
            this.tabJobsOptions.Name = "tabJobsOptions";
            this.tabJobsOptions.SelectedIndex = 0;
            this.tabJobsOptions.Size = new System.Drawing.Size(836, 559);
            this.tabJobsOptions.TabIndex = 0;
            this.tabJobsOptions.SelectedIndexChanged += new System.EventHandler(this.tabJobsOptions_SelectedIndexChanged);
            // 
            // tabPageSchedule
            // 
            this.tabPageSchedule.Controls.Add(this.groupBox2);
            this.tabPageSchedule.Location = new System.Drawing.Point(4, 25);
            this.tabPageSchedule.Name = "tabPageSchedule";
            this.tabPageSchedule.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageSchedule.Size = new System.Drawing.Size(828, 530);
            this.tabPageSchedule.TabIndex = 0;
            this.tabPageSchedule.Text = "Schedule";
            this.tabPageSchedule.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.groupBox2.Controls.Add(this.tmEditEndTime);
            this.groupBox2.Controls.Add(this.tmEditStartTime);
            this.groupBox2.Controls.Add(this.cmbCalendar);
            this.groupBox2.Controls.Add(this.numFrequency);
            this.groupBox2.Controls.Add(this.numDuration);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(822, 524);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Job Schedule";
            // 
            // tmEditEndTime
            // 
            this.tmEditEndTime.EditValue = new System.DateTime(2014, 4, 3, 0, 0, 0, 0);
            this.tmEditEndTime.Location = new System.Drawing.Point(293, 92);
            this.tmEditEndTime.Name = "tmEditEndTime";
            this.tmEditEndTime.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tmEditEndTime.Size = new System.Drawing.Size(100, 20);
            this.tmEditEndTime.TabIndex = 20;
            this.tmEditEndTime.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tmEditEndTime_KeyPress);
            // 
            // tmEditStartTime
            // 
            this.tmEditStartTime.EditValue = new System.DateTime(2014, 3, 20, 2, 26, 58, 0);
            this.tmEditStartTime.Location = new System.Drawing.Point(104, 92);
            this.tmEditStartTime.Name = "tmEditStartTime";
            this.tmEditStartTime.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tmEditStartTime.Size = new System.Drawing.Size(100, 20);
            this.tmEditStartTime.TabIndex = 19;
            this.tmEditStartTime.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tmEditStartTime_KeyPress);
            // 
            // cmbCalendar
            // 
            this.cmbCalendar.Location = new System.Drawing.Point(104, 41);
            this.cmbCalendar.Name = "cmbCalendar";
            this.cmbCalendar.Size = new System.Drawing.Size(140, 23);
            this.cmbCalendar.TabIndex = 18;
            this.cmbCalendar.SelectedIndexChanged += new System.EventHandler(this.cmbCalendar_SelectedIndexChanged);
            // 
            // numFrequency
            // 
            this.numFrequency.Font = new System.Drawing.Font("Nina", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numFrequency.Location = new System.Drawing.Point(105, 191);
            this.numFrequency.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numFrequency.Name = "numFrequency";
            this.numFrequency.Size = new System.Drawing.Size(61, 22);
            this.numFrequency.TabIndex = 12;
            this.numFrequency.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.numFrequency_KeyPress);
            // 
            // numDuration
            // 
            this.numDuration.Font = new System.Drawing.Font("Nina", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numDuration.Location = new System.Drawing.Point(105, 145);
            this.numDuration.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numDuration.Name = "numDuration";
            this.numDuration.Size = new System.Drawing.Size(61, 22);
            this.numDuration.TabIndex = 11;
            this.numDuration.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.numDuration_KeyPress);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Nina", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(172, 153);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(23, 14);
            this.label9.TabIndex = 7;
            this.label9.Text = "min";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(16, 195);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(81, 15);
            this.label8.TabIndex = 6;
            this.label8.Text = "FREQUENCY";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Nina", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(172, 199);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 14);
            this.label7.TabIndex = 5;
            this.label7.Text = "no. of times";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(16, 145);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 15);
            this.label6.TabIndex = 4;
            this.label6.Text = "DURATION";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(218, 95);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 15);
            this.label5.TabIndex = 3;
            this.label5.Text = "END TIME";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(16, 95);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 15);
            this.label4.TabIndex = 2;
            this.label4.Text = "START TIME";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(16, 45);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 15);
            this.label3.TabIndex = 1;
            this.label3.Text = "CALENDAR";
            // 
            // tabPageTrigger
            // 
            this.tabPageTrigger.Controls.Add(this.groupBox5);
            this.tabPageTrigger.Location = new System.Drawing.Point(4, 25);
            this.tabPageTrigger.Name = "tabPageTrigger";
            this.tabPageTrigger.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageTrigger.Size = new System.Drawing.Size(828, 530);
            this.tabPageTrigger.TabIndex = 4;
            this.tabPageTrigger.Text = "Trigger";
            this.tabPageTrigger.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.Color.Transparent;
            this.groupBox5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.groupBox5.Controls.Add(this.btDelete);
            this.groupBox5.Controls.Add(this.btAdd);
            this.groupBox5.Controls.Add(this.cmbTriggerJobName);
            this.groupBox5.Controls.Add(this.cmbStatus);
            this.groupBox5.Controls.Add(this.label17);
            this.groupBox5.Controls.Add(this.label16);
            this.groupBox5.Controls.Add(this.gridTrigger);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox5.Location = new System.Drawing.Point(3, 3);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(822, 524);
            this.groupBox5.TabIndex = 0;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Job Trigger";
            // 
            // btDelete
            // 
            this.btDelete.BackColor = System.Drawing.Color.Transparent;
            this.btDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btDelete.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btDelete.Image = global::CCI.MIP.JobPlugin.UI.Properties.Resources.cancel_16x16;
            this.btDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btDelete.Location = new System.Drawing.Point(717, 468);
            this.btDelete.Name = "btDelete";
            this.btDelete.Size = new System.Drawing.Size(89, 32);
            this.btDelete.TabIndex = 18;
            this.btDelete.Text = "Delete";
            this.btDelete.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btDelete.UseVisualStyleBackColor = false;
            this.btDelete.Click += new System.EventHandler(this.btDelete_Click);
            // 
            // btAdd
            // 
            this.btAdd.BackColor = System.Drawing.Color.Transparent;
            this.btAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btAdd.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btAdd.Image = global::CCI.MIP.JobPlugin.UI.Properties.Resources.newtask_16x16;
            this.btAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btAdd.Location = new System.Drawing.Point(739, 26);
            this.btAdd.Name = "btAdd";
            this.btAdd.Size = new System.Drawing.Size(67, 32);
            this.btAdd.TabIndex = 17;
            this.btAdd.Text = "Add";
            this.btAdd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btAdd.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btAdd.UseVisualStyleBackColor = false;
            this.btAdd.Click += new System.EventHandler(this.btAdd_Click);
            // 
            // cmbTriggerJobName
            // 
            this.cmbTriggerJobName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTriggerJobName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbTriggerJobName.FormattingEnabled = true;
            this.cmbTriggerJobName.Location = new System.Drawing.Point(141, 74);
            this.cmbTriggerJobName.Name = "cmbTriggerJobName";
            this.cmbTriggerJobName.Size = new System.Drawing.Size(290, 24);
            this.cmbTriggerJobName.TabIndex = 2;
            // 
            // cmbStatus
            // 
            this.cmbStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbStatus.FormattingEnabled = true;
            this.cmbStatus.Items.AddRange(new object[] {
            "Job Completed Successfully",
            "Job Failed",
            "Job Cancelled",
            "Stale Data on WebSite",
            "Timed out",
            "Job completed successfully with some errors",
            ""});
            this.cmbStatus.Location = new System.Drawing.Point(140, 34);
            this.cmbStatus.Name = "cmbStatus";
            this.cmbStatus.Size = new System.Drawing.Size(290, 24);
            this.cmbStatus.TabIndex = 2;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(9, 37);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(53, 15);
            this.label17.TabIndex = 1;
            this.label17.Text = "STATUS";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(9, 77);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(125, 15);
            this.label16.TabIndex = 1;
            this.label16.Text = "TRIGGER JOB NAME";
            // 
            // gridTrigger
            // 
            this.gridTrigger.Location = new System.Drawing.Point(12, 135);
            this.gridTrigger.LookAndFeel.SkinName = "DevExpress Dark Style";
            this.gridTrigger.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Style3D;
            this.gridTrigger.LookAndFeel.UseWindowsXPTheme = true;
            this.gridTrigger.MainView = this.gridViewTrigger;
            this.gridTrigger.Name = "gridTrigger";
            this.gridTrigger.Size = new System.Drawing.Size(794, 316);
            this.gridTrigger.TabIndex = 0;
            this.gridTrigger.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewTrigger});
            // 
            // gridViewTrigger
            // 
            this.gridViewTrigger.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.TRIG_ID,
            this.TRIGGER_JOB_ID,
            this.TRIGGER_JOB_NAME,
            this.STATUS});
            this.gridViewTrigger.GridControl = this.gridTrigger;
            this.gridViewTrigger.Name = "gridViewTrigger";
            this.gridViewTrigger.OptionsBehavior.Editable = false;
            this.gridViewTrigger.OptionsView.ShowGroupPanel = false;
            this.gridViewTrigger.PaintStyleName = "Office2003";
            // 
            // TRIG_ID
            // 
            this.TRIG_ID.Caption = "TRIG_ID";
            this.TRIG_ID.Name = "TRIG_ID";
            // 
            // TRIGGER_JOB_ID
            // 
            this.TRIGGER_JOB_ID.Caption = "TRIGGER_JOB_ID";
            this.TRIGGER_JOB_ID.Name = "TRIGGER_JOB_ID";
            this.TRIGGER_JOB_ID.Visible = true;
            this.TRIGGER_JOB_ID.VisibleIndex = 0;
            this.TRIGGER_JOB_ID.Width = 111;
            // 
            // TRIGGER_JOB_NAME
            // 
            this.TRIGGER_JOB_NAME.Caption = "TRIGGER_JOB_NAME";
            this.TRIGGER_JOB_NAME.Name = "TRIGGER_JOB_NAME";
            this.TRIGGER_JOB_NAME.Visible = true;
            this.TRIGGER_JOB_NAME.VisibleIndex = 1;
            this.TRIGGER_JOB_NAME.Width = 199;
            // 
            // STATUS
            // 
            this.STATUS.Caption = "STATUS";
            this.STATUS.Name = "STATUS";
            this.STATUS.Visible = true;
            this.STATUS.VisibleIndex = 2;
            this.STATUS.Width = 202;
            // 
            // tabPageMailing
            // 
            this.tabPageMailing.Controls.Add(this.groupBox3);
            this.tabPageMailing.Location = new System.Drawing.Point(4, 25);
            this.tabPageMailing.Name = "tabPageMailing";
            this.tabPageMailing.Size = new System.Drawing.Size(828, 530);
            this.tabPageMailing.TabIndex = 2;
            this.tabPageMailing.Text = "Mailing";
            this.tabPageMailing.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.groupBox3.Controls.Add(this.panel1);
            this.groupBox3.Controls.Add(this.btnCreateMailGroup);
            this.groupBox3.Controls.Add(this.gcMailing);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox3.Location = new System.Drawing.Point(0, 0);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(828, 530);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Mailing";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.cmbMailGrpStatus);
            this.panel1.Controls.Add(this.lblSelectStatus);
            this.panel1.Controls.Add(this.cmbMailGroupName);
            this.panel1.Controls.Add(this.btnAddMail);
            this.panel1.Controls.Add(this.txtEditMailGroup);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Location = new System.Drawing.Point(22, 294);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(657, 100);
            this.panel1.TabIndex = 5;
            // 
            // cmbMailGrpStatus
            // 
            this.cmbMailGrpStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMailGrpStatus.FormattingEnabled = true;
            this.cmbMailGrpStatus.Location = new System.Drawing.Point(172, 55);
            this.cmbMailGrpStatus.Name = "cmbMailGrpStatus";
            this.cmbMailGrpStatus.Size = new System.Drawing.Size(306, 23);
            this.cmbMailGrpStatus.TabIndex = 8;
            // 
            // lblSelectStatus
            // 
            this.lblSelectStatus.AutoSize = true;
            this.lblSelectStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSelectStatus.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblSelectStatus.Location = new System.Drawing.Point(6, 55);
            this.lblSelectStatus.Name = "lblSelectStatus";
            this.lblSelectStatus.Size = new System.Drawing.Size(78, 15);
            this.lblSelectStatus.TabIndex = 7;
            this.lblSelectStatus.Text = "Select Status";
            // 
            // cmbMailGroupName
            // 
            this.cmbMailGroupName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMailGroupName.FormattingEnabled = true;
            this.cmbMailGroupName.Location = new System.Drawing.Point(172, 16);
            this.cmbMailGroupName.Name = "cmbMailGroupName";
            this.cmbMailGroupName.Size = new System.Drawing.Size(306, 23);
            this.cmbMailGroupName.TabIndex = 6;
            this.cmbMailGroupName.SelectedIndexChanged += new System.EventHandler(this.cmbGroupName_SelectedIndexChanged);
            // 
            // btnAddMail
            // 
            this.btnAddMail.BackColor = System.Drawing.Color.Transparent;
            this.btnAddMail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddMail.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnAddMail.Image = global::CCI.MIP.JobPlugin.UI.Properties.Resources.save_16x16;
            this.btnAddMail.Location = new System.Drawing.Point(567, 18);
            this.btnAddMail.Name = "btnAddMail";
            this.btnAddMail.Size = new System.Drawing.Size(75, 23);
            this.btnAddMail.TabIndex = 5;
            this.btnAddMail.Text = "Add";
            this.btnAddMail.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAddMail.UseVisualStyleBackColor = false;
            this.btnAddMail.Click += new System.EventHandler(this.btnAddMail_Click);
            // 
            // txtEditMailGroup
            // 
            this.txtEditMailGroup.Location = new System.Drawing.Point(197, 77);
            this.txtEditMailGroup.Name = "txtEditMailGroup";
            this.txtEditMailGroup.Size = new System.Drawing.Size(254, 20);
            this.txtEditMailGroup.TabIndex = 4;
            this.txtEditMailGroup.Visible = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label11.Location = new System.Drawing.Point(6, 16);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(122, 15);
            this.label11.TabIndex = 3;
            this.label11.Text = "Select Mailing Group";
            // 
            // btnCreateMailGroup
            // 
            this.btnCreateMailGroup.BackColor = System.Drawing.Color.Transparent;
            this.btnCreateMailGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCreateMailGroup.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnCreateMailGroup.Image = global::CCI.MIP.JobPlugin.UI.Properties.Resources.newtask_16x16;
            this.btnCreateMailGroup.Location = new System.Drawing.Point(697, 38);
            this.btnCreateMailGroup.Name = "btnCreateMailGroup";
            this.btnCreateMailGroup.Size = new System.Drawing.Size(84, 23);
            this.btnCreateMailGroup.TabIndex = 4;
            this.btnCreateMailGroup.Text = "New";
            this.btnCreateMailGroup.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip1.SetToolTip(this.btnCreateMailGroup, "Add New Group and User Email ID");
            this.btnCreateMailGroup.UseVisualStyleBackColor = false;
            this.btnCreateMailGroup.Click += new System.EventHandler(this.button1_Click);
            // 
            // gcMailing
            // 
            this.gcMailing.Location = new System.Drawing.Point(22, 36);
            this.gcMailing.MainView = this.gvMailing;
            this.gcMailing.Name = "gcMailing";
            this.gcMailing.Size = new System.Drawing.Size(657, 242);
            this.gcMailing.TabIndex = 0;
            this.gcMailing.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvMailing});
            // 
            // gvMailing
            // 
            this.gvMailing.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.JOB_MAIL_STATUS,
            this.MAIL_GROUP_NAME,
            this.USER_MAIL_ID});
            this.gvMailing.GridControl = this.gcMailing;
            this.gvMailing.Name = "gvMailing";
            this.gvMailing.OptionsBehavior.Editable = false;
            this.gvMailing.OptionsView.ShowGroupPanel = false;
            // 
            // JOB_MAIL_STATUS
            // 
            this.JOB_MAIL_STATUS.Caption = "JOB MAIL STATUS";
            this.JOB_MAIL_STATUS.Name = "JOB_MAIL_STATUS";
            this.JOB_MAIL_STATUS.Visible = true;
            this.JOB_MAIL_STATUS.VisibleIndex = 0;
            // 
            // MAIL_GROUP_NAME
            // 
            this.MAIL_GROUP_NAME.Caption = "MAIL GROUP NAME";
            this.MAIL_GROUP_NAME.FieldName = "MAIL_GROUP_NAME";
            this.MAIL_GROUP_NAME.Name = "MAIL_GROUP_NAME";
            this.MAIL_GROUP_NAME.Visible = true;
            this.MAIL_GROUP_NAME.VisibleIndex = 1;
            // 
            // USER_MAIL_ID
            // 
            this.USER_MAIL_ID.Caption = "USER MAIL ID";
            this.USER_MAIL_ID.Name = "USER_MAIL_ID";
            // 
            // tabpageConfigPlugin
            // 
            this.tabpageConfigPlugin.Controls.Add(this.groupBox4);
            this.tabpageConfigPlugin.Location = new System.Drawing.Point(4, 25);
            this.tabpageConfigPlugin.Name = "tabpageConfigPlugin";
            this.tabpageConfigPlugin.Padding = new System.Windows.Forms.Padding(3);
            this.tabpageConfigPlugin.Size = new System.Drawing.Size(828, 530);
            this.tabpageConfigPlugin.TabIndex = 1;
            this.tabpageConfigPlugin.Text = "Config Plugin";
            this.tabpageConfigPlugin.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btCreate);
            this.groupBox4.Controls.Add(this.btRemove);
            this.groupBox4.Controls.Add(this.numLoad);
            this.groupBox4.Controls.Add(this.numTransform);
            this.groupBox4.Controls.Add(this.numCapture);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Controls.Add(this.label12);
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Controls.Add(this.tabControlPlugin);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox4.Location = new System.Drawing.Point(3, 3);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(822, 524);
            this.groupBox4.TabIndex = 6;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Plugin";
            // 
            // btCreate
            // 
            this.btCreate.BackColor = System.Drawing.Color.Transparent;
            this.btCreate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btCreate.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btCreate.Image = global::CCI.MIP.JobPlugin.UI.Properties.Resources.packageproduct_16x16;
            this.btCreate.Location = new System.Drawing.Point(696, 24);
            this.btCreate.Name = "btCreate";
            this.btCreate.Size = new System.Drawing.Size(91, 25);
            this.btCreate.TabIndex = 3;
            this.btCreate.Text = "Create";
            this.btCreate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btCreate.UseVisualStyleBackColor = false;
            this.btCreate.Click += new System.EventHandler(this.btCreate_Click);
            // 
            // btRemove
            // 
            this.btRemove.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btRemove.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btRemove.Image = global::CCI.MIP.JobPlugin.UI.Properties.Resources.cancel_16x16;
            this.btRemove.Location = new System.Drawing.Point(711, 24);
            this.btRemove.Name = "btRemove";
            this.btRemove.Size = new System.Drawing.Size(86, 25);
            this.btRemove.TabIndex = 4;
            this.btRemove.Text = "Remove";
            this.btRemove.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btRemove.UseVisualStyleBackColor = true;
            this.btRemove.Visible = false;
            this.btRemove.Click += new System.EventHandler(this.btRemove_Click);
            // 
            // numLoad
            // 
            this.numLoad.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numLoad.Location = new System.Drawing.Point(286, 29);
            this.numLoad.Name = "numLoad";
            this.numLoad.Size = new System.Drawing.Size(44, 22);
            this.numLoad.TabIndex = 2;
            // 
            // numTransform
            // 
            this.numTransform.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numTransform.Location = new System.Drawing.Point(197, 29);
            this.numTransform.Name = "numTransform";
            this.numTransform.Size = new System.Drawing.Size(44, 22);
            this.numTransform.TabIndex = 2;
            // 
            // numCapture
            // 
            this.numCapture.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numCapture.Location = new System.Drawing.Point(75, 29);
            this.numCapture.Name = "numCapture";
            this.numCapture.Size = new System.Drawing.Size(44, 22);
            this.numCapture.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label2.Location = new System.Drawing.Point(246, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "Load";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label12.Location = new System.Drawing.Point(121, 33);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(63, 15);
            this.label12.TabIndex = 1;
            this.label12.Text = "Transform";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label13.Location = new System.Drawing.Point(13, 33);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(50, 15);
            this.label13.TabIndex = 1;
            this.label13.Text = "Capture";
            // 
            // tabControlPlugin
            // 
            this.tabControlPlugin.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tabControlPlugin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControlPlugin.Location = new System.Drawing.Point(3, 58);
            this.tabControlPlugin.Name = "tabControlPlugin";
            this.tabControlPlugin.SelectedIndex = 0;
            this.tabControlPlugin.Size = new System.Drawing.Size(816, 463);
            this.tabControlPlugin.TabIndex = 0;
            this.tabControlPlugin.SelectedIndexChanged += new System.EventHandler(this.tabControlPlugin_SelectedIndexChanged);
            this.tabControlPlugin.Selected += new System.Windows.Forms.TabControlEventHandler(this.tabControlPlugin_Selected);
            this.tabControlPlugin.Leave += new System.EventHandler(this.tabControlPlugin_Leave);
            // 
            // tabPageHistory
            // 
            this.tabPageHistory.Controls.Add(this.btnRefreshHistory);
            this.tabPageHistory.Controls.Add(this.label14);
            this.tabPageHistory.Controls.Add(this.grdcontrolHistory);
            this.tabPageHistory.Location = new System.Drawing.Point(4, 25);
            this.tabPageHistory.Name = "tabPageHistory";
            this.tabPageHistory.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageHistory.Size = new System.Drawing.Size(828, 530);
            this.tabPageHistory.TabIndex = 3;
            this.tabPageHistory.Text = "Job History";
            this.tabPageHistory.UseVisualStyleBackColor = true;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label14.Location = new System.Drawing.Point(6, 464);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(258, 16);
            this.label14.TabIndex = 1;
            this.label14.Text = "**Double click on any row to view Job Log ";
            // 
            // grdcontrolHistory
            // 
            this.grdcontrolHistory.Location = new System.Drawing.Point(8, 21);
            this.grdcontrolHistory.LookAndFeel.UseWindowsXPTheme = true;
            this.grdcontrolHistory.MainView = this.grdViewHistory;
            this.grdcontrolHistory.Name = "grdcontrolHistory";
            this.grdcontrolHistory.Size = new System.Drawing.Size(802, 431);
            this.grdcontrolHistory.TabIndex = 0;
            this.grdcontrolHistory.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdViewHistory});
            // 
            // grdViewHistory
            // 
            this.grdViewHistory.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STATUS_ID,
            this.JOB_ID,
            this.SCHEDULE_TIME,
            this.START_TIME,
            this.END_TIME,
            this.CURRENT_STATUS});
            this.grdViewHistory.GridControl = this.grdcontrolHistory;
            this.grdViewHistory.Name = "grdViewHistory";
            this.grdViewHistory.OptionsBehavior.Editable = false;
            this.grdViewHistory.OptionsView.ShowGroupPanel = false;
            this.grdViewHistory.OptionsView.ShowPreviewRowLines = DevExpress.Utils.DefaultBoolean.True;
            this.grdViewHistory.PaintStyleName = "Skin";
            this.grdViewHistory.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.STATUS_ID, DevExpress.Data.ColumnSortOrder.Descending)});
            this.grdViewHistory.CustomColumnDisplayText += new DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventHandler(this.grdViewHistory_CustomColumnDisplayText);
            this.grdViewHistory.DoubleClick += new System.EventHandler(this.grdViewHistory_DoubleClick);
            // 
            // STATUS_ID
            // 
            this.STATUS_ID.Caption = "STATUS_ID";
            this.STATUS_ID.Name = "STATUS_ID";
            this.STATUS_ID.Visible = true;
            this.STATUS_ID.VisibleIndex = 0;
            this.STATUS_ID.Width = 80;
            // 
            // JOB_ID
            // 
            this.JOB_ID.Caption = "JOB_ID";
            this.JOB_ID.Name = "JOB_ID";
            this.JOB_ID.Visible = true;
            this.JOB_ID.VisibleIndex = 1;
            this.JOB_ID.Width = 76;
            // 
            // SCHEDULE_TIME
            // 
            this.SCHEDULE_TIME.Caption = "SCHEDULE_TIME";
            this.SCHEDULE_TIME.Name = "SCHEDULE_TIME";
            this.SCHEDULE_TIME.Visible = true;
            this.SCHEDULE_TIME.VisibleIndex = 2;
            this.SCHEDULE_TIME.Width = 137;
            // 
            // START_TIME
            // 
            this.START_TIME.Caption = "START_TIME";
            this.START_TIME.Name = "START_TIME";
            this.START_TIME.Visible = true;
            this.START_TIME.VisibleIndex = 3;
            this.START_TIME.Width = 141;
            // 
            // END_TIME
            // 
            this.END_TIME.Caption = "END_TIME";
            this.END_TIME.Name = "END_TIME";
            this.END_TIME.Visible = true;
            this.END_TIME.VisibleIndex = 4;
            this.END_TIME.Width = 148;
            // 
            // CURRENT_STATUS
            // 
            this.CURRENT_STATUS.Caption = "CURRENT_STATUS";
            this.CURRENT_STATUS.Name = "CURRENT_STATUS";
            this.CURRENT_STATUS.Visible = true;
            this.CURRENT_STATUS.VisibleIndex = 5;
            this.CURRENT_STATUS.Width = 114;
            // 
            // btUpdate
            // 
            this.btUpdate.BackColor = System.Drawing.Color.Transparent;
            this.btUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btUpdate.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btUpdate.Image = global::CCI.MIP.JobPlugin.UI.Properties.Resources.save_16x16;
            this.btUpdate.Location = new System.Drawing.Point(724, 81);
            this.btUpdate.Name = "btUpdate";
            this.btUpdate.Size = new System.Drawing.Size(78, 32);
            this.btUpdate.TabIndex = 17;
            this.btUpdate.Text = "Update";
            this.btUpdate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btUpdate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btUpdate.UseVisualStyleBackColor = false;
            this.btUpdate.Visible = false;
            this.btUpdate.Click += new System.EventHandler(this.btUpdate_Click);
            // 
            // btSave
            // 
            this.btSave.BackColor = System.Drawing.Color.Transparent;
            this.btSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btSave.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btSave.Image = global::CCI.MIP.JobPlugin.UI.Properties.Resources.saveandnew_16x16;
            this.btSave.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btSave.Location = new System.Drawing.Point(745, 80);
            this.btSave.Name = "btSave";
            this.btSave.Size = new System.Drawing.Size(67, 32);
            this.btSave.TabIndex = 16;
            this.btSave.Text = "Save";
            this.btSave.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btSave.UseVisualStyleBackColor = false;
            this.btSave.Visible = false;
            this.btSave.Click += new System.EventHandler(this.btSave_Click);
            // 
            // rdbNo
            // 
            this.rdbNo.AutoSize = true;
            this.rdbNo.BackColor = System.Drawing.Color.Transparent;
            this.rdbNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdbNo.ForeColor = System.Drawing.SystemColors.ControlText;
            this.rdbNo.Location = new System.Drawing.Point(179, 88);
            this.rdbNo.Name = "rdbNo";
            this.rdbNo.Size = new System.Drawing.Size(43, 19);
            this.rdbNo.TabIndex = 18;
            this.rdbNo.TabStop = true;
            this.rdbNo.Text = "NO";
            this.rdbNo.UseVisualStyleBackColor = false;
            this.rdbNo.CheckedChanged += new System.EventHandler(this.rdbNo_CheckedChanged);
            // 
            // rdbYes
            // 
            this.rdbYes.AutoSize = true;
            this.rdbYes.BackColor = System.Drawing.Color.Transparent;
            this.rdbYes.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdbYes.ForeColor = System.Drawing.SystemColors.ControlText;
            this.rdbYes.Location = new System.Drawing.Point(109, 88);
            this.rdbYes.Name = "rdbYes";
            this.rdbYes.Size = new System.Drawing.Size(48, 19);
            this.rdbYes.TabIndex = 17;
            this.rdbYes.TabStop = true;
            this.rdbYes.Text = "YES";
            this.rdbYes.UseVisualStyleBackColor = false;
            this.rdbYes.CheckedChanged += new System.EventHandler(this.rdbYes_CheckedChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label10.Location = new System.Drawing.Point(16, 90);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(53, 15);
            this.label10.TabIndex = 16;
            this.label10.Text = "ACTIVE";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.groupBox1.Controls.Add(this.datagridconfighidden);
            this.groupBox1.Controls.Add(this.rdbNo);
            this.groupBox1.Controls.Add(this.rdbYes);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.txtDesk);
            this.groupBox1.Controls.Add(this.txtJobName);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.lbljobId);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btSave);
            this.groupBox1.Controls.Add(this.btUpdate);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(836, 118);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Job Details";
            // 
            // datagridconfighidden
            // 
            this.datagridconfighidden.AllowUserToAddRows = false;
            this.datagridconfighidden.AllowUserToDeleteRows = false;
            this.datagridconfighidden.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.datagridconfighidden.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6,
            this.Column7});
            this.datagridconfighidden.Location = new System.Drawing.Point(647, 12);
            this.datagridconfighidden.Name = "datagridconfighidden";
            this.datagridconfighidden.ReadOnly = true;
            this.datagridconfighidden.Size = new System.Drawing.Size(155, 66);
            this.datagridconfighidden.TabIndex = 19;
            this.datagridconfighidden.Visible = false;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Column1";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Column2";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Column3";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Column4";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Column5";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            // 
            // Column6
            // 
            this.Column6.HeaderText = "Column6";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            // 
            // Column7
            // 
            this.Column7.HeaderText = "Column7";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            // 
            // txtDesk
            // 
            this.txtDesk.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDesk.Location = new System.Drawing.Point(105, 56);
            this.txtDesk.Name = "txtDesk";
            this.txtDesk.Size = new System.Drawing.Size(281, 21);
            this.txtDesk.TabIndex = 2;
            // 
            // txtJobName
            // 
            this.txtJobName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtJobName.Location = new System.Drawing.Point(105, 24);
            this.txtJobName.Name = "txtJobName";
            this.txtJobName.Size = new System.Drawing.Size(281, 21);
            this.txtJobName.TabIndex = 2;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Nina", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label15.Location = new System.Drawing.Point(16, 59);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(39, 16);
            this.label15.TabIndex = 0;
            this.label15.Text = "DESK";
            // 
            // lbljobId
            // 
            this.lbljobId.AutoSize = true;
            this.lbljobId.BackColor = System.Drawing.Color.Transparent;
            this.lbljobId.Font = new System.Drawing.Font("Nina", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbljobId.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lbljobId.Location = new System.Drawing.Point(422, 27);
            this.lbljobId.Name = "lbljobId";
            this.lbljobId.Size = new System.Drawing.Size(55, 16);
            this.lbljobId.TabIndex = 1;
            this.lbljobId.Text = "JOB ID :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Nina", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Location = new System.Drawing.Point(16, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "JOB NAME";
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "gridColumn1";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "gridColumn2";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // timerJobStatus
            // 
            this.timerJobStatus.Interval = 3000;
            this.timerJobStatus.Tick += new System.EventHandler(this.timerJobStatus_Tick);
            // 
            // btnRefreshHistory
            // 
            this.btnRefreshHistory.Location = new System.Drawing.Point(643, 491);
            this.btnRefreshHistory.Name = "btnRefreshHistory";
            this.btnRefreshHistory.Size = new System.Drawing.Size(152, 23);
            this.btnRefreshHistory.TabIndex = 2;
            this.btnRefreshHistory.Text = "Refresh Job History";
            this.btnRefreshHistory.UseVisualStyleBackColor = true;
            this.btnRefreshHistory.Click += new System.EventHandler(this.btnRefreshHistory_Click);
            // 
            // frmJobSchedule
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(836, 677);
            this.Controls.Add(this.tabJobsOptions);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmJobSchedule";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Job Schedule";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmJobSchedule_FormClosing);
            this.Load += new System.EventHandler(this.frmJobSchedule_Load);
            this.tabJobsOptions.ResumeLayout(false);
            this.tabPageSchedule.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tmEditEndTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tmEditStartTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numFrequency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDuration)).EndInit();
            this.tabPageTrigger.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridTrigger)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTrigger)).EndInit();
            this.tabPageMailing.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtEditMailGroup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcMailing)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvMailing)).EndInit();
            this.tabpageConfigPlugin.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numLoad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTransform)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numCapture)).EndInit();
            this.tabPageHistory.ResumeLayout(false);
            this.tabPageHistory.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdcontrolHistory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewHistory)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datagridconfighidden)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion
        private System.Windows.Forms.TabControl tabJobsOptions;
        private System.Windows.Forms.TabPage tabPageSchedule;
        private System.Windows.Forms.TabPage tabPageMailing;
        private System.Windows.Forms.TabPage tabpageConfigPlugin;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.NumericUpDown numFrequency;
        private System.Windows.Forms.NumericUpDown numDuration;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btUpdate;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lbljobId;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox3;
        private DevExpress.XtraGrid.GridControl gcMailing;
        private DevExpress.XtraGrid.Views.Grid.GridView gvMailing;
        private DevExpress.XtraEditors.TimeEdit tmEditStartTime;
        private System.Windows.Forms.Button btnCreateMailGroup;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btRemove;
        private System.Windows.Forms.Button btCreate;
        private System.Windows.Forms.NumericUpDown numLoad;
        private System.Windows.Forms.NumericUpDown numTransform;
        private System.Windows.Forms.NumericUpDown numCapture;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        public System.Windows.Forms.TabControl tabControlPlugin;
        private System.Windows.Forms.TabPage tabPageHistory;
        private DevExpress.XtraGrid.GridControl grdcontrolHistory;
        private DevExpress.XtraGrid.Views.Grid.GridView grdViewHistory;
        private DevExpress.XtraGrid.Columns.GridColumn STATUS_ID;
        private DevExpress.XtraGrid.Columns.GridColumn JOB_ID;
        private DevExpress.XtraGrid.Columns.GridColumn SCHEDULE_TIME;
        private DevExpress.XtraGrid.Columns.GridColumn START_TIME;
        private DevExpress.XtraGrid.Columns.GridColumn END_TIME;
        private DevExpress.XtraGrid.Columns.GridColumn CURRENT_STATUS;
        private DevExpress.XtraGrid.Columns.GridColumn MAIL_GROUP_NAME;
        public System.Windows.Forms.Button btSave;
        private System.Windows.Forms.Label label14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private System.Windows.Forms.RadioButton rdbYes;
        private System.Windows.Forms.Label label10;
        public System.Windows.Forms.TextBox txtJobName;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox cmbMailGroupName;
        private System.Windows.Forms.Button btnAddMail;
        private DevExpress.XtraEditors.TextEdit txtEditMailGroup;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ToolTip toolTip1;
        public System.Windows.Forms.RadioButton rdbNo;
        public System.Windows.Forms.ComboBox cmbCalendar;
        public System.Windows.Forms.DataGridView datagridconfighidden;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        public System.Windows.Forms.TextBox txtDesk;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TabPage tabPageTrigger;
        private System.Windows.Forms.GroupBox groupBox5;
        private DevExpress.XtraGrid.GridControl gridTrigger;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewTrigger;
        public DevExpress.XtraEditors.TimeEdit tmEditEndTime;
        private DevExpress.XtraGrid.Columns.GridColumn TRIGGER_JOB_ID;
        private DevExpress.XtraGrid.Columns.GridColumn TRIGGER_JOB_NAME;
        private DevExpress.XtraGrid.Columns.GridColumn STATUS;
        public System.Windows.Forms.Button btDelete;
        public System.Windows.Forms.Button btAdd;
        private System.Windows.Forms.ComboBox cmbTriggerJobName;
        private System.Windows.Forms.ComboBox cmbStatus;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private DevExpress.XtraGrid.Columns.GridColumn TRIG_ID;
        private System.Windows.Forms.Timer timerJobStatus;
        private System.Windows.Forms.ComboBox cmbMailGrpStatus;
        private System.Windows.Forms.Label lblSelectStatus;
        private DevExpress.XtraGrid.Columns.GridColumn JOB_MAIL_STATUS;
        private DevExpress.XtraGrid.Columns.GridColumn USER_MAIL_ID;
        private System.Windows.Forms.Button btnRefreshHistory;
    }
}
