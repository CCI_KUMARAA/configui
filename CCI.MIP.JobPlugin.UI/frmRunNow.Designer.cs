﻿namespace CCI.MIP.JobPlugin.UI
{
    partial class frmRunNow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.cmbJobName = new System.Windows.Forms.ComboBox();
            this.btRun = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Location = new System.Drawing.Point(12, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Job Name";
            // 
            // cmbJobName
            // 
            this.cmbJobName.FormattingEnabled = true;
            this.cmbJobName.Location = new System.Drawing.Point(97, 28);
            this.cmbJobName.Name = "cmbJobName";
            this.cmbJobName.Size = new System.Drawing.Size(233, 21);
            this.cmbJobName.TabIndex = 1;
            // 
            // btRun
            // 
            this.btRun.BackColor = System.Drawing.Color.Transparent;
            this.btRun.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btRun.Image = global::CCI.MIP.JobPlugin.UI.Properties.Resources.next_16x16;
            this.btRun.Location = new System.Drawing.Point(266, 78);
            this.btRun.Name = "btRun";
            this.btRun.Size = new System.Drawing.Size(64, 23);
            this.btRun.TabIndex = 2;
            this.btRun.Text = "Run";
            this.btRun.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btRun.UseVisualStyleBackColor = false;
            this.btRun.Click += new System.EventHandler(this.btRun_Click);
            // 
            // frmRunNow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(342, 119);
            this.Controls.Add(this.btRun);
            this.Controls.Add(this.cmbJobName);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmRunNow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Run Now";
            this.Load += new System.EventHandler(this.frmRunNow_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbJobName;
        private System.Windows.Forms.Button btRun;
    }
}