﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace CCI.MIP.JobPlugin.UI
{
    public partial class frmBinary : Form
    {
        JobConfigWinService.JobConfigServiceClient jobref = new JobConfigWinService.JobConfigServiceClient();
        JobConfigWinService.JobConfigPlugin jobscd = new JobConfigWinService.JobConfigPlugin();
        private static readonly log4net.ILog _log = log4net.LogManager.GetLogger(typeof(frmBinary));
      
        public frmBinary()
        {
            InitializeComponent();
        }

        private void frmBinary_Load(object sender, EventArgs e)
        {

        }

        private void btSave_Click(object sender, EventArgs e)
        {
            try
            {
                _log.Info("calling btSave_Click");
                jobscd.Description = "mnt/marketintel/SourceDocument/Temp.csv";
                jobscd.Previousvalue ="mnt/marketintel/SourceDocument/Temp.csv";               
                int pluginId = Convert.ToInt32(txtPluginid.Text);  
                jobscd.Jobname=txtJobName.Text;
                jobscd.PluginId=pluginId;
                jobscd.PluginConfigId = pluginId;
                jobscd.JobLastUpdate=System.DateTime.Now.ToString();
                jobscd.PropName="Binary";
                jobscd.PropValue = txtJobName.Text;
                int iresult = jobref.InsertJobUpdate(jobscd);
                int iresultprop = jobref.InsertJobPluginProp(jobscd);
                if (iresult == 1 && iresultprop == 1)
                {
                    MessageBox.Show("Record inserted successfully!", "Binary", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred while trying to save."+ ex.Message, "Binary", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);

            }
        }

        private void btUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                _log.Info("calling btUpdate_Click");
                jobscd = new JobConfigWinService.JobConfigPlugin();
                jobscd.PluginId = Convert.ToInt32(txtPluginid.Text);
                jobscd.PluginConfigId = Convert.ToInt32(txtPluginid.Text);
                jobscd.PropId = Convert.ToInt32(txtBinaryId.Text);
                jobscd.UpdateId = Convert.ToInt32(txtJobUpdateId.Text);
                jobscd.Jobname = txtJobName.Text;
                jobscd.JobLastUpdate=System.DateTime.Now.ToString();
                jobscd.Description = "";
                jobscd.PropName = "Binary";
                jobscd.PropValue = txtJobName.Text;

                int iresult = jobref.UpdateJobUpdate(jobscd);
                int iresultprop = jobref.UpdateJobPluginProp(jobscd);
                if (iresult == 1 && iresultprop==1)
                {
                    MessageBox.Show("Record Updated successfully!", "Binary", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred while trying to update."+ ex.Message, "Binary", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);
            }
        }
    }
}
