﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;
namespace CCI.MIP.JobPlugin.UI
{
    public partial class frmMailingScreen : Form
    {
        private static readonly log4net.ILog _log = log4net.LogManager.GetLogger(typeof(frmMailingScreen));
        JobConfigWinService.JobConfigServiceClient jobref = new JobConfigWinService.JobConfigServiceClient();
        JobConfigWinService.JobConfigPlugin jobscd = new JobConfigWinService.JobConfigPlugin();
        public frmMailingScreen()
        {
            InitializeComponent();

        }
        private void cmbMailGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdoExisting.Checked)
            {
                DataSet ds = new DataSet();
                string mailgroupname = "";
                if (cmbMailGroup.SelectedItem != null)
                {
                    mailgroupname = cmbMailGroup.SelectedItem.ToString();
                }
                try
                {
                    _log.Info("calling cmbMailGroup_SelectedIndexChanged");
                    jobscd.MailGroupName = mailgroupname;
                    ds.Clear();
                    gvMailing.DataSource = null;
                    gvMailing.Columns.Clear();
                    ds.Tables.Clear();
                    ds = jobref.GetMailingList(jobscd);
                    gvMailing.DataSource = ds.Tables[0];
                    this.gvMailing.Columns["MAIL_GROUP_NAME"].Visible = false;
                    
                    this.gvMailing.Columns["ID"].Visible = false;
                   
                    
                }
                catch (Exception ex)
                {
                    MessageBox.Show("An error occurred while trying to fill combo." + ex.Message, "Mailing", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    _log.Error(ex.Message, ex);
                }
            }
        }
        private void frmMailingScreen_Load(object sender, EventArgs e)
        {
            LoadComboMailGroup(Program.globaljobid);
        }
        public void LoadComboMailGroup(int jobid)
        {
            try
            {
                _log.Info("calling LoadComboMailGroup");
                jobscd.JobId = jobid;
                cmbMailGroup.Items.Clear();
                DataSet ds = jobref.GetJobMailGroup(jobscd);
                for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                {
                    cmbMailGroup.Items.Add(ds.Tables[0].Rows[i].ItemArray[0].ToString());
                    //cmbCalendar.DisplayMember = ds.Tables[0].Rows[0].ItemArray[i].ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred while trying to Load." + ex.Message, "Mailing", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);
            }
        }
        private void btnEdit_Click_1(object sender, EventArgs e)
        {
            //int i;
            //i = gcMailing.SelectedCells[0].RowIndex;
            //txtName.Text = gvMailing.Rows[i].Cells[1].Value.ToString();
            //txtLocation.Text = gvMailing.Rows[i].Cells[2].Value.ToString();
        }
        public void btSave_Click(object sender, EventArgs e)
        {
            try
            {
                _log.Info("calling btSave_Click");
                if (btSave.Text == "Add")
                {
                    if ((textBox1.Text.ToString() != "") && (textBox1.Text.ToString() != null))
                    {
                        int addedGroup = 0;
                        int jobId = Program.globaljobid;
                        string mailgroupname = textBox1.Text.ToString();
                        jobscd.MailGroupName = mailgroupname;
                        jobscd.JobId = jobId;
                        int existMailGroup = jobref.CheckMailGroup(jobscd);
                        if (existMailGroup >= 1)
                        {
                            MessageBox.Show("Group Already Exists", "Mailing", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            int count = 0;
                            if (gvMailing.Rows.Count > 1)
                            {
                                int j1 = 0;
                                int i1 = 0;
                                for (j1 = 0; j1 < gvMailing.Rows.Count; j1++)
                                {
                                    string UserMailId = gvMailing.Rows[i1].Cells[0].Value.ToString();
                                    bool valid = isEmail(UserMailId);
                                    if (valid == false)
                                    {
                                        count = count + 1;
                                    }
                                    i1++;
                                    j1++;
                                }
                                if (count < 1)
                                {
                                    addedGroup = jobref.InsertJobMailGroup(jobscd);
                                    if (addedGroup >= 1)
                                    {
                                        if (gvMailing.Rows.Count > 1)
                                        {
                                            int j = 0;
                                            int i = 0;
                                            for (j = 0; j < gvMailing.Rows.Count; j++)
                                            {
                                                string UserMailId = gvMailing.Rows[i].Cells[0].Value.ToString();
                                                jobscd.UserMailId = UserMailId;
                                                int added = jobref.InsertMailingList(jobscd);
                                                i++;
                                                j++;
                                            }
                                            textBox1.Clear();
                                            gvMailing.Rows.Clear();
                                            MessageBox.Show("Mail Group is created successfully.", "Mailing", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        }
                                        else
                                        {
                                            MessageBox.Show("Please Enter Valid Mail ID.", "Mailing", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                        }
                                    }
                                    else
                                    {
                                        MessageBox.Show("An error occoured while Inserting Group", "Mailing", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    }
                                    //
                                }
                                else
                                {
                                    MessageBox.Show("Please Enter Valid Mail ID.", "Mailing", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                }
                            }
                            else
                            {
                                MessageBox.Show("Please Enter Valid Mail ID.", "Mailing", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("Please Enter a valid Group Name.", "Mailing", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    LoadComboMailGroup(Program.globaljobid);

                }
                else if (btSave.Text == "Save")
                {
                    int updatedcount = 0;
                    int insertedcount = 0;
                    int j = 0;
                    int i = 0;
                    for (j = 0; j <= gvMailing.Rows.Count - 1; j++)
                    {
                        if ((gvMailing.Rows[j].Cells[0].FormattedValue != "") && (gvMailing.Rows[j].Cells[0].FormattedValue != null))
                        {
                            string UserMailId = gvMailing.Rows[j].Cells[2].Value.ToString();
                            int UserId = Convert.ToInt32(gvMailing.Rows[j].Cells[0].Value.ToString());
                            string mailgroupname = cmbMailGroup.SelectedItem.ToString();
                            jobscd.MailGroupName = mailgroupname;
                            jobscd.UserMailId = UserMailId;
                            jobscd.UserId = UserId;
                            int updated = jobref.UpdateMailingList(jobscd);
                            updatedcount = updatedcount + updated;
                        }
                        else
                        {
                            if (gvMailing.Rows[j].Cells[2].Value != null)
                            {
                                string UserMailId = gvMailing.Rows[j].Cells[2].Value.ToString();
                                string mailgroupname = cmbMailGroup.Text.ToString();
                                jobscd.UserMailId = UserMailId;
                                jobscd.MailGroupName = mailgroupname;
                                int inserted = jobref.InsertMailingList(jobscd);
                                insertedcount = insertedcount + inserted;
                            }
                        }
                        i++;
                    }
                    if ((updatedcount >= -1) && (insertedcount >= -1))
                    {
                        MessageBox.Show("Successfully Updated Mailing List.", "Mailing", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("Error while updating Mailing List.", "Mailing", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred while trying to Load." + ex.Message, "Mailing", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);
            }

        }
        public static bool isEmail(string inputEmail)
        {
            string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                  @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                  @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
            Regex re = new Regex(strRegex);
            if (re.IsMatch(inputEmail))
                return (true);
            else
                return (false);
        }
        private void rdoExisting_CheckedChanged(object sender, EventArgs e)
        {
            btSave.Text = "Save";
            if (rdoExisting.Checked == true)
            {
                rdoNew.Checked = false;
                textBox1.Enabled = false;
                textBox1.Clear();
                gvMailing.Rows.Clear();
                cmbMailGroup.Enabled = true;
            }
        }
        private void rdoNew_CheckedChanged(object sender, EventArgs e)
        {
            btSave.Text = "Add";
            cmbMailGroup.SelectedItem = null;
            if (rdoNew.Checked == true)
            {
                rdoExisting.Checked = false;
                cmbMailGroup.Enabled = false;
                textBox1.Enabled = true;
                gvMailing.DataSource = null;
                gvMailing.Columns.Clear();
                this.gvMailing.Columns.Add("USER_MAIL_ID", "MAIL ID");
            }
        }

        private void frmMailingScreen_FormClosing(object sender, FormClosingEventArgs e)
        {
            frmJobSchedule frm = new frmJobSchedule();
            frm.FillMailingGrid();
            frm.LoadMailingGrid();
            frm.LoadComboMailGroup(Program.globaljobid);

        }
    }
}