﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CCI.MIP.JobPlugin;

namespace CCI.MIP.JobPlugin.UI
{
    public partial class frmRunNow : Form
    {
        JobConfigWinService.JobConfigServiceClient jobref = new JobConfigWinService.JobConfigServiceClient();
        JobConfigWinService.JobConfigPlugin jobscd = new JobConfigWinService.JobConfigPlugin();

        private static readonly log4net.ILog _log = log4net.LogManager.GetLogger(typeof(frmRunNow));

        public frmRunNow()
        {
            InitializeComponent();
        }

        private void frmRunNow_Load(object sender, EventArgs e)
        {
            try
            {

                DataSet ds=jobref.GetJobMaster();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count;i++ )
                    {
                        cmbJobName.Items.Add(ds.Tables[0].Rows[i].ItemArray[1].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred while trying to Load." + ex.Message, "Run Now", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);

            }

        }

        private void btRun_Click(object sender, EventArgs e)
        {
            try
            {
                _log.Info("calling btRun_Click");
                jobscd = new JobConfigWinService.JobConfigPlugin();
                if (cmbJobName.SelectedItem.ToString() != null)
                {
                    jobscd.Jobname = cmbJobName.SelectedItem.ToString();
                    int iresult = jobref.InsertJobStatus(jobscd);
                    if (iresult < 0)
                    {
                        MessageBox.Show("Record added sucessfully", "Run Now", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Close();
                    }
                }
                else
                {
                    MessageBox.Show("Please select Job Name.", "Run Now", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred while trying to Load." + ex.Message, "Run Now", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);

            }
        }
    }
}
