﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CCI.MIP.JobPlugin.UI
{
    public partial class frmSource : Form
    {
        JobConfigWinService.JobConfigServiceClient jobref = new JobConfigWinService.JobConfigServiceClient();
        JobConfigWinService.JobConfigPlugin jobscd = new JobConfigWinService.JobConfigPlugin();
        private static readonly log4net.ILog _log = log4net.LogManager.GetLogger(typeof(frmSource));

        public frmSource()
        {
            InitializeComponent();
        }

        private void btNew_Click(object sender, EventArgs e)
        {
            try
            {
                _log.Info("calling btNew_Click");
                clearcontrol();


            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred " + ex.Message, "Source", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);

            }
        }

        private void clearcontrol()
        {
            try
            {
                _log.Info("calling btNew_Click");
                txtSourceKey.Text = "";
                txtSourceName.Text = "";
                txtDescription.Text = "";
                txtDisplaySource.Text = "";
                btSave.Visible = true;
                btUpdate.Visible = false;


            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred " + ex.Message, "Source", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);

            }
        }

        private void frmSource_Load(object sender, EventArgs e)
        {
            try
            {
                _log.Info("calling frmSource_Load");
                RefreshGrid();


            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred " + ex.Message, "Source", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);

            }
        }

        private void RefreshGrid()
        {
            try
            {
                _log.Info("calling frmSource_Load");
                DataSet dsprod = jobref.GetSource();
                if (dsprod.Tables.Count > 0)
                {
                    if (dsprod.Tables[0].Rows.Count > 0)
                    {
                        SOURCE_KEY.FieldName = dsprod.Tables[0].Columns[0].ToString();
                        NAME.FieldName = dsprod.Tables[0].Columns[1].ToString();
                        DISPLAY_SOURCE.FieldName = dsprod.Tables[0].Columns[2].ToString();
                        DESCRIPTION.FieldName = dsprod.Tables[0].Columns[3].ToString();
                        grdcontrolSource.DataSource = dsprod.Tables[0];
                        btSave.Visible = false;
                        btUpdate.Visible = true;

                    }

                }


            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred " + ex.Message, "Source", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);

            }
        }

        private void grdViewSource_Click(object sender, EventArgs e)
        {
            txtSourceKey.Text = (grdViewSource.GetFocusedDataRow()["SOURCE_KEY"].ToString());
            txtSourceName.Text = (grdViewSource.GetFocusedDataRow()["NAME"].ToString());
            txtDisplaySource.Text = (grdViewSource.GetFocusedDataRow()["DISPLAY_SOURCE"].ToString());
            txtDescription.Text = (grdViewSource.GetFocusedDataRow()["DESCRIPTION"].ToString());
        }

        private void btSave_Click(object sender, EventArgs e)
        {
            try
            {
                _log.Info("calling btSave_Click");
                if (txtSourceName.Text != "" && txtDisplaySource.Text != "")
                {
                    jobscd.Display_Source = txtDisplaySource.Text;
                    jobscd.Source_Name = txtSourceName.Text;
                    jobscd.Source_Description = txtDescription.Text;
                    int iresult = jobref.InsertSource(jobscd);
                    if (iresult == 1)
                    {
                        MessageBox.Show("Record saved successfully.", "Source", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        RefreshGrid();
                        clearcontrol();
                        btSave.Visible = false;
                        btUpdate.Visible = true;
                    }
                    else
                    {
                        MessageBox.Show("Error occurs while saving.", "Source", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }

                }
                else
                {
                    MessageBox.Show("Please Enter Source Name.", "Source", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred " + ex.Message, "Source", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);

            }
        }

        private void btUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                _log.Info("calling btUpdate_Click");
                if (txtSourceName.Text != "" && txtDisplaySource.Text != "" && txtSourceKey.Text != "")
                {
                    jobscd.Display_Source = txtDisplaySource.Text;
                    jobscd.Source_Name = txtSourceName.Text;
                    jobscd.Source_Description = txtDescription.Text;
                    jobscd.Source_Key = Convert.ToInt32(txtSourceKey.Text);
                    int iresult = jobref.UpdateSource(jobscd);
                    if (iresult == 1)
                    {
                        MessageBox.Show("Record Updated successfully.", "Source", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        RefreshGrid();
                        clearcontrol();
                        btSave.Visible = false;
                        btUpdate.Visible = true;
                    }
                    else
                    {
                        MessageBox.Show("Error occurs while updating.", "Source", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }

                }
                else
                {
                    MessageBox.Show("Please Select Source.", "Source", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred " + ex.Message, "Source", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);

            }
        }
    }
}
