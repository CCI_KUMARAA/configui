﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CCI.MIP.JobPlugin.UI
{
    public partial class frmMetaDataCSV : Form
    {
        private static readonly log4net.ILog _log = log4net.LogManager.GetLogger(typeof(frmMetaDataCSV));
        JobConfigWinService.JobConfigServiceClient jmclient = new JobConfigWinService.JobConfigServiceClient();
         JobConfigWinService.JobConfigPlugin jobscd = new JobConfigWinService.JobConfigPlugin();
                
        public frmMetaDataCSV()
        {
            InitializeComponent();
        }

        private void btSave_Click(object sender, EventArgs e)
        {
            jobscd = new JobConfigWinService.JobConfigPlugin();
            
            if (txtSource.Text != "")
            {
                jobscd.PropName = "SourceName";
                jobscd.PropValue = txtSource.Text;
                jobscd.PropId = 0;
                jobscd.PluginConfigId = 0;
                
                int iupdate = jmclient.UpdateJobPluginProp(jobscd);
                if (iupdate == 1)
                {
                    MessageBox.Show("Record updated successfully.", "Metadata CSV Creation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //INSERT INTO JOBSTATUS

                    jobscd.Jobname = "MetadataFromSusTable";
                    int iresult = jmclient.InsertJobStatus(jobscd);
                    this.Close();
                }
            }
        }
    }
}
