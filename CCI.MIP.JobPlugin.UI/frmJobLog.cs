﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
namespace CCI.MIP.JobPlugin.UI
{
    public partial class frmJobLog : Form
    {
        private static readonly log4net.ILog _log = log4net.LogManager.GetLogger(typeof(frmJobLog));
        JobConfigWinService.JobConfigPlugin jobscd = new JobConfigWinService.JobConfigPlugin();
        JobConfigWinService.JobConfigServiceClient jobref = new JobConfigWinService.JobConfigServiceClient();
        public frmJobLog()
        {
            InitializeComponent();
        }
        public void FillJobLog(string statusId)
        {
            try
            {
                _log.Info("calling FillJobLog");
                
                jobscd = new JobConfigWinService.JobConfigPlugin();
                jobscd.StatusId = Convert.ToInt32(statusId);
                DataSet ds = jobref.GetJobLog(jobscd);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                    {
                        string strcomments = String.Format("{0} \t {1}", ds.Tables[0].Rows[i].ItemArray[3], ds.Tables[0].Rows[i].ItemArray[4]);

                        richTextJobLog.Text = String.Format("{0}\n{1}", richTextJobLog.Text, strcomments);
                    }
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show("An error occurred while trying to retrieve." +ex.Message, "Log", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);
            }
        }

        private void btCopy_Click(object sender, EventArgs e)
        {
            try
            {
                _log.Info("calling btCopy_Click");
                if (richTextJobLog.Text != "")
                {
                    String text = richTextJobLog.SelectedText;
                    if (text != "")
                    {
                        Clipboard.SetText(text);
                        lblcopy.Text = "Text Copied to clipboard";

                    }
                    else
                    {
                        MessageBox.Show("Please select text to copy.", "Log", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("An error occurred while trying to copy." +ex.Message, "Log", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);
            }
        }

        private void frmJobLog_Load(object sender, EventArgs e)
        {
            lblcopy.Text = "";
        }
    }
}
