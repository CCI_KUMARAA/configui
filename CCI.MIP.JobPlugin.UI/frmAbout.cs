﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.ServiceModel.Configuration;


namespace CCI.MIP.JobPlugin.UI
{
    public partial class frmAbout : Form
    {
        private static readonly log4net.ILog _log = log4net.LogManager.GetLogger(typeof(frmAbout));
        public frmAbout()
        {
            InitializeComponent();
        }

        private void frmAbout_Load(object sender, EventArgs e)
        {
            try
            {
                _log.Info("calling frmAbout_Load");
                ClientSection clientSettings = ConfigurationManager.GetSection("system.serviceModel/client") as ClientSection;


                foreach (ChannelEndpointElement endpoint in clientSettings.Endpoints)
                {
                    if (endpoint.Name != "")
                    {
                        txtEndpoint.Text = endpoint.Address.ToString();
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred while changing the endpoint." + ex.Message, "Help", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);
            }
        }

        private void btChange_Click(object sender, EventArgs e)
        {
            try
            {
                _log.Info("calling btChange_Click");
                Configuration wConfig = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                ServiceModelSectionGroup wServiceSection = ServiceModelSectionGroup.GetSectionGroup(wConfig);

                ClientSection wClientSection = wServiceSection.Client;
                if (txtEndpoint.Text != "")
                {
                    System.Uri uri = new System.Uri(txtEndpoint.Text);
                    wClientSection.Endpoints[0].Address = uri;
                    wConfig.Save();
                    MessageBox.Show("Changes done ,Please Restart Config UI to reflect the changes", "About", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Please enter endpoint", "About", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred while changing the endpoint." + ex.Message, "Help", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);
            }

                
        }
    }
}
