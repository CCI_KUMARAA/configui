﻿namespace CCI.MIP.JobPlugin.UI
{
    partial class UserControlCapture
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
        #region Component Designer generated code
        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblDataFilter = new System.Windows.Forms.Label();
            this.btDataFilter = new System.Windows.Forms.Button();
            this.labelDataFilter = new System.Windows.Forms.Label();
            this.btWebAdd = new System.Windows.Forms.Button();
            this.btBinaryAdd = new System.Windows.Forms.Button();
            this.btConfigAdd = new System.Windows.Forms.Button();
            this.txtSeqNo = new System.Windows.Forms.TextBox();
            this.dataGrdViewProp = new System.Windows.Forms.DataGridView();
            this.PROP_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PLUGIN_CONFIG_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROP_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROP_VALUE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtPropId_config = new System.Windows.Forms.TextBox();
            this.txtPropId_binary = new System.Windows.Forms.TextBox();
            this.txtprop_datafilterId = new System.Windows.Forms.TextBox();
            this.txtPropId_webchange = new System.Windows.Forms.TextBox();
            this.txtpluginId = new System.Windows.Forms.TextBox();
            this.lblSeqNo = new System.Windows.Forms.Label();
            this.btWebRemove = new System.Windows.Forms.Button();
            this.btBinaryRemove = new System.Windows.Forms.Button();
            this.btConfigremove = new System.Windows.Forms.Button();
            this.labelBIN = new System.Windows.Forms.Label();
            this.lblBinary = new System.Windows.Forms.Label();
            this.labelWEB = new System.Windows.Forms.Label();
            this.lblWeb = new System.Windows.Forms.Label();
            this.labelCONFIG = new System.Windows.Forms.Label();
            this.lblConfig = new System.Windows.Forms.Label();
            this.btDelete = new System.Windows.Forms.Button();
            this.btKeep = new System.Windows.Forms.Button();
            this.btSave = new System.Windows.Forms.Button();
            this.txtPulgin = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrdViewProp)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.groupBox1.Controls.Add(this.lblDataFilter);
            this.groupBox1.Controls.Add(this.btDataFilter);
            this.groupBox1.Controls.Add(this.labelDataFilter);
            this.groupBox1.Controls.Add(this.btWebAdd);
            this.groupBox1.Controls.Add(this.btBinaryAdd);
            this.groupBox1.Controls.Add(this.btConfigAdd);
            this.groupBox1.Controls.Add(this.txtSeqNo);
            this.groupBox1.Controls.Add(this.dataGrdViewProp);
            this.groupBox1.Controls.Add(this.txtPropId_config);
            this.groupBox1.Controls.Add(this.txtPropId_binary);
            this.groupBox1.Controls.Add(this.txtprop_datafilterId);
            this.groupBox1.Controls.Add(this.txtPropId_webchange);
            this.groupBox1.Controls.Add(this.txtpluginId);
            this.groupBox1.Controls.Add(this.lblSeqNo);
            this.groupBox1.Controls.Add(this.btWebRemove);
            this.groupBox1.Controls.Add(this.btBinaryRemove);
            this.groupBox1.Controls.Add(this.btConfigremove);
            this.groupBox1.Controls.Add(this.labelBIN);
            this.groupBox1.Controls.Add(this.lblBinary);
            this.groupBox1.Controls.Add(this.labelWEB);
            this.groupBox1.Controls.Add(this.lblWeb);
            this.groupBox1.Controls.Add(this.labelCONFIG);
            this.groupBox1.Controls.Add(this.lblConfig);
            this.groupBox1.Controls.Add(this.btDelete);
            this.groupBox1.Controls.Add(this.btKeep);
            this.groupBox1.Controls.Add(this.btSave);
            this.groupBox1.Controls.Add(this.txtPulgin);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(474, 373);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // lblDataFilter
            // 
            this.lblDataFilter.AutoSize = true;
            this.lblDataFilter.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataFilter.Location = new System.Drawing.Point(129, 302);
            this.lblDataFilter.Name = "lblDataFilter";
            this.lblDataFilter.Size = new System.Drawing.Size(0, 15);
            this.lblDataFilter.TabIndex = 12;
            // 
            // btDataFilter
            // 
            this.btDataFilter.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btDataFilter.Image = global::CCI.MIP.JobPlugin.UI.Properties.Resources.add_32x32;
            this.btDataFilter.Location = new System.Drawing.Point(420, 296);
            this.btDataFilter.Name = "btDataFilter";
            this.btDataFilter.Size = new System.Drawing.Size(30, 27);
            this.btDataFilter.TabIndex = 11;
            this.btDataFilter.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip1.SetToolTip(this.btDataFilter, "Add web change");
            this.btDataFilter.UseVisualStyleBackColor = true;
            this.btDataFilter.Click += new System.EventHandler(this.btDataFilter_Click);
            // 
            // labelDataFilter
            // 
            this.labelDataFilter.AutoSize = true;
            this.labelDataFilter.BackColor = System.Drawing.Color.Transparent;
            this.labelDataFilter.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDataFilter.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelDataFilter.Location = new System.Drawing.Point(18, 301);
            this.labelDataFilter.Name = "labelDataFilter";
            this.labelDataFilter.Size = new System.Drawing.Size(76, 16);
            this.labelDataFilter.TabIndex = 10;
            this.labelDataFilter.Text = "Data Filter -";
            // 
            // btWebAdd
            // 
            this.btWebAdd.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btWebAdd.Image = global::CCI.MIP.JobPlugin.UI.Properties.Resources.add_32x32;
            this.btWebAdd.Location = new System.Drawing.Point(420, 295);
            this.btWebAdd.Name = "btWebAdd";
            this.btWebAdd.Size = new System.Drawing.Size(30, 27);
            this.btWebAdd.TabIndex = 5;
            this.btWebAdd.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip1.SetToolTip(this.btWebAdd, "Add web change");
            this.btWebAdd.UseVisualStyleBackColor = true;
            this.btWebAdd.Click += new System.EventHandler(this.btWebAdd_Click);
            // 
            // btBinaryAdd
            // 
            this.btBinaryAdd.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btBinaryAdd.Image = global::CCI.MIP.JobPlugin.UI.Properties.Resources.add_32x32;
            this.btBinaryAdd.Location = new System.Drawing.Point(420, 267);
            this.btBinaryAdd.Name = "btBinaryAdd";
            this.btBinaryAdd.Size = new System.Drawing.Size(30, 27);
            this.btBinaryAdd.TabIndex = 5;
            this.btBinaryAdd.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip1.SetToolTip(this.btBinaryAdd, "Add Binary check");
            this.btBinaryAdd.UseVisualStyleBackColor = true;
            this.btBinaryAdd.Click += new System.EventHandler(this.btBinaryAdd_Click);
            // 
            // btConfigAdd
            // 
            this.btConfigAdd.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btConfigAdd.Image = global::CCI.MIP.JobPlugin.UI.Properties.Resources.add_32x32;
            this.btConfigAdd.Location = new System.Drawing.Point(420, 239);
            this.btConfigAdd.Name = "btConfigAdd";
            this.btConfigAdd.Size = new System.Drawing.Size(30, 27);
            this.btConfigAdd.TabIndex = 5;
            this.btConfigAdd.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip1.SetToolTip(this.btConfigAdd, "Add configuration");
            this.btConfigAdd.UseVisualStyleBackColor = true;
            this.btConfigAdd.Click += new System.EventHandler(this.btConfigAdd_Click);
            // 
            // txtSeqNo
            // 
            this.txtSeqNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSeqNo.Location = new System.Drawing.Point(395, 18);
            this.txtSeqNo.Name = "txtSeqNo";
            this.txtSeqNo.Size = new System.Drawing.Size(55, 20);
            this.txtSeqNo.TabIndex = 9;
            this.txtSeqNo.Leave += new System.EventHandler(this.txtSeqNo_Leave);
            // 
            // dataGrdViewProp
            // 
            this.dataGrdViewProp.AllowUserToDeleteRows = false;
            this.dataGrdViewProp.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGrdViewProp.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGrdViewProp.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PROP_ID,
            this.PLUGIN_CONFIG_ID,
            this.PROP_NAME,
            this.PROP_VALUE});
            this.dataGrdViewProp.Location = new System.Drawing.Point(23, 66);
            this.dataGrdViewProp.Name = "dataGrdViewProp";
            this.dataGrdViewProp.Size = new System.Drawing.Size(427, 134);
            this.dataGrdViewProp.TabIndex = 8;
            this.dataGrdViewProp.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dataGrdViewProp_CellBeginEdit);
            this.dataGrdViewProp.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGrdViewProp_CellClick);
            this.dataGrdViewProp.RowValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGrdViewProp_RowValidated);
            // 
            // PROP_ID
            // 
            this.PROP_ID.HeaderText = "PROP_ID";
            this.PROP_ID.Name = "PROP_ID";
            this.PROP_ID.Visible = false;
            // 
            // PLUGIN_CONFIG_ID
            // 
            this.PLUGIN_CONFIG_ID.HeaderText = "PLUGIN_CONFIG_ID";
            this.PLUGIN_CONFIG_ID.Name = "PLUGIN_CONFIG_ID";
            this.PLUGIN_CONFIG_ID.Visible = false;
            // 
            // PROP_NAME
            // 
            this.PROP_NAME.FillWeight = 101.5228F;
            this.PROP_NAME.HeaderText = "PROP_NAME";
            this.PROP_NAME.Name = "PROP_NAME";
            // 
            // PROP_VALUE
            // 
            this.PROP_VALUE.FillWeight = 98.47716F;
            this.PROP_VALUE.HeaderText = "PROP_VALUE";
            this.PROP_VALUE.Name = "PROP_VALUE";
            // 
            // txtPropId_config
            // 
            this.txtPropId_config.Location = new System.Drawing.Point(450, 80);
            this.txtPropId_config.Name = "txtPropId_config";
            this.txtPropId_config.Size = new System.Drawing.Size(29, 20);
            this.txtPropId_config.TabIndex = 7;
            this.txtPropId_config.Visible = false;
            // 
            // txtPropId_binary
            // 
            this.txtPropId_binary.Location = new System.Drawing.Point(452, 106);
            this.txtPropId_binary.Name = "txtPropId_binary";
            this.txtPropId_binary.Size = new System.Drawing.Size(29, 20);
            this.txtPropId_binary.TabIndex = 7;
            this.txtPropId_binary.Visible = false;
            // 
            // txtprop_datafilterId
            // 
            this.txtprop_datafilterId.Location = new System.Drawing.Point(453, 158);
            this.txtprop_datafilterId.Name = "txtprop_datafilterId";
            this.txtprop_datafilterId.Size = new System.Drawing.Size(29, 20);
            this.txtprop_datafilterId.TabIndex = 7;
            this.txtprop_datafilterId.Visible = false;
            // 
            // txtPropId_webchange
            // 
            this.txtPropId_webchange.Location = new System.Drawing.Point(453, 132);
            this.txtPropId_webchange.Name = "txtPropId_webchange";
            this.txtPropId_webchange.Size = new System.Drawing.Size(29, 20);
            this.txtPropId_webchange.TabIndex = 7;
            this.txtPropId_webchange.Visible = false;
            // 
            // txtpluginId
            // 
            this.txtpluginId.Location = new System.Drawing.Point(448, 43);
            this.txtpluginId.Name = "txtpluginId";
            this.txtpluginId.Size = new System.Drawing.Size(29, 20);
            this.txtpluginId.TabIndex = 7;
            this.txtpluginId.Visible = false;
            // 
            // lblSeqNo
            // 
            this.lblSeqNo.AutoSize = true;
            this.lblSeqNo.BackColor = System.Drawing.Color.Transparent;
            this.lblSeqNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSeqNo.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblSeqNo.Location = new System.Drawing.Point(327, 21);
            this.lblSeqNo.Name = "lblSeqNo";
            this.lblSeqNo.Size = new System.Drawing.Size(61, 16);
            this.lblSeqNo.TabIndex = 6;
            this.lblSeqNo.Text = "SeqNo. -";
            // 
            // btWebRemove
            // 
            this.btWebRemove.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btWebRemove.Image = global::CCI.MIP.JobPlugin.UI.Properties.Resources.cancel_32x32;
            this.btWebRemove.Location = new System.Drawing.Point(459, 296);
            this.btWebRemove.Name = "btWebRemove";
            this.btWebRemove.Size = new System.Drawing.Size(30, 27);
            this.btWebRemove.TabIndex = 5;
            this.btWebRemove.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btWebRemove.UseVisualStyleBackColor = true;
            this.btWebRemove.Visible = false;
            // 
            // btBinaryRemove
            // 
            this.btBinaryRemove.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btBinaryRemove.Image = global::CCI.MIP.JobPlugin.UI.Properties.Resources.cancel_32x32;
            this.btBinaryRemove.Location = new System.Drawing.Point(459, 268);
            this.btBinaryRemove.Name = "btBinaryRemove";
            this.btBinaryRemove.Size = new System.Drawing.Size(30, 27);
            this.btBinaryRemove.TabIndex = 5;
            this.btBinaryRemove.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btBinaryRemove.UseVisualStyleBackColor = true;
            this.btBinaryRemove.Visible = false;
            // 
            // btConfigremove
            // 
            this.btConfigremove.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btConfigremove.Image = global::CCI.MIP.JobPlugin.UI.Properties.Resources.cancel_32x32;
            this.btConfigremove.Location = new System.Drawing.Point(459, 240);
            this.btConfigremove.Name = "btConfigremove";
            this.btConfigremove.Size = new System.Drawing.Size(30, 27);
            this.btConfigremove.TabIndex = 5;
            this.btConfigremove.UseVisualStyleBackColor = true;
            this.btConfigremove.Visible = false;
            // 
            // labelBIN
            // 
            this.labelBIN.AutoSize = true;
            this.labelBIN.BackColor = System.Drawing.Color.Transparent;
            this.labelBIN.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBIN.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelBIN.Location = new System.Drawing.Point(18, 273);
            this.labelBIN.Name = "labelBIN";
            this.labelBIN.Size = new System.Drawing.Size(92, 16);
            this.labelBIN.TabIndex = 4;
            this.labelBIN.Text = "Binary check -";
            // 
            // lblBinary
            // 
            this.lblBinary.AutoSize = true;
            this.lblBinary.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBinary.Location = new System.Drawing.Point(131, 271);
            this.lblBinary.Name = "lblBinary";
            this.lblBinary.Size = new System.Drawing.Size(0, 15);
            this.lblBinary.TabIndex = 4;
            // 
            // labelWEB
            // 
            this.labelWEB.AutoSize = true;
            this.labelWEB.BackColor = System.Drawing.Color.Transparent;
            this.labelWEB.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelWEB.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelWEB.Location = new System.Drawing.Point(18, 302);
            this.labelWEB.Name = "labelWEB";
            this.labelWEB.Size = new System.Drawing.Size(94, 16);
            this.labelWEB.TabIndex = 4;
            this.labelWEB.Text = "Web Change -";
            // 
            // lblWeb
            // 
            this.lblWeb.AutoSize = true;
            this.lblWeb.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWeb.Location = new System.Drawing.Point(129, 302);
            this.lblWeb.Name = "lblWeb";
            this.lblWeb.Size = new System.Drawing.Size(0, 15);
            this.lblWeb.TabIndex = 4;
            // 
            // labelCONFIG
            // 
            this.labelCONFIG.AutoSize = true;
            this.labelCONFIG.BackColor = System.Drawing.Color.Transparent;
            this.labelCONFIG.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCONFIG.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelCONFIG.Location = new System.Drawing.Point(18, 245);
            this.labelCONFIG.Name = "labelCONFIG";
            this.labelCONFIG.Size = new System.Drawing.Size(96, 16);
            this.labelCONFIG.TabIndex = 4;
            this.labelCONFIG.Text = "Configuration  -";
            // 
            // lblConfig
            // 
            this.lblConfig.AutoSize = true;
            this.lblConfig.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConfig.Location = new System.Drawing.Point(131, 244);
            this.lblConfig.Name = "lblConfig";
            this.lblConfig.Size = new System.Drawing.Size(0, 15);
            this.lblConfig.TabIndex = 4;
            // 
            // btDelete
            // 
            this.btDelete.BackColor = System.Drawing.Color.Transparent;
            this.btDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btDelete.Image = global::CCI.MIP.JobPlugin.UI.Properties.Resources.cancel_16x16;
            this.btDelete.Location = new System.Drawing.Point(379, 206);
            this.btDelete.Name = "btDelete";
            this.btDelete.Size = new System.Drawing.Size(73, 26);
            this.btDelete.TabIndex = 2;
            this.btDelete.Text = "Delete";
            this.btDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btDelete.UseVisualStyleBackColor = false;
            this.btDelete.Click += new System.EventHandler(this.btDelete_Click);
            // 
            // btKeep
            // 
            this.btKeep.BackColor = System.Drawing.Color.Transparent;
            this.btKeep.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btKeep.Image = global::CCI.MIP.JobPlugin.UI.Properties.Resources.addfile_16x16;
            this.btKeep.Location = new System.Drawing.Point(309, 206);
            this.btKeep.Name = "btKeep";
            this.btKeep.Size = new System.Drawing.Size(64, 25);
            this.btKeep.TabIndex = 2;
            this.btKeep.Text = "Keep";
            this.btKeep.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btKeep.UseVisualStyleBackColor = false;
            this.btKeep.Click += new System.EventHandler(this.btKeep_Click);
            // 
            // btSave
            // 
            this.btSave.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btSave.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btSave.Image = global::CCI.MIP.JobPlugin.UI.Properties.Resources.addfile_16x16;
            this.btSave.Location = new System.Drawing.Point(227, 206);
            this.btSave.Name = "btSave";
            this.btSave.Size = new System.Drawing.Size(62, 25);
            this.btSave.TabIndex = 2;
            this.btSave.Text = "Keep";
            this.btSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip1.SetToolTip(this.btSave, "Save Plugin and Properties");
            this.btSave.UseVisualStyleBackColor = false;
            this.btSave.Visible = false;
            this.btSave.Click += new System.EventHandler(this.btSave_Click);
            // 
            // txtPulgin
            // 
            this.txtPulgin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPulgin.Location = new System.Drawing.Point(78, 17);
            this.txtPulgin.Name = "txtPulgin";
            this.txtPulgin.Size = new System.Drawing.Size(233, 20);
            this.txtPulgin.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label2.Location = new System.Drawing.Point(18, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 16);
            this.label2.TabIndex = 0;
            this.label2.Text = "Properties";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Location = new System.Drawing.Point(18, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Plugin -";
            // 
            // UserControlCapture
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Name = "UserControlCapture";
            this.Size = new System.Drawing.Size(494, 389);
            this.Load += new System.EventHandler(this.UserControlCapture_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrdViewProp)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btSave;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblSeqNo;
        public System.Windows.Forms.Label lblBinary;
        public System.Windows.Forms.Button btBinaryRemove;
        public System.Windows.Forms.Button btBinaryAdd;
        public System.Windows.Forms.Button btConfigremove;
        public System.Windows.Forms.Button btConfigAdd;
        public System.Windows.Forms.Label lblWeb;
        public System.Windows.Forms.Label lblConfig;
        public System.Windows.Forms.Button btWebRemove;
        public System.Windows.Forms.Button btWebAdd;
        private System.Windows.Forms.ToolTip toolTip1;
        public System.Windows.Forms.TextBox txtPulgin;
        public System.Windows.Forms.DataGridView dataGrdViewProp;
        public System.Windows.Forms.Label labelBIN;
        public System.Windows.Forms.Label labelWEB;
        public System.Windows.Forms.Label labelCONFIG;
        public System.Windows.Forms.TextBox txtSeqNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROP_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn PLUGIN_CONFIG_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROP_NAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROP_VALUE;
        public System.Windows.Forms.TextBox txtpluginId;
        public System.Windows.Forms.TextBox txtPropId_webchange;
        public System.Windows.Forms.TextBox txtPropId_config;
        public System.Windows.Forms.TextBox txtPropId_binary;
        private System.Windows.Forms.Button btDelete;
        private System.Windows.Forms.Button btKeep;
        public System.Windows.Forms.Button btDataFilter;
        public System.Windows.Forms.Label labelDataFilter;
        public System.Windows.Forms.Label lblDataFilter;
        public System.Windows.Forms.TextBox txtprop_datafilterId;
    }
}
