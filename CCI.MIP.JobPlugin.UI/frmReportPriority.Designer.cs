﻿namespace CCI.MIP.JobPlugin.UI
{
    partial class frmReportPriority
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmReportPriority));
            this.btNew = new System.Windows.Forms.Button();
            this.txtReportKey = new System.Windows.Forms.TextBox();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.txtReportName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.grdconrolReport = new DevExpress.XtraGrid.GridControl();
            this.grdViewReport = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.Report_Key = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ReportName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Priority = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Description = new DevExpress.XtraGrid.Columns.GridColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtPriority = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btSave = new System.Windows.Forms.Button();
            this.btUpdate = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.grdconrolReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewReport)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btNew
            // 
            this.btNew.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btNew.Image = global::CCI.MIP.JobPlugin.UI.Properties.Resources.newtask_16x16;
            this.btNew.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btNew.Location = new System.Drawing.Point(487, 260);
            this.btNew.Name = "btNew";
            this.btNew.Size = new System.Drawing.Size(75, 23);
            this.btNew.TabIndex = 4;
            this.btNew.Text = "New";
            this.btNew.UseVisualStyleBackColor = true;
            this.btNew.Click += new System.EventHandler(this.btNew_Click);
            // 
            // txtReportKey
            // 
            this.txtReportKey.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtReportKey.Location = new System.Drawing.Point(391, 369);
            this.txtReportKey.Name = "txtReportKey";
            this.txtReportKey.Size = new System.Drawing.Size(42, 20);
            this.txtReportKey.TabIndex = 3;
            this.txtReportKey.Visible = false;
            // 
            // txtDescription
            // 
            this.txtDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDescription.Location = new System.Drawing.Point(149, 331);
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(218, 58);
            this.txtDescription.TabIndex = 3;
            // 
            // txtReportName
            // 
            this.txtReportName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtReportName.Location = new System.Drawing.Point(149, 263);
            this.txtReportName.Name = "txtReportName";
            this.txtReportName.Size = new System.Drawing.Size(218, 20);
            this.txtReportName.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(21, 263);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "Report Name";
            // 
            // grdconrolReport
            // 
            this.grdconrolReport.Location = new System.Drawing.Point(24, 19);
            this.grdconrolReport.MainView = this.grdViewReport;
            this.grdconrolReport.Name = "grdconrolReport";
            this.grdconrolReport.Size = new System.Drawing.Size(538, 219);
            this.grdconrolReport.TabIndex = 0;
            this.grdconrolReport.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdViewReport});
            // 
            // grdViewReport
            // 
            this.grdViewReport.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.Report_Key,
            this.ReportName,
            this.Priority,
            this.Description});
            this.grdViewReport.GridControl = this.grdconrolReport;
            this.grdViewReport.Name = "grdViewReport";
            this.grdViewReport.OptionsBehavior.Editable = false;
            this.grdViewReport.OptionsView.ShowGroupPanel = false;
            this.grdViewReport.Click += new System.EventHandler(this.grdViewReport_Click);
            // 
            // Report_Key
            // 
            this.Report_Key.Caption = "Report_Key";
            this.Report_Key.Name = "Report_Key";
            // 
            // ReportName
            // 
            this.ReportName.Caption = "Report Name";
            this.ReportName.MinWidth = 120;
            this.ReportName.Name = "ReportName";
            this.ReportName.Visible = true;
            this.ReportName.VisibleIndex = 0;
            this.ReportName.Width = 120;
            // 
            // Priority
            // 
            this.Priority.Caption = "Priority";
            this.Priority.Name = "Priority";
            this.Priority.Visible = true;
            this.Priority.VisibleIndex = 1;
            // 
            // Description
            // 
            this.Description.Caption = "Description";
            this.Description.Name = "Description";
            this.Description.Visible = true;
            this.Description.VisibleIndex = 2;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btNew);
            this.groupBox1.Controls.Add(this.txtReportKey);
            this.groupBox1.Controls.Add(this.txtDescription);
            this.groupBox1.Controls.Add(this.txtPriority);
            this.groupBox1.Controls.Add(this.txtReportName);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.grdconrolReport);
            this.groupBox1.Controls.Add(this.btSave);
            this.groupBox1.Controls.Add(this.btUpdate);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(572, 410);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            // 
            // txtPriority
            // 
            this.txtPriority.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPriority.Location = new System.Drawing.Point(149, 297);
            this.txtPriority.Name = "txtPriority";
            this.txtPriority.Size = new System.Drawing.Size(218, 20);
            this.txtPriority.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(21, 331);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 16);
            this.label3.TabIndex = 1;
            this.label3.Text = "Description";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(21, 297);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Report Priority";
            // 
            // btSave
            // 
            this.btSave.BackColor = System.Drawing.Color.Transparent;
            this.btSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btSave.Image = global::CCI.MIP.JobPlugin.UI.Properties.Resources.save_16x16;
            this.btSave.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btSave.Location = new System.Drawing.Point(476, 361);
            this.btSave.Name = "btSave";
            this.btSave.Size = new System.Drawing.Size(85, 30);
            this.btSave.TabIndex = 18;
            this.btSave.Text = "Save";
            this.btSave.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btSave.UseVisualStyleBackColor = false;
            this.btSave.Visible = false;
            this.btSave.Click += new System.EventHandler(this.btSave_Click);
            // 
            // btUpdate
            // 
            this.btUpdate.BackColor = System.Drawing.Color.Transparent;
            this.btUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btUpdate.Image = global::CCI.MIP.JobPlugin.UI.Properties.Resources.saveandnew_16x16;
            this.btUpdate.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btUpdate.Location = new System.Drawing.Point(477, 361);
            this.btUpdate.Name = "btUpdate";
            this.btUpdate.Size = new System.Drawing.Size(85, 30);
            this.btUpdate.TabIndex = 18;
            this.btUpdate.Text = "Update";
            this.btUpdate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btUpdate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btUpdate.UseVisualStyleBackColor = false;
            this.btUpdate.Visible = false;
            this.btUpdate.Click += new System.EventHandler(this.btUpdate_Click);
            // 
            // frmReportPriority
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(572, 410);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmReportPriority";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Report Priority";
            this.Load += new System.EventHandler(this.frmReportPriority_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grdconrolReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewReport)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btNew;
        private System.Windows.Forms.TextBox txtReportKey;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.TextBox txtReportName;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraGrid.GridControl grdconrolReport;
        private DevExpress.XtraGrid.Views.Grid.GridView grdViewReport;
        private DevExpress.XtraGrid.Columns.GridColumn Report_Key;
        private DevExpress.XtraGrid.Columns.GridColumn ReportName;
        private DevExpress.XtraGrid.Columns.GridColumn Priority;
        private DevExpress.XtraGrid.Columns.GridColumn Description;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtPriority;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btUpdate;
        private System.Windows.Forms.Button btSave;
    }
}