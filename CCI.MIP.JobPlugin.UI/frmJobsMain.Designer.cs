﻿namespace CCI.MIP.JobPlugin.UI
{
    partial class frmJobMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmJobMain));
            this.gridControlJobs = new DevExpress.XtraGrid.GridControl();
            this.gridViewJobSchedule = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.JOB_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.JOB_NAME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DESK = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CALENDAR = new DevExpress.XtraGrid.Columns.GridColumn();
            this.START_TIME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.END_TIME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DURATION = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FREQUENCY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ACTIVE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LAST_UPDATE_DATE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.toolStripJobOption = new System.Windows.Forms.ToolStrip();
            this.btnAdd = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.toolbtcopy = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator12 = new System.Windows.Forms.ToolStripSeparator();
            this.toolbtEdit = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolbtRefresh = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolbtRunNow = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.toolbtRerun = new System.Windows.Forms.ToolStripButton();
            this.toolStripbtAbout = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator13 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripBtSettings = new System.Windows.Forms.ToolStripButton();
            this.toolstripMenu = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.tsBtCSVcreation = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbtUploadNew = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbtReplay = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbtCalendar = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripBtMailGroup = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel3 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator14 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.updateSeriesNameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addProductGroupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportPriorityToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addSourceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlJobs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewJobSchedule)).BeginInit();
            this.toolStripJobOption.SuspendLayout();
            this.toolstripMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // gridControlJobs
            // 
            this.gridControlJobs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlJobs.Location = new System.Drawing.Point(161, 25);
            this.gridControlJobs.LookAndFeel.SkinName = "Office 2010 Silver";
            this.gridControlJobs.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.gridControlJobs.LookAndFeel.UseWindowsXPTheme = true;
            this.gridControlJobs.MainView = this.gridViewJobSchedule;
            this.gridControlJobs.Name = "gridControlJobs";
            this.gridControlJobs.Size = new System.Drawing.Size(830, 607);
            this.gridControlJobs.TabIndex = 0;
            this.gridControlJobs.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewJobSchedule});
            // 
            // gridViewJobSchedule
            // 
            this.gridViewJobSchedule.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.gridViewJobSchedule.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridViewJobSchedule.Appearance.GroupRow.BackColor = System.Drawing.Color.White;
            this.gridViewJobSchedule.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridViewJobSchedule.Appearance.OddRow.BackColor = System.Drawing.Color.White;
            this.gridViewJobSchedule.Appearance.OddRow.Options.UseBackColor = true;
            this.gridViewJobSchedule.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.gridViewJobSchedule.Appearance.Row.Options.UseBackColor = true;
            this.gridViewJobSchedule.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.gridViewJobSchedule.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.JOB_ID,
            this.JOB_NAME,
            this.DESK,
            this.CALENDAR,
            this.START_TIME,
            this.END_TIME,
            this.DURATION,
            this.FREQUENCY,
            this.ACTIVE,
            this.LAST_UPDATE_DATE});
            this.gridViewJobSchedule.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridViewJobSchedule.GridControl = this.gridControlJobs;
            this.gridViewJobSchedule.Name = "gridViewJobSchedule";
            this.gridViewJobSchedule.OptionsBehavior.Editable = false;
            this.gridViewJobSchedule.OptionsFilter.ShowAllTableValuesInFilterPopup = true;
            this.gridViewJobSchedule.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewJobSchedule.OptionsView.AllowGlyphSkinning = true;
            this.gridViewJobSchedule.OptionsView.ShowAutoFilterRow = true;
            this.gridViewJobSchedule.OptionsView.ShowGroupPanel = false;
            this.gridViewJobSchedule.PaintStyleName = "Office2003";
            this.gridViewJobSchedule.CustomColumnSort += new DevExpress.XtraGrid.Views.Base.CustomColumnSortEventHandler(this.gridViewJobSchedule_CustomColumnSort);
            this.gridViewJobSchedule.DoubleClick += new System.EventHandler(this.gridViewJobSchedule_DoubleClick);
            // 
            // JOB_ID
            // 
            this.JOB_ID.AppearanceHeader.BorderColor = System.Drawing.Color.Black;
            this.JOB_ID.AppearanceHeader.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.JOB_ID.AppearanceHeader.Options.UseBorderColor = true;
            this.JOB_ID.Caption = "JOB_ID";
            this.JOB_ID.Name = "JOB_ID";
            this.JOB_ID.OptionsColumn.AllowEdit = false;
            this.JOB_ID.Visible = true;
            this.JOB_ID.VisibleIndex = 0;
            this.JOB_ID.Width = 46;
            // 
            // JOB_NAME
            // 
            this.JOB_NAME.Caption = "JOB_NAME";
            this.JOB_NAME.Name = "JOB_NAME";
            this.JOB_NAME.OptionsColumn.AllowEdit = false;
            this.JOB_NAME.Visible = true;
            this.JOB_NAME.VisibleIndex = 1;
            this.JOB_NAME.Width = 119;
            // 
            // DESK
            // 
            this.DESK.Caption = "DESK";
            this.DESK.Name = "DESK";
            this.DESK.OptionsColumn.AllowEdit = false;
            this.DESK.Visible = true;
            this.DESK.VisibleIndex = 2;
            this.DESK.Width = 81;
            // 
            // CALENDAR
            // 
            this.CALENDAR.Caption = "CALENDAR";
            this.CALENDAR.Name = "CALENDAR";
            this.CALENDAR.OptionsColumn.AllowEdit = false;
            this.CALENDAR.Visible = true;
            this.CALENDAR.VisibleIndex = 3;
            this.CALENDAR.Width = 56;
            // 
            // START_TIME
            // 
            this.START_TIME.Caption = "START_TIME";
            this.START_TIME.Name = "START_TIME";
            this.START_TIME.OptionsColumn.AllowEdit = false;
            this.START_TIME.SortMode = DevExpress.XtraGrid.ColumnSortMode.Custom;
            this.START_TIME.Visible = true;
            this.START_TIME.VisibleIndex = 4;
            this.START_TIME.Width = 62;
            // 
            // END_TIME
            // 
            this.END_TIME.Caption = "END_TIME";
            this.END_TIME.Name = "END_TIME";
            this.END_TIME.OptionsColumn.AllowEdit = false;
            this.END_TIME.SortMode = DevExpress.XtraGrid.ColumnSortMode.Custom;
            this.END_TIME.Visible = true;
            this.END_TIME.VisibleIndex = 5;
            this.END_TIME.Width = 59;
            // 
            // DURATION
            // 
            this.DURATION.Caption = "DURATION";
            this.DURATION.Name = "DURATION";
            this.DURATION.OptionsColumn.AllowEdit = false;
            this.DURATION.Visible = true;
            this.DURATION.VisibleIndex = 6;
            this.DURATION.Width = 57;
            // 
            // FREQUENCY
            // 
            this.FREQUENCY.Caption = "FREQUENCY";
            this.FREQUENCY.Name = "FREQUENCY";
            this.FREQUENCY.OptionsColumn.AllowEdit = false;
            this.FREQUENCY.Visible = true;
            this.FREQUENCY.VisibleIndex = 7;
            this.FREQUENCY.Width = 68;
            // 
            // ACTIVE
            // 
            this.ACTIVE.Caption = "ACTIVE";
            this.ACTIVE.Name = "ACTIVE";
            this.ACTIVE.OptionsColumn.AllowEdit = false;
            this.ACTIVE.Visible = true;
            this.ACTIVE.VisibleIndex = 8;
            this.ACTIVE.Width = 45;
            // 
            // LAST_UPDATE_DATE
            // 
            this.LAST_UPDATE_DATE.Caption = "LAST_UPDATE_DATE";
            this.LAST_UPDATE_DATE.FieldName = "LAST_UPDATE_DATE";
            this.LAST_UPDATE_DATE.Name = "LAST_UPDATE_DATE";
            this.LAST_UPDATE_DATE.OptionsColumn.AllowEdit = false;
            this.LAST_UPDATE_DATE.UnboundType = DevExpress.Data.UnboundColumnType.DateTime;
            this.LAST_UPDATE_DATE.Visible = true;
            this.LAST_UPDATE_DATE.VisibleIndex = 9;
            this.LAST_UPDATE_DATE.Width = 104;
            // 
            // toolStripJobOption
            // 
            this.toolStripJobOption.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("toolStripJobOption.BackgroundImage")));
            this.toolStripJobOption.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripJobOption.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAdd,
            this.toolStripSeparator11,
            this.toolbtcopy,
            this.toolStripSeparator12,
            this.toolbtEdit,
            this.toolStripSeparator1,
            this.toolbtRefresh,
            this.toolStripSeparator2,
            this.toolbtRunNow,
            this.toolStripSeparator8,
            this.toolbtRerun,
            this.toolStripbtAbout,
            this.toolStripSeparator13,
            this.toolStripBtSettings});
            this.toolStripJobOption.Location = new System.Drawing.Point(161, 0);
            this.toolStripJobOption.Name = "toolStripJobOption";
            this.toolStripJobOption.Size = new System.Drawing.Size(830, 25);
            this.toolStripJobOption.TabIndex = 1;
            this.toolStripJobOption.Text = "toolStrip1";
            // 
            // btnAdd
            // 
            this.btnAdd.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAdd.Image = global::CCI.MIP.JobPlugin.UI.Properties.Resources.add_32x32;
            this.btnAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(49, 22);
            this.btnAdd.Text = "Add";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(6, 25);
            // 
            // toolbtcopy
            // 
            this.toolbtcopy.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolbtcopy.Image = global::CCI.MIP.JobPlugin.UI.Properties.Resources.copy_16x16;
            this.toolbtcopy.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolbtcopy.Name = "toolbtcopy";
            this.toolbtcopy.Size = new System.Drawing.Size(60, 22);
            this.toolbtcopy.Text = "Copy";
            this.toolbtcopy.Click += new System.EventHandler(this.toolbtcopy_Click);
            // 
            // toolStripSeparator12
            // 
            this.toolStripSeparator12.Name = "toolStripSeparator12";
            this.toolStripSeparator12.Size = new System.Drawing.Size(6, 25);
            // 
            // toolbtEdit
            // 
            this.toolbtEdit.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolbtEdit.Image = global::CCI.MIP.JobPlugin.UI.Properties.Resources.edit_32x32;
            this.toolbtEdit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolbtEdit.Name = "toolbtEdit";
            this.toolbtEdit.Size = new System.Drawing.Size(52, 22);
            this.toolbtEdit.Text = "Edit";
            this.toolbtEdit.Click += new System.EventHandler(this.toolbtEdit_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolbtRefresh
            // 
            this.toolbtRefresh.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolbtRefresh.Image = global::CCI.MIP.JobPlugin.UI.Properties.Resources.refresh2_32x32;
            this.toolbtRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolbtRefresh.Name = "toolbtRefresh";
            this.toolbtRefresh.Size = new System.Drawing.Size(76, 22);
            this.toolbtRefresh.Text = "Refresh";
            this.toolbtRefresh.Click += new System.EventHandler(this.toolbtRefresh_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolbtRunNow
            // 
            this.toolbtRunNow.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolbtRunNow.Image = global::CCI.MIP.JobPlugin.UI.Properties.Resources.next_16x16;
            this.toolbtRunNow.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolbtRunNow.Name = "toolbtRunNow";
            this.toolbtRunNow.Size = new System.Drawing.Size(84, 22);
            this.toolbtRunNow.Text = "Run Now";
            this.toolbtRunNow.Click += new System.EventHandler(this.toolbtRunNow_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(6, 25);
            // 
            // toolbtRerun
            // 
            this.toolbtRerun.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolbtRerun.Image = global::CCI.MIP.JobPlugin.UI.Properties.Resources.last_16x16;
            this.toolbtRerun.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolbtRerun.Name = "toolbtRerun";
            this.toolbtRerun.Size = new System.Drawing.Size(68, 22);
            this.toolbtRerun.Text = "ReRun";
            this.toolbtRerun.Click += new System.EventHandler(this.toolbtRerun_Click);
            // 
            // toolStripbtAbout
            // 
            this.toolStripbtAbout.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolStripbtAbout.Image = global::CCI.MIP.JobPlugin.UI.Properties.Resources.favicon;
            this.toolStripbtAbout.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripbtAbout.Name = "toolStripbtAbout";
            this.toolStripbtAbout.Size = new System.Drawing.Size(56, 22);
            this.toolStripbtAbout.Text = "Help";
            this.toolStripbtAbout.Click += new System.EventHandler(this.toolStripbtAbout_Click);
            // 
            // toolStripSeparator13
            // 
            this.toolStripSeparator13.Name = "toolStripSeparator13";
            this.toolStripSeparator13.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripBtSettings
            // 
            this.toolStripBtSettings.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolStripBtSettings.Image = global::CCI.MIP.JobPlugin.UI.Properties.Resources.build_16x16;
            this.toolStripBtSettings.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripBtSettings.Name = "toolStripBtSettings";
            this.toolStripBtSettings.Size = new System.Drawing.Size(107, 22);
            this.toolStripBtSettings.Text = "Series Name";
            this.toolStripBtSettings.Visible = false;
            this.toolStripBtSettings.Click += new System.EventHandler(this.toolStripBtSettings_Click);
            // 
            // toolstripMenu
            // 
            this.toolstripMenu.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("toolstripMenu.BackgroundImage")));
            this.toolstripMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.toolstripMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.toolStripSeparator3,
            this.toolStripLabel2,
            this.toolStripSeparator4,
            this.tsBtCSVcreation,
            this.toolStripSeparator5,
            this.tsbtUploadNew,
            this.toolStripSeparator6,
            this.tsbtReplay,
            this.toolStripSeparator7,
            this.tsbtCalendar,
            this.toolStripSeparator9,
            this.toolStripBtMailGroup,
            this.toolStripSeparator10,
            this.toolStripLabel3,
            this.toolStripSeparator14,
            this.toolStripDropDownButton1});
            this.toolstripMenu.Location = new System.Drawing.Point(0, 0);
            this.toolstripMenu.Name = "toolstripMenu";
            this.toolstripMenu.Size = new System.Drawing.Size(161, 632);
            this.toolstripMenu.TabIndex = 4;
            this.toolstripMenu.Text = "toolStrip2";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.toolStripLabel1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripLabel1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripLabel1.Image")));
            this.toolStripLabel1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(158, 63);
            this.toolStripLabel1.Text = "toolStripLabel1";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(158, 6);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Font = new System.Drawing.Font("MS Reference Sans Serif", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripLabel2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(158, 19);
            this.toolStripLabel2.Text = "Job Details";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(158, 6);
            // 
            // tsBtCSVcreation
            // 
            this.tsBtCSVcreation.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsBtCSVcreation.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.tsBtCSVcreation.Image = global::CCI.MIP.JobPlugin.UI.Properties.Resources.exporttocsv_32x32;
            this.tsBtCSVcreation.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtCSVcreation.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtCSVcreation.Name = "tsBtCSVcreation";
            this.tsBtCSVcreation.Size = new System.Drawing.Size(158, 52);
            this.tsBtCSVcreation.Text = "MetaData CSV Creation";
            this.tsBtCSVcreation.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsBtCSVcreation.Click += new System.EventHandler(this.tsBtCSVcreation_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(158, 6);
            // 
            // tsbtUploadNew
            // 
            this.tsbtUploadNew.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsbtUploadNew.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.tsbtUploadNew.Image = global::CCI.MIP.JobPlugin.UI.Properties.Resources.download_32x32;
            this.tsbtUploadNew.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbtUploadNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtUploadNew.Name = "tsbtUploadNew";
            this.tsbtUploadNew.Size = new System.Drawing.Size(158, 52);
            this.tsbtUploadNew.Text = "Upload New Series";
            this.tsbtUploadNew.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbtUploadNew.Click += new System.EventHandler(this.tsbtUploadNew_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(158, 6);
            // 
            // tsbtReplay
            // 
            this.tsbtReplay.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsbtReplay.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.tsbtReplay.Image = global::CCI.MIP.JobPlugin.UI.Properties.Resources.doublenext_32x32;
            this.tsbtReplay.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbtReplay.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtReplay.Name = "tsbtReplay";
            this.tsbtReplay.Size = new System.Drawing.Size(158, 52);
            this.tsbtReplay.Text = "Replay";
            this.tsbtReplay.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbtReplay.Click += new System.EventHandler(this.tsbtReplay_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(158, 6);
            // 
            // tsbtCalendar
            // 
            this.tsbtCalendar.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsbtCalendar.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.tsbtCalendar.Image = global::CCI.MIP.JobPlugin.UI.Properties.Resources.calendar_32x32;
            this.tsbtCalendar.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbtCalendar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtCalendar.Name = "tsbtCalendar";
            this.tsbtCalendar.Size = new System.Drawing.Size(158, 52);
            this.tsbtCalendar.Text = "Calendar";
            this.tsbtCalendar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(158, 6);
            // 
            // toolStripBtMailGroup
            // 
            this.toolStripBtMailGroup.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.toolStripBtMailGroup.Image = global::CCI.MIP.JobPlugin.UI.Properties.Resources.mail_32x32;
            this.toolStripBtMailGroup.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripBtMailGroup.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripBtMailGroup.Name = "toolStripBtMailGroup";
            this.toolStripBtMailGroup.Size = new System.Drawing.Size(158, 49);
            this.toolStripBtMailGroup.Text = "Mail Group";
            this.toolStripBtMailGroup.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripBtMailGroup.Click += new System.EventHandler(this.toolStripBtMailGroup_Click);
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(158, 6);
            // 
            // toolStripLabel3
            // 
            this.toolStripLabel3.Font = new System.Drawing.Font("MS Reference Sans Serif", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripLabel3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.toolStripLabel3.Name = "toolStripLabel3";
            this.toolStripLabel3.Size = new System.Drawing.Size(158, 19);
            this.toolStripLabel3.Text = "Series Details";
            // 
            // toolStripSeparator14
            // 
            this.toolStripSeparator14.Name = "toolStripSeparator14";
            this.toolStripSeparator14.Size = new System.Drawing.Size(158, 6);
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.BackColor = System.Drawing.Color.Transparent;
            this.toolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.updateSeriesNameToolStripMenuItem,
            this.addProductGroupToolStripMenuItem,
            this.reportPriorityToolStripMenuItem,
            this.addSourceToolStripMenuItem});
            this.toolStripDropDownButton1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripDropDownButton1.ForeColor = System.Drawing.Color.White;
            this.toolStripDropDownButton1.Image = global::CCI.MIP.JobPlugin.UI.Properties.Resources.customizegrid_32x32;
            this.toolStripDropDownButton1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(158, 52);
            this.toolStripDropDownButton1.Text = "Series Details Menu";
            this.toolStripDropDownButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // updateSeriesNameToolStripMenuItem
            // 
            this.updateSeriesNameToolStripMenuItem.Image = global::CCI.MIP.JobPlugin.UI.Properties.Resources.edit_32x32;
            this.updateSeriesNameToolStripMenuItem.Name = "updateSeriesNameToolStripMenuItem";
            this.updateSeriesNameToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.updateSeriesNameToolStripMenuItem.Text = "Update Series Name";
            this.updateSeriesNameToolStripMenuItem.Click += new System.EventHandler(this.updateSeriesNameToolStripMenuItem_Click);
            // 
            // addProductGroupToolStripMenuItem
            // 
            this.addProductGroupToolStripMenuItem.Image = global::CCI.MIP.JobPlugin.UI.Properties.Resources.addfile_16x16;
            this.addProductGroupToolStripMenuItem.Name = "addProductGroupToolStripMenuItem";
            this.addProductGroupToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.addProductGroupToolStripMenuItem.Text = "Add Product Group";
            this.addProductGroupToolStripMenuItem.Click += new System.EventHandler(this.addProductGroupToolStripMenuItem_Click);
            // 
            // reportPriorityToolStripMenuItem
            // 
            this.reportPriorityToolStripMenuItem.Image = global::CCI.MIP.JobPlugin.UI.Properties.Resources.add_32x32;
            this.reportPriorityToolStripMenuItem.Name = "reportPriorityToolStripMenuItem";
            this.reportPriorityToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.reportPriorityToolStripMenuItem.Text = "Report Priority";
            this.reportPriorityToolStripMenuItem.Click += new System.EventHandler(this.reportPriorityToolStripMenuItem_Click);
            // 
            // addSourceToolStripMenuItem
            // 
            this.addSourceToolStripMenuItem.Image = global::CCI.MIP.JobPlugin.UI.Properties.Resources.addfile_16x16;
            this.addSourceToolStripMenuItem.Name = "addSourceToolStripMenuItem";
            this.addSourceToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.addSourceToolStripMenuItem.Text = "Add Source";
            this.addSourceToolStripMenuItem.Click += new System.EventHandler(this.addSourceToolStripMenuItem_Click);
            // 
            // frmJobMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(61)))), ((int)(((byte)(101)))));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(991, 632);
            this.Controls.Add(this.gridControlJobs);
            this.Controls.Add(this.toolStripJobOption);
            this.Controls.Add(this.toolstripMenu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmJobMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Jobs";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmJobMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlJobs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewJobSchedule)).EndInit();
            this.toolStripJobOption.ResumeLayout(false);
            this.toolStripJobOption.PerformLayout();
            this.toolstripMenu.ResumeLayout(false);
            this.toolstripMenu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion
        private DevExpress.XtraGrid.GridControl gridControlJobs;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewJobSchedule;
        private System.Windows.Forms.ToolStrip toolStripJobOption;
        private System.Windows.Forms.ToolStripButton btnAdd;
        private System.Windows.Forms.ToolStripButton toolbtEdit;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolbtRefresh;
        private DevExpress.XtraGrid.Columns.GridColumn JOB_ID;
        private DevExpress.XtraGrid.Columns.GridColumn JOB_NAME;
        private DevExpress.XtraGrid.Columns.GridColumn DESK;
        private DevExpress.XtraGrid.Columns.GridColumn CALENDAR;
        private DevExpress.XtraGrid.Columns.GridColumn START_TIME;
        private DevExpress.XtraGrid.Columns.GridColumn END_TIME;
        private DevExpress.XtraGrid.Columns.GridColumn DURATION;
        private DevExpress.XtraGrid.Columns.GridColumn FREQUENCY;
        private DevExpress.XtraGrid.Columns.GridColumn ACTIVE;
        private DevExpress.XtraGrid.Columns.GridColumn LAST_UPDATE_DATE;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton toolbtRunNow;
        private System.Windows.Forms.ToolStrip toolstripMenu;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton tsBtCSVcreation;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripButton tsbtUploadNew;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripButton tsbtReplay;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripButton tsbtCalendar;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripButton toolbtRerun;
        private System.Windows.Forms.ToolStripButton toolStripBtMailGroup;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.ToolStripButton toolbtcopy;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator12;
        private System.Windows.Forms.ToolStripButton toolStripbtAbout;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator13;
        private System.Windows.Forms.ToolStripButton toolStripBtSettings;
        private System.Windows.Forms.ToolStripLabel toolStripLabel3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator14;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.ToolStripMenuItem updateSeriesNameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addProductGroupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportPriorityToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addSourceToolStripMenuItem;

    }
}
