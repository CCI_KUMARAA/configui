﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CCI.MIP.JobPlugin.UI
{
    public partial class frmCopyJobName : Form
    {
        public frmCopyJobName()
        {
            InitializeComponent();
        }
        public  frmCopyJobName(ref string newJobname)
        {
           
                
        }
        private void btCopySave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtNewJobName.Text != null && txtNewJobName.Text != "")
                {
                    Program.NewJobName = txtNewJobName.Text;
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Please enter Job Name.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
            catch
            {

            }
            
        }
    }
}
