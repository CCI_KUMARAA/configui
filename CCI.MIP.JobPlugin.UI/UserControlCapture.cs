﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraRichEdit;
using DevExpress.XtraRichEdit.API.Native;
using DevExpress.XtraRichEdit.Services;

namespace CCI.MIP.JobPlugin.UI
{
    public partial class UserControlCapture : UserControl
    {
        private static readonly log4net.ILog _log = log4net.LogManager.GetLogger(typeof(UserControlCapture));
        JobConfigWinService.JobConfigServiceClient jobref = new JobConfigWinService.JobConfigServiceClient();
        JobConfigWinService.JobConfigPlugin jobscd = new JobConfigWinService.JobConfigPlugin();
   

        //public static frmJobSchedule frmjs = new frmJobSchedule();
  

             
        public UserControlCapture()
        {
            InitializeComponent();
        }
        private void btSearch_Click(object sender, EventArgs e)
        {
            //jobscd = new JobConfigWinService.JobConfigPlugin();
            //frmJobSchedule js=new frmJobSchedule();
            //string enterplugin=txtPulgin.Text;
            //jobscd.Plugin = enterplugin;
            //jobscd.EngineType = "Capture";
            //jobscd.JobId = Program.globaljobid;

            //DataSet ds = jobref.GetJobPluginProp(jobscd);
            //if (ds.Tables[0].Rows.Count > 0)
            //{
                

            //    lblSeqNo.Text = "Seq No.- " + ds.Tables[0].Rows[0].ItemArray[2].ToString();
            //    for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
            //    {
            //        if (ds.Tables[0].Rows[i].ItemArray[7].ToString().ToUpper() == "WEBCHANGE")
            //        {
            //            lblWeb.Text = "Web Change :- " + ds.Tables[0].Rows[i].ItemArray[8].ToString();
            //        }
            //        if (ds.Tables[0].Rows[i].ItemArray[7].ToString().ToUpper() == "BINARY")
            //        {
            //            lblWeb.Text = "Binary :- " + ds.Tables[0].Rows[i].ItemArray[8].ToString();
            //        }
            //    }
            //}
            //else
            //{
            //    MessageBox.Show("Record not found !","Capture",MessageBoxButtons.OK,MessageBoxIcon.Warning);
            //}
             
        }
        private void btConfigAdd_Click(object sender, EventArgs e)
        {
            try
            {
                _log.Info("calling btConfigAdd_Click");
                frmConfiguration frmconfig = new frmConfiguration();
                if (txtpluginId.Text != "")
                {
                    frmconfig.txtPluginId.Text = txtpluginId.Text;
                    frmconfig.txtPropId_config.Text = txtPropId_config.Text;
                    
                    jobscd = new JobConfigWinService.JobConfigPlugin();

                    if (lblConfig.Text != string.Empty)
                    {

                        jobscd.ConfigGUIDkey = lblConfig.Text;
                        DataSet ds = jobref.GetConfigReposGUID(jobscd);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            string strGUIDkey = ds.Tables[0].Rows[0].ItemArray[1].ToString();
                            frmconfig.txtGUIDfullname.Text = strGUIDkey;
                            string[] strsplitGUID = strGUIDkey.Split('-');
                            int len=strsplitGUID.Length;
                            frmconfig.cmbPropName.Text = strsplitGUID[1];
                            frmconfig.txtpropGUID.Text = strsplitGUID[0] + "-" + strsplitGUID[1] + "-";
                            frmconfig.txtVersionGUID.Text="-"+ strsplitGUID[len-2] +"-"+ strsplitGUID[len-1];
                            frmconfig.txtVersion.Text = strsplitGUID[len - 1];
                            frmconfig.txtseqnumber.Text = strsplitGUID[len - 2];
                            for (int i = 2; i < len - 2; i++)
                            {
                                if (frmconfig.txtGUID.Text == "")
                                {
                                    frmconfig.txtGUID.Text = strsplitGUID[i];
                                }
                                else
                                {
                                    frmconfig.txtGUID.Text = frmconfig.txtGUID.Text + "-" + strsplitGUID[i];
                                }
                            }
                           
                           frmconfig.txtScript.Text = ds.Tables[0].Rows[0].ItemArray[2].ToString();
                           
                           frmconfig.rchEditControl.ReplaceService<ISyntaxHighlightService>(new SyntaxHighlight(frmconfig.rchEditControl));
                           
                           frmconfig.rchEditControl.Text = ds.Tables[0].Rows[0].ItemArray[2].ToString();
                           frmconfig.rtbscript.Text = ds.Tables[0].Rows[0].ItemArray[2].ToString();

                            frmconfig.txtconfigID.Text = ds.Tables[0].Rows[0].ItemArray[0].ToString();
                            frmconfig.btUpdate.Visible = true;
                            frmconfig.btNew.Visible = true;

                        }

                    }
                    else
                    {
                        var fm = frmJobSchedule.MainFormRef;
                        frmconfig.btAdd.Visible = true;
                        frmconfig.btNew.Visible = false;
                        frmconfig.txtVersion.Text = "1";
                        string strjobname = Program.JobName;//fm.txtJobName.Text;
                        strjobname = strjobname.Replace('-', '_');
                        strjobname = strjobname.Replace(' ', '_');
                        frmconfig.txtGUID.Text = strjobname;

                    }
                    frmconfig.ShowDialog();
                    lblConfig.Text = frmconfig.txtGUIDfullname.Text;
                    refreshDataGrid();
                    //frmjs.RefreshCaptureTab();
                    //frmjs.RefreshTransformTab();
                    //frmjs.RefreshLoadTab();
                }
                else
                {
                    MessageBox.Show("Please save Plugin first  ", "Plugin", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred while trying to save." + ex.Message, "Plugin prop", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);
            }
        }
        private void btSave_Click(object sender, EventArgs e)
        {
            try
            {
                _log.Info("calling btSave_Click");
                jobscd = new JobConfigWinService.JobConfigPlugin();
                if (txtPulgin.Text != "" && txtSeqNo.Text != "")
                {
                    string tabtext = Program.tabselectedvalue;
                    string[] type = tabtext.Split('-');
                    string enginetype = type[0].ToString();
                    int seqno = Convert.ToInt32( txtSeqNo.Text);//Convert.ToInt32(type[1]);
                    jobscd.Plugin = txtPulgin.Text;
                    jobscd.JobId = Program.globaljobid;
                    jobscd.Seqnum = seqno;
                    jobscd.EngineType = enginetype;

                    int iresult=0, iresultprop=0;

               
                    DataSet ds = new DataSet();
                    if (txtpluginId.Text != "")
                    {
                        jobscd.PluginConfigId = Convert.ToInt32( txtpluginId.Text);
                        iresult = jobref.UpdateJobPluginConfig(jobscd);
                        jobscd.PluginConfigId = Convert.ToInt32(txtpluginId.Text);
                    }
                    else
                    {
                        //ds = jobref.GetJobPluginConfigWithPlugin(jobscd);
                        //if (ds.Tables[0].Rows.Count == 0)
                        //{
                            iresult = jobref.InsertJobPluginConfig(jobscd);
                            ds = jobref.GetJobPluginConfigWithPlugin(jobscd);
                            if (ds.Tables.Count > 0)
                            {
                                if (ds.Tables[0].Rows.Count > 0)
                                {
                                    int pluginconfigId = Convert.ToInt32(ds.Tables[0].Rows[0].ItemArray[0].ToString());
                                    jobscd.PluginConfigId = pluginconfigId;
                                }
                            }

                        //}
                    }

                    
                    for (int i = 0; i <= dataGrdViewProp.Rows.Count - 2; i++)
                    {
                        int propId = Convert.ToInt32(dataGrdViewProp.Rows[i].Cells[0].Value);
                        string propname = dataGrdViewProp.Rows[i].Cells[2].Value.ToString();
                        string propvalue = dataGrdViewProp.Rows[i].Cells[3].Value.ToString();
                        jobscd.PropId = propId;
                        jobscd.PropName = propname;
                        jobscd.PropValue = propvalue;

                        if (propId == null || propId == 0)
                        {
                            iresultprop = jobref.InsertJobPluginProp(jobscd);
                        }
                        else
                        {
                            iresultprop = jobref.UpdateJobPluginProp(jobscd);
                        }

                    }
                    if (iresultprop == 1 || iresult == 1)
                    {
                        //MessageBox.Show("Record inserted successfully!", "Properties", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        refreshDataGrid();
                    }

                }
                else
                {
                    MessageBox.Show("Please enter Plugin and SeqNo", "Properties", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred while trying to save plugin property." + ex.Message, "Plugin prop", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);
            }
        }
        public void refreshDataGrid()
        {
            try
            {
                _log.Info("calling refreshDataGrid");
                jobscd = new JobConfigWinService.JobConfigPlugin();
                string tabtext = Program.tabselectedvalue;
                string[] type = tabtext.Split('-');
                string enginetype = type[0].ToString();
                int seqno = Convert.ToInt32(txtSeqNo.Text);//Convert.ToInt32(type[1]);
                int cnt = 0;

                jobscd.Plugin = txtPulgin.Text;
                jobscd.JobId = Program.globaljobid;
                jobscd.Seqnum = seqno;
                jobscd.EngineType = enginetype;


                DataSet ds = jobref.GetJobPluginConfigWithPlugin(jobscd);

                txtpluginId.Text = ds.Tables[0].Rows[0].ItemArray[0].ToString();
                jobscd.PluginId = Convert.ToInt32(txtpluginId.Text);
                DataSet dstranform = jobref.GetJobPluginPropWitPluginId(jobscd);
                int row = dstranform.Tables[0].Rows.Count - 1;

                dataGrdViewProp.Rows.Clear();
                dataGrdViewProp.Refresh();

                if (dstranform.Tables[0].Rows.Count > 0)
                {

                    for (int k = 0; k <= dstranform.Tables[0].Rows.Count - 1; k++)
                    {
                        
                        if (dataGrdViewProp.Rows.Count <= dstranform.Tables[0].Rows.Count)
                        {

                            dataGrdViewProp.Rows.Add();
                            cnt = cnt + 1;

                        }
                        //}
                    }
                }


                for (int r = 0; r <= row; r++)
                {

                    dataGrdViewProp.Rows[r].Cells[0].Value = dstranform.Tables[0].Rows[r].ItemArray[0];
                    dataGrdViewProp.Rows[r].Cells[1].Value = dstranform.Tables[0].Rows[r].ItemArray[1];
                    dataGrdViewProp.Rows[r].Cells[2].Value = dstranform.Tables[0].Rows[r].ItemArray[2];
                    dataGrdViewProp.Rows[r].Cells[3].Value = dstranform.Tables[0].Rows[r].ItemArray[3];

                }
               
            }
            catch (Exception ex)
            {

            }
            //}
        }
        private void btWebAdd_Click(object sender, EventArgs e)
        {
            try
            {
                _log.Info("calling btWebAdd_Click");
                frmWebChange webchange = new frmWebChange();
                if (txtpluginId.Text != "")
                {
                    webchange.txtWebChangeId.Text = txtPropId_webchange.Text;
                    webchange.txtPluginid.Text = txtpluginId.Text;
                    jobscd = new JobConfigWinService.JobConfigPlugin();

                    if (lblWeb.Text != string.Empty)
                    {
                        jobscd.Jobname = lblWeb.Text;
                        DataSet ds = jobref.GetJobUpdatewitName(jobscd);
                        if (ds.Tables[0].Rows.Count > 0)
                        {

                            webchange.txtJobName.Text = ds.Tables[0].Rows[0].ItemArray[1].ToString();
                            webchange.txtFileFilter.Text = ds.Tables[0].Rows[0].ItemArray[3].ToString();
                            webchange.txtDate.Text = ds.Tables[0].Rows[0].ItemArray[2].ToString();
                            webchange.txtJobUpdateId.Text = ds.Tables[0].Rows[0].ItemArray[0].ToString();

                            webchange.btUpdate.Visible = true;
                        }

                    }
                    else
                    {
                        webchange.btSave.Visible = true;
                    }
                    webchange.ShowDialog();
                    lblWeb.Text = webchange.txtJobName.Text;
                    refreshDataGrid();
                }
                else
                {
                    
                    MessageBox.Show("Please save Plugin first  ", "Plugin", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show("An error occurred ." + ex.Message, "Usercontrol", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);
            }
        }

        private void btBinaryAdd_Click(object sender, EventArgs e)
        {
            try
            {
                _log.Info("calling btBinaryAdd_Click");
                jobscd = new JobConfigWinService.JobConfigPlugin();
                frmBinary frmbin = new frmBinary();
                if (txtpluginId.Text != "")
                {
                    frmbin.txtPluginid.Text = txtpluginId.Text;
                    frmbin.txtBinaryId.Text = txtPropId_binary.Text;
                    if (lblBinary.Text != string.Empty)
                    {
                        jobscd.Jobname = lblBinary.Text;
                        DataSet ds = jobref.GetJobUpdatewitName(jobscd);
                        if (ds.Tables[0].Rows.Count > 0)
                        {

                            frmbin.txtJobName.Text = ds.Tables[0].Rows[0].ItemArray[1].ToString();
                            frmbin.txtDate.Text = ds.Tables[0].Rows[0].ItemArray[2].ToString();
                            frmbin.txtJobUpdateId.Text = ds.Tables[0].Rows[0].ItemArray[0].ToString();
                            frmbin.btUpdate.Visible = true;
                        }

                    }
                    else
                    {
                        frmbin.btSave.Visible = true;
                    }
                    frmbin.ShowDialog();
                    lblBinary.Text = frmbin.txtJobName.Text;
                    refreshDataGrid();
                }
                else
                {
                    MessageBox.Show("Please save Plugin first  ", "Plugin", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred ." + ex.Message, "Usercontrol", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);
            }
            
            
           
        }

        private void UserControlCapture_Load(object sender, EventArgs e)
        {
            //string[] strtab = Program.tabselectedvalue.Split('-');
            //if (strtab[0].ToString() == "Capture")
            //{
            //    RefreshCaptureTab();
            //}
            //if (strtab[0].ToString() == "Transform")
            //{

            //    RefreshTransformTab();
            //}
            //if (strtab[0].ToString() == "Load")
            //{
            //    RefreshLoadTab();
            //}
        }
        public void RefreshCaptureTab()
        {
            try
            {
                jobscd = new JobConfigWinService.JobConfigPlugin();
                jobscd.JobId = Program.globaljobid;
                DataSet ds = jobref.GetJobPluginConfigWithJobId(jobscd);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                    {
                        if (ds.Tables[0].Rows[i].ItemArray[3].ToString() == "Capture")
                        {
                            int PluginId = Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[0].ToString());
                            jobscd.PluginId = PluginId;
                            this.Dock = System.Windows.Forms.DockStyle.Fill;
                            txtpluginId.Text = ds.Tables[0].Rows[i].ItemArray[0].ToString();
                            txtPulgin.Text = ds.Tables[0].Rows[i].ItemArray[4].ToString();
                            txtSeqNo.Text = ds.Tables[0].Rows[i].ItemArray[2].ToString();

                            DataSet dscapture = jobref.GetJobPluginPropWitPluginId(jobscd);

                            int row = dscapture.Tables[0].Rows.Count - 1;
                            if (dscapture.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k <= dscapture.Tables[0].Rows.Count - 1; k++)
                                {
                                    if (dataGrdViewProp.Rows.Count < dscapture.Tables[0].Rows.Count)
                                    {

                                        dataGrdViewProp.Rows.Add();
                                    }
                                }
                            }
                            for (int r = 0; r <= row; r++)
                            {

                                dataGrdViewProp.Rows[r].Cells[0].Value = dscapture.Tables[0].Rows[r].ItemArray[0];
                                dataGrdViewProp.Rows[r].Cells[1].Value = dscapture.Tables[0].Rows[r].ItemArray[1];
                                dataGrdViewProp.Rows[r].Cells[2].Value = dscapture.Tables[0].Rows[r].ItemArray[2];
                                dataGrdViewProp.Rows[r].Cells[3].Value = dscapture.Tables[0].Rows[r].ItemArray[3];

                            }
                            
                                for (int j = 0; j <= dscapture.Tables[0].Rows.Count - 1; j++)
                                {
                                    if (dscapture.Tables[0].Rows[j].ItemArray[2].ToString().ToUpper() == "XSLT")
                                    {
                                        lblConfig.Text = dscapture.Tables[0].Rows[j].ItemArray[3].ToString();
                                        txtPropId_config.Text = dscapture.Tables[0].Rows[j].ItemArray[0].ToString();
                                    }
                                    if (dscapture.Tables[0].Rows[j].ItemArray[2].ToString().ToUpper() == "WEBCHANGE")
                                    {
                                        lblWeb.Text = dscapture.Tables[0].Rows[j].ItemArray[3].ToString();
                                        txtPropId_webchange.Text = dscapture.Tables[0].Rows[j].ItemArray[0].ToString();

                                    }
                                    if (dscapture.Tables[0].Rows[j].ItemArray[2].ToString().ToUpper() == "BINARY")
                                    {
                                        lblBinary.Text = dscapture.Tables[0].Rows[j].ItemArray[3].ToString();
                                        txtPropId_binary.Text = dscapture.Tables[0].Rows[j].ItemArray[0].ToString();
                                    }
                                }
                            
                        }

                        
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred while trying to retrieve." + ex.Message, "Properties", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);
            }
        }
        public void RefreshTransformTab()
        {
            try
            {
                jobscd = new JobConfigWinService.JobConfigPlugin();
                jobscd.JobId = Program.globaljobid;
                DataSet ds = jobref.GetJobPluginConfigWithJobId(jobscd);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                    {
                        if (ds.Tables[0].Rows[i].ItemArray[3].ToString() == "Transform")
                        {

                            int PluginId = Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[0].ToString());
                            jobscd.PluginId = PluginId;
                            this.Dock = System.Windows.Forms.DockStyle.Fill;
                            txtpluginId.Text = ds.Tables[0].Rows[i].ItemArray[0].ToString();
                            txtPulgin.Text = ds.Tables[0].Rows[i].ItemArray[4].ToString();
                            txtSeqNo.Text = ds.Tables[0].Rows[i].ItemArray[2].ToString();

                            DataSet dstranform = jobref.GetJobPluginPropWitPluginId(jobscd);
                            int row = dstranform.Tables[0].Rows.Count - 1;

                            if (dstranform.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k <= dstranform.Tables[0].Rows.Count - 1; k++)
                                {
                                    if (dataGrdViewProp.Rows.Count < dstranform.Tables[0].Rows.Count)
                                    {

                                        dataGrdViewProp.Rows.Add();
                                    }
                                }
                            }
                            for (int r = 0; r <= row; r++)
                            {
                                //ctrltransform.dataGrdViewProp.Rows.Add();
                                dataGrdViewProp.Rows[r].Cells[0].Value = dstranform.Tables[0].Rows[r].ItemArray[0];
                                dataGrdViewProp.Rows[r].Cells[1].Value = dstranform.Tables[0].Rows[r].ItemArray[1];
                                dataGrdViewProp.Rows[r].Cells[2].Value = dstranform.Tables[0].Rows[r].ItemArray[2];
                                dataGrdViewProp.Rows[r].Cells[3].Value = dstranform.Tables[0].Rows[r].ItemArray[3];

                            }
                            //end
                            lblWeb.Visible = false;
                            btWebAdd.Visible = false;
                            btWebRemove.Visible = false;
                            labelWEB.Visible = false;

                            for (int j = 0; j <= dstranform.Tables[0].Rows.Count - 1; j++)
                            {
                                if (dstranform.Tables[0].Rows[j].ItemArray[2].ToString().ToUpper() == "BINARY")
                                {
                                    lblBinary.Text = dstranform.Tables[0].Rows[j].ItemArray[3].ToString();
                                    txtPropId_binary.Text = dstranform.Tables[0].Rows[j].ItemArray[0].ToString();
                                }
                                if (dstranform.Tables[0].Rows[j].ItemArray[2].ToString().ToUpper() == "XSLT")
                                {
                                    lblConfig.Text = dstranform.Tables[0].Rows[j].ItemArray[3].ToString();
                                    txtPropId_config.Text = dstranform.Tables[0].Rows[j].ItemArray[0].ToString();
                                }

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred while trying to Load Tabs." + ex.Message, "Properties", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);
            }
        }
        public void RefreshLoadTab()
        {
            try
            {
                jobscd = new JobConfigWinService.JobConfigPlugin();
                jobscd.JobId = Program.globaljobid;
                DataSet ds = jobref.GetJobPluginConfigWithJobId(jobscd);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                    {
                        if (ds.Tables[0].Rows[i].ItemArray[3].ToString() == "Load")
                        {

                            int PluginId = Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[0].ToString());
                            jobscd.PluginId = PluginId;
                            this.Dock = System.Windows.Forms.DockStyle.Fill;
                            txtpluginId.Text = ds.Tables[0].Rows[i].ItemArray[0].ToString();
                            txtPulgin.Text = ds.Tables[0].Rows[i].ItemArray[4].ToString();
                            txtSeqNo.Text = ds.Tables[0].Rows[i].ItemArray[2].ToString();

                            DataSet dsload = jobref.GetJobPluginPropWitPluginId(jobscd);
                            int row = dsload.Tables[0].Rows.Count - 1;
                            if (dsload.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k <= dsload.Tables[0].Rows.Count - 1; k++)
                                {
                                    if (dataGrdViewProp.Rows.Count < dsload.Tables[0].Rows.Count)
                                    {

                                        dataGrdViewProp.Rows.Add();
                                    }
                                }
                            }

                            for (int r = 0; r <= row; r++)
                            {
                                //ctrlLoad.dataGrdViewProp.Rows.Add();
                                dataGrdViewProp.Rows[r].Cells[0].Value = dsload.Tables[0].Rows[r].ItemArray[0];
                                dataGrdViewProp.Rows[r].Cells[1].Value = dsload.Tables[0].Rows[r].ItemArray[1];
                                dataGrdViewProp.Rows[r].Cells[2].Value = dsload.Tables[0].Rows[r].ItemArray[2];
                                dataGrdViewProp.Rows[r].Cells[3].Value = dsload.Tables[0].Rows[r].ItemArray[3];

                            }
                            //end
                            //ctrlLoad.lblConfig.Visible = false;
                            //ctrlLoad.btConfigAdd.Visible = false;
                            //ctrlLoad.btConfigremove.Visible = false;
                            lblWeb.Visible = false;
                            btWebAdd.Visible = false;
                            btWebRemove.Visible = false;
                            lblBinary.Visible = false;
                            btBinaryAdd.Visible = false;
                            btBinaryRemove.Visible = false;
                            labelWEB.Visible = false;
                            //ctrlLoad.labelCONFIG.Visible = false;
                            labelBIN.Visible = false;

                            for (int j = 0; j <= dsload.Tables[0].Rows.Count - 1; j++)
                            {
                                if (dsload.Tables[0].Rows[j].ItemArray[2].ToString().ToUpper() == "XSLT")
                                {
                                    lblConfig.Text = dsload.Tables[0].Rows[j].ItemArray[3].ToString();
                                    txtPropId_config.Text = dsload.Tables[0].Rows[j].ItemArray[0].ToString();
                                }

                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred while trying to Load Tabs." + ex.Message, "Properties", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);
            }
        }

        private void dataGrdViewProp_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //if (dataGrdViewProp.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().ToUpper() == "BINARY" || dataGrdViewProp.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().ToUpper() == "WEBCHANGE" || dataGrdViewProp.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().ToUpper() == "XSLT")
            //{

            //    dataGrdViewProp.Rows[e.RowIndex].ReadOnly = true;

            //}
 

        }

        private void dataGrdViewProp_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (dataGrdViewProp.Rows[e.RowIndex].Cells[e.ColumnIndex].Value!=null)
            {
            if (dataGrdViewProp.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().ToUpper() == "BINARY" || dataGrdViewProp.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().ToUpper() == "WEBCHANGE" || dataGrdViewProp.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().ToUpper() == "XSLT")
            {

                dataGrdViewProp.Rows[e.RowIndex].ReadOnly = true;

            }
            }
        }

        private void btDelete_Click(object sender, EventArgs e)
        {
            try
            {
                _log.Info("calling btDelete_Click");
                if(dataGrdViewProp.Rows.Count>0)
                {
                    if (dataGrdViewProp.SelectedRows.Count > 0)
                    {
                        DialogResult dialogResult = MessageBox.Show("Are you sure, you want to delete ?", "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (dialogResult == DialogResult.Yes)
                        {
                            foreach (DataGridViewRow item in this.dataGrdViewProp.SelectedRows)
                            {
                                //dataGrdViewProp.Rows.RemoveAt(item.Index);
                                jobscd.PropId = Convert.ToInt32(dataGrdViewProp.Rows[item.Index].Cells[0].Value.ToString());
                                jobscd.Jobname = dataGrdViewProp.Rows[item.Index].Cells[3].Value.ToString();
                                int iresultprop = jobref.DeleteJobPluginProp(jobscd);
                                if (iresultprop == 1)
                                {
                                    int iresultupdate = jobref.DeleteJobUpdate(jobscd);
                                    if (iresultupdate == 1)
                                    {
                                        if (dataGrdViewProp.Rows[item.Index].Cells[2].Value.ToString() == "Binary")
                                        {
                                            lblBinary.Text = "";
                                            txtPropId_binary.Text = "";
                                        }

                                        if (dataGrdViewProp.Rows[item.Index].Cells[2].Value.ToString() == "WebChange")
                                        {
                                            lblWeb.Text = "";
                                            txtPropId_webchange.Text = "";
                                            
                                        }
                                        if (dataGrdViewProp.Rows[item.Index].Cells[2].Value.ToString() == "DataFilter")
                                        {
                                            lblDataFilter.Text = "";
                                            txtprop_datafilterId.Text = "";

                                        }
                                    }
                                    if (dataGrdViewProp.Rows[item.Index].Cells[2].Value.ToString().ToUpper() == "XSLT" || dataGrdViewProp.Rows[item.Index].Cells[2].Value.ToString().ToUpper() == "CODENAME" || dataGrdViewProp.Rows[item.Index].Cells[2].Value.ToString().ToUpper() == "QUERY")
                                    {
                                        lblConfig.Text = "";
                                        txtPropId_config.Text="";
                                    }

                                    MessageBox.Show("Record deleted successfully!", "Properties", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                    refreshDataGrid();
                                }
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("Please select entire row to deleted !", "Properties", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                }
            }
            catch (Exception ex)
            {

            }
        }

        private void txtSeqNo_Leave(object sender, EventArgs e)
        {
            //try
            //{
            //    var fm = frmJobSchedule.MainFormRef;
            //    string tabtext = Program.tabselectedvalue;
            //    string[] type = tabtext.Split('-');
            //    string enginetype = type[0].ToString();
            //    int seqno = Convert.ToInt32(txtSeqNo.Text);//Convert.ToInt32(type[1]);
                
            //    for (int i = 0; i <= dataGrdViewProp.Rows.Count - 1; i++)
            //    {                    
            //        fm.datagridconfighidden.Rows.Add(txtPulgin.Text, seqno, enginetype, dataGrdViewProp.Rows[i].Cells[2].Value, dataGrdViewProp.Rows[i].Cells[3].Value);

            //    }
            //}
            //catch (Exception ex)
            //{

            //}

        }

        private void dataGrdViewProp_Leave(object sender, EventArgs e)
        {
            //try
            //{
            //    var fm = frmJobSchedule.MainFormRef;
            //    string tabtext = Program.tabselectedvalue;
            //    string[] type = tabtext.Split('-');
            //    string enginetype = type[0].ToString();
            //    int seqno = Convert.ToInt32(txtSeqNo.Text);//Convert.ToInt32(type[1]);

            //    for (int i = 0; i <= dataGrdViewProp.Rows.Count - 1; i++)
            //    {
            //        fm.datagridconfighidden.Rows.Add(txtPulgin.Text, seqno, enginetype, dataGrdViewProp.Rows[i].Cells[2].Value, dataGrdViewProp.Rows[i].Cells[3].Value);

            //    }
            //}
            //catch (Exception ex)
            //{

            //}
        }

   

   

        private void dataGrdViewProp_RowValidated(object sender, DataGridViewCellEventArgs e)
        {
            //try
            //{
            //    var fm = frmJobSchedule.MainFormRef;
            //    string tabtext = Program.tabselectedvalue;
            //    string[] type = tabtext.Split('-');
            //    string enginetype = type[0].ToString();
            //    int seqno = Convert.ToInt32(txtSeqNo.Text);//Convert.ToInt32(type[1]);
                
            //        for (int i = 0; i <= fm.datagridconfighidden.Rows.Count - 2; i++)
            //        {
            //            if (enginetype == "Capture")
            //            {
            //                if (fm.datagridconfighidden.Rows[i].Cells[2].Value.ToString() == "Capture" && fm.datagridconfighidden.Rows[i].Cells[1].Value == txtSeqNo.Text)
            //                {
            //                    fm.datagridconfighidden.Rows.RemoveAt(fm.datagridconfighidden.Rows[i].Index);
            //                }
            //            }
            //            if (enginetype == "Transform")
            //            {
            //                if (fm.datagridconfighidden.Rows[i].Cells[2].Value.ToString() == "Transform" && fm.datagridconfighidden.Rows[i].Cells[1].Value == txtSeqNo.Text)
            //                {
            //                    fm.datagridconfighidden.Rows.RemoveAt(fm.datagridconfighidden.Rows[i].Index);
            //                }
            //            }
            //            if (enginetype == "Load")
            //            {
            //                if (fm.datagridconfighidden.Rows[i].Cells[2].Value.ToString() == "Load" && fm.datagridconfighidden.Rows[i].Cells[1].Value == txtSeqNo.Text)
            //                {
            //                    fm.datagridconfighidden.Rows.RemoveAt(fm.datagridconfighidden.Rows[i].Index);
            //                }
            //            }
            //        }

            //        for (int i = 0; i <= dataGrdViewProp.Rows.Count - 2; i++)
            //        {
            //            fm.datagridconfighidden.Rows.Add(txtPulgin.Text, seqno, enginetype, dataGrdViewProp.Rows[i].Cells[2].Value, dataGrdViewProp.Rows[i].Cells[3].Value);

            //        }
                
            //}
            //catch (Exception ex)
            //{

            //}
        }

        private void btKeep_Click(object sender, EventArgs e)
        {
            try
            {
                _log.Info("calling btKeep_Click");
                var fm = frmJobSchedule.MainFormRef;
                string tabtext = Program.tabselectedvalue;
                string[] type = tabtext.Split('-');
                string enginetype = type[0].ToString();
                int seqno = Convert.ToInt32(txtSeqNo.Text);//Convert.ToInt32(type[1]);
                
                if (fm.datagridconfighidden.Rows.Count > 0)
                {
                    for (int i = fm.datagridconfighidden.Rows.Count - 1; i >= 0; i--)
                    {
                       

                        if (enginetype == "Capture")
                        {
                            if (fm.datagridconfighidden.Rows[i].Cells[2].Value.ToString() == "Capture" && fm.datagridconfighidden.Rows[i].Cells[1].Value.ToString() == txtSeqNo.Text)
                            {
                                fm.datagridconfighidden.Rows.RemoveAt(i);
                                
                            }
                        }
                        if (enginetype == "Transform")
                        {
                            if (fm.datagridconfighidden.Rows[i].Cells[2].Value.ToString() == "Transform" && fm.datagridconfighidden.Rows[i].Cells[1].Value.ToString() == txtSeqNo.Text)
                            {
                                fm.datagridconfighidden.Rows.RemoveAt(i);
                            }
                        }
                        if (enginetype == "Load")
                        {
                            if (fm.datagridconfighidden.Rows[i].Cells[2].Value.ToString() == "Load" && fm.datagridconfighidden.Rows[i].Cells[1].Value.ToString() == txtSeqNo.Text)
                            {
                                fm.datagridconfighidden.Rows.RemoveAt(i);
                            }
                        }
                    }
                }
                for (int i = 0; i <= dataGrdViewProp.Rows.Count - 2; i++)
                {
                    fm.datagridconfighidden.Rows.Add(txtPulgin.Text, seqno, enginetype, dataGrdViewProp.Rows[i].Cells[2].Value, dataGrdViewProp.Rows[i].Cells[3].Value, txtpluginId.Text, dataGrdViewProp.Rows[i].Cells[0].Value == null ? "" : dataGrdViewProp.Rows[i].Cells[0].Value);

                }
                if (enginetype != "" && seqno != null)
                {
                    fm.datagridconfighidden.Rows.Add(txtPulgin.Text, seqno, enginetype, "", "", txtpluginId.Text, "");
                }
            }
                
            catch (Exception ex)
            {

            }
        }

        private void btDataFilter_Click(object sender, EventArgs e)
        {
            try
            {
                _log.Info("calling btWebAdd_Click");
                frmDataFilter datafilter = new frmDataFilter();
                if (txtpluginId.Text != "")
                {
                    datafilter.txtDataFilterId.Text = txtprop_datafilterId.Text;
                    datafilter.txtPluginid.Text = txtpluginId.Text;
                    jobscd = new JobConfigWinService.JobConfigPlugin();

                    if (lblDataFilter.Text != string.Empty)
                    {
                        jobscd.Jobname = lblDataFilter.Text;
                        DataSet ds = jobref.GetJobUpdatewitName(jobscd);
                        if (ds.Tables[0].Rows.Count > 0)
                        {

                            datafilter.txtJobName.Text = ds.Tables[0].Rows[0].ItemArray[1].ToString();
                            datafilter.txtDescription.Text = ds.Tables[0].Rows[0].ItemArray[3].ToString();
                            datafilter.txtDate.Text = ds.Tables[0].Rows[0].ItemArray[2].ToString();
                            datafilter.txtJobUpdateId.Text = ds.Tables[0].Rows[0].ItemArray[0].ToString();

                            datafilter.btUpdate.Visible = true;
                        }

                    }
                    else
                    {
                        datafilter.btSave.Visible = true;
                    }
                    datafilter.ShowDialog();
                    lblDataFilter.Text = datafilter.txtJobName.Text;
                    refreshDataGrid();
                }
                else
                {

                    MessageBox.Show("Please save Plugin first  ", "Plugin", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                }
            }

            catch (Exception ex)
            {
                MessageBox.Show("An error occurred ." + ex.Message, "Usercontrol", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);
            }
        }
    }
}
