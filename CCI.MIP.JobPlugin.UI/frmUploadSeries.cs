﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Configuration;

namespace CCI.MIP.JobPlugin.UI
{
    public partial class frmUploadSeries : Form
    {
        private static readonly log4net.ILog _log = log4net.LogManager.GetLogger(typeof(frmUploadSeries));
    
        JobConfigWinService.JobConfigServiceClient jobref = new JobConfigWinService.JobConfigServiceClient();
        JobConfigWinService.JobConfigPlugin jobscd = new JobConfigWinService.JobConfigPlugin();

        public frmUploadSeries()
        {
            InitializeComponent();
        }

        private void btOpenCsv_Click(object sender, EventArgs e)
        {
            try
            {
                _log.Info("calling btOpenCsv_Click");
                // string targetPath = @"C:\mnt\marketintel\UploadMetadata\";  
                //string targetPath = @"\\stmnetapp01\MarketIntelD02\UploadMetadata\";
                string targetPath = ConfigurationManager.AppSettings["FilePathUploadSeries"].ToString();
                
                openCSVFileDialog.Filter = "csv Files (.csv)|*.csv|All Files (*.*)|*.*";
                openCSVFileDialog.FilterIndex = 1;
                openCSVFileDialog.Multiselect = true;
                DialogResult result = openCSVFileDialog.ShowDialog(); // Show the dialog.
                
                if (result == DialogResult.OK) // Test result.
                {
                    foreach (String file in openCSVFileDialog.FileNames)
                    {
                        txtPath.Text = file;
                        string sourceFile = txtPath.Text;
                        
                        var sections = sourceFile.Split('\\');
                        var fileName = sections[sections.Length - 1];
                        string destFile = System.IO.Path.Combine(targetPath, fileName);

                        if (!System.IO.Directory.Exists(targetPath))
                        {
                            System.IO.Directory.CreateDirectory(targetPath);
                        }

                        // To copy a file to another location and  
                        // overwrite the destination file if it already exists.
                        System.IO.File.Copy(sourceFile, destFile, true);
                    }
                    lblResult.Text = "File is uploaded to " +  targetPath;
                    jobscd.Jobname = "UploadNewSeries";
                    int iresult = jobref.InsertJobStatus(jobscd);

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred while opening the file." + ex.Message, "CSV Creation", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);
            }
        }

   
    }
}
