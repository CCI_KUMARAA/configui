﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraEditors;


namespace CCI.MIP.JobPlugin.UI
{
    public partial class frmJobSchedule : Form
    {
        private static readonly log4net.ILog _log = log4net.LogManager.GetLogger(typeof(frmJobSchedule));
        JobConfigWinService.JobConfigServiceClient jobref = new JobConfigWinService.JobConfigServiceClient();
        JobConfigWinService.JobConfigPlugin jobscd = new JobConfigWinService.JobConfigPlugin();
       
        public int capt_PluginId = 0, capt_PropConfigId = 0, capt_PropBinId = 0, capt_PropWebId = 0;
        public int trans_Pluginid = 0, trans_PropConfigId = 0, trans_PropBinId = 0;
        public int load_PropConfigId = 0, load_pluginId = 0;
        public bool flagschedule;
        public bool flagactive;

        public static frmJobSchedule MainFormRef { get; private set; }


        public bool flag = false;

        public frmJobSchedule()
        {
            InitializeComponent();
            MainFormRef = this;
            
        }
        public frmJobSchedule(string jobname, JobConfigWinService.JobConfigPlugin objschd)
        {
            try
            {
                _log.Info("calling frmJobSchedule");
                MainFormRef = this;
            jobscd = objschd;
            InitializeComponent();
            lbljobId.Text = "JOB_ID:-" + jobscd.JobId;
            txtJobName.Text = jobname;
            txtDesk.Text = jobscd.Desk;
            cmbCalendar.ValueMember = jobscd.Calendar.ToString();
            cmbCalendar.Text = jobscd.Calendar.ToString();
            numFrequency.Value = jobscd.Frequency;
            //tpStartTime.Value = Convert.ToDateTime(jobscd.StartTime);
            //tpEndDate.Value = Convert.ToDateTime(jobscd.EndTime);
            string end = Convert.ToString(jobscd.EndTime.ToString("hh:mm:ss tt"));
            string start = Convert.ToString(jobscd.StartTime.ToString("hh:mm:ss tt"));
                            
            numDuration.Value = jobscd.Duration;
           
            //tpStartTime.Value = Convert.ToDateTime(jobscd.LastUpdateDate);
            tmEditEndTime.EditValue = end;
            tmEditStartTime.EditValue = start;
            btUpdate.Visible = true;
            if (jobscd.Active == "YES")
            {
                flagactive = true;
                rdbYes.Checked = true;
                
            }
          
            else
            {
                flagactive = true;
                rdbNo.Checked = true;
               
            }
            flagactive = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred while trying to retrieve." + ex.Message, "Job Schedule", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);
            }
        }
        private void frmJobSchedule_Load(object sender, EventArgs e)
        {
            Loadcontrols();
            if (btSave.Visible == true)
            {
                cmbCalendar.SelectedIndex = 1;
            }
            FillMailingGrid();

            LoadComboMailGroup(Program.globaljobid);

            FillComboMailGroupStatus();
        }
        public void LoadMailingGrid()
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            try
            {
                _log.Info("calling LoadMailingGrid");
                JobConfigWinService.JobConfigServiceClient jobs = new JobConfigWinService.JobConfigServiceClient();
                jobscd = new JobConfigWinService.JobConfigPlugin();
                jobscd.JobId = Program.globaljobid;

                ds.Tables.Clear();
                //ds = jobs.GetJobMaster();
                ds = jobs.GetMailingNameWithStatus(jobscd);
                //USER_MAIL_ID.FieldName = ds.Tables[0].Columns[0].ToString();
                //JOB_NAME.FieldName = ds.Tables[0].Columns[1].ToString();
                //CALENDAR.FieldName = ds.Tables[0].Columns[2].ToString();
                //START_TIME.FieldName = ds.Tables[0].Columns[3].ToString();
                //END_TIME.FieldName = ds.Tables[0].Columns[4].ToString();
                //DURATION.FieldName = ds.Tables[0].Columns[5].ToString();
                //FREQUENCY.FieldName = ds.Tables[0].Columns[6].ToString();
                //ACTIVE.FieldName = ds.Tables[0].Columns[7].ToString();
                //LAST_UPDATE_DATE.FieldName = ds.Tables[0].Columns[8].ToString();
                gcMailing.DataSource = ds.Tables[0];
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred while trying to retrieve." +ex.Message, "Mailing", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);
            }
        }
        public void Loadcontrols()
        {
            try
            {
                _log.Info("calling btCopy_Click");
                DataSet ds = jobref.GetCalendar();
                for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                {
                    cmbCalendar.Items.Add(ds.Tables[0].Rows[i].ItemArray[0].ToString());
                    //cmbCalendar.DisplayMember = ds.Tables[0].Rows[0].ItemArray[i].ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred while trying to Load controls." + ex.Message, "Load", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);
            }
        }
        private void btSave_Click(object sender, EventArgs e)
        {
            try
            {
                _log.Info("calling btSave_Click");
                //string strdatgrid=datagridconfighidden.Rows[0].Cells[0].Value.ToString();
                int insertJobSchedule;             

                string jobname = txtJobName.Text.ToString();
                Program.JobName = jobname;
                jobscd.Description = "";
                jobscd.Desk = txtDesk.Text;
                jobscd.Jobname = jobname;
                jobscd.Calendar = Convert.ToString(cmbCalendar.SelectedItem);
                jobscd.Frequency = Convert.ToInt32(numFrequency.Value);
                jobscd.StartTime = Convert.ToDateTime(tmEditStartTime.Text);
                jobscd.EndTime = Convert.ToDateTime(tmEditEndTime.Text);
                jobscd.Duration = Convert.ToInt32(numDuration.Value);
                jobscd.LastUpdateDate = System.DateTime.Now;


                if (rdbNo.Checked == true)
                {
                    jobscd.Active = "NO";
                }
                else if (rdbYes.Checked == true)
                {
                    jobscd.Active = "YES";
                }
                int insertJobMaster = jobref.InsertJobMaster(jobscd);
                if ((insertJobMaster == -1) && ((jobname == null) || (jobname == "")))
                {
                    MessageBox.Show("Please enter a name JobName", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else if ((insertJobMaster == -1) && ((jobname != null) || (jobname != "")))
                {
                    MessageBox.Show("Record with same JobName Exists", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else if (insertJobMaster == 1)
                {
                    int jobid = jobref.GetJobId(jobscd);
                    jobscd.JobId = jobid;
                    Program.globaljobid = jobid;
                    lbljobId.Text = "JOB_ID :- " + Convert.ToString(jobid);
                    insertJobSchedule = jobref.InsertJobSchedule(jobscd);
                    if ((insertJobSchedule == 1) && (insertJobMaster == 1))
                    {

                        if (datagridconfighidden.Rows.Count > 0)
                        {
                            for (int i = 0; i <= datagridconfighidden.Rows.Count - 1; i++)
                            {
                                jobscd.JobId = Program.globaljobid;
                                jobscd.Plugin = datagridconfighidden.Rows[i].Cells[0].Value.ToString();
                                jobscd.Seqnum = Convert.ToInt32(datagridconfighidden.Rows[i].Cells[1].Value);
                                jobscd.EngineType = datagridconfighidden.Rows[i].Cells[2].Value.ToString();
                                int iresultplugin = jobref.InsertJobPluginConfig(jobscd);

                                DataSet ds = jobref.GetJobPluginConfigWithPlugin(jobscd);

                                if (ds.Tables.Count > 0)
                                {
                                    if (ds.Tables[0].Rows.Count > 0)
                                    {
                                        int pluginconfigId = Convert.ToInt32(ds.Tables[0].Rows[0].ItemArray[0].ToString());
                                        jobscd.PluginConfigId = pluginconfigId;
                                        jobscd.PropName = datagridconfighidden.Rows[i].Cells[3].Value.ToString();
                                        jobscd.PropValue = datagridconfighidden.Rows[i].Cells[4].Value.ToString();
                                        int iresultprop = jobref.InsertJobPluginProp(jobscd);
                                    }
                                }
                            }
                        }
                        MessageBox.Show("Record inserted Succesfully", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        cleargrid();
                        tabJobsOptions.SelectedIndex = 0;
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Error Occoured while Saving the Record into JobSchedule !" , "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {

                    MessageBox.Show("Error Occoured while Saving the Record into JobMaster !", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred while trying to save.-" +ex.Message, "Log", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);
            }
        }
        private void btUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                _log.Info("calling btUpdate_Click");
                //string str = datagridconfighidden.Rows[0].Cells[0].Value.ToString();
                string starttime = tmEditStartTime.Text;
                string EndTime = tmEditEndTime.Text;
                string st = String.Format("{0:MM/dd/yyyy} {1}", System.DateTime.Now, starttime);
                string st1 = String.Format("{0:MM/dd/yyyy} {1}", System.DateTime.Now, EndTime);



                string jobname = txtJobName.Text.ToString();
                jobscd.Jobname = jobname;
                Program.JobName = jobname;
                jobscd.Description = "";
                jobscd.Desk = txtDesk.Text;
                jobscd.JobId = Program.globaljobid;
                //to update schedule
                if (cmbCalendar.SelectedItem == null)
                {
                    jobscd.Calendar = cmbCalendar.ValueMember.ToString();
                }
                else
                {
                    jobscd.Calendar = cmbCalendar.SelectedItem.ToString();
                }
                jobscd.Frequency = Convert.ToInt32(numFrequency.Value);              
                jobscd.StartTime = Convert.ToDateTime(st.ToString());
                jobscd.EndTime = Convert.ToDateTime(st1.ToString());
                jobscd.Duration = Convert.ToInt32(numDuration.Value);
                //jobscd.LastUpdateDate = System.DateTime.Now;
                if (rdbNo.Checked == true)
                {
                    jobscd.Active = "NO";
                }
                else if (rdbYes.Checked == true)
                {
                    jobscd.Active = "YES";
                }

                int iresultjobmaster = jobref.UpdateJobMaster(jobscd);
                int updateJobSchedule = 0;
                if (flagschedule == true)
                {
                    updateJobSchedule = jobref.UpdateJobSchedule(jobscd);
                }

                //if (updateJobSchedule == 1 && iresultjobmaster==1)
                //{
                    if (datagridconfighidden.Rows.Count > 0)
                    {
                        for (int i = 0; i <= datagridconfighidden.Rows.Count - 1; i++)
                        {
                            jobscd.JobId = Program.globaljobid;
                            jobscd.Plugin = datagridconfighidden.Rows[i].Cells[0].Value.ToString();
                            jobscd.Seqnum = Convert.ToInt32(datagridconfighidden.Rows[i].Cells[1].Value);
                            jobscd.EngineType = datagridconfighidden.Rows[i].Cells[2].Value.ToString();
                            string pluginidstr = datagridconfighidden.Rows[i].Cells[5].Value.ToString();
                            int iresultplugin;
                            if (pluginidstr != "")
                            {
                                jobscd.PluginConfigId = Convert.ToInt32(pluginidstr);
                                iresultplugin = jobref.UpdateJobPluginConfig(jobscd);
                            }
                            else
                            {
                                iresultplugin = jobref.InsertJobPluginConfig(jobscd);
                            }
                             DataSet ds = jobref.GetJobPluginConfigWithPlugin(jobscd);
                             if (ds.Tables.Count > 0)
                             {
                                 if (ds.Tables[0].Rows.Count > 0)
                                 {

                                     jobscd.PluginConfigId = Convert.ToInt32(ds.Tables[0].Rows[0].ItemArray[0].ToString());

                                     string strpropid = datagridconfighidden.Rows[i].Cells[6].Value.ToString();
                                     jobscd.PropName = datagridconfighidden.Rows[i].Cells[3].Value.ToString();
                                     jobscd.PropValue = datagridconfighidden.Rows[i].Cells[4].Value.ToString();
                                     if (strpropid != null && strpropid != "")
                                     {
                                         jobscd.PropId = Convert.ToInt32(strpropid);
                                         int iresultprop = jobref.UpdateJobPluginProp(jobscd);
                                     }
                                     else
                                     {
                                         int iresultprop = jobref.InsertJobPluginProp(jobscd);
                                     }

                                 }
                             }

                        }
                    }
                    MessageBox.Show("Record Updated Successfully!", "Job Schedule", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    cleargrid();
                    tabJobsOptions.SelectedIndex = 0;
                    this.Close();
                    
               // }
                //else
                //{
                //    MessageBox.Show("Error occurred while updating the Record .", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //}
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred while trying to update." + ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);
            }

            //this.Close();

            //frmJobMain frm1 = new frmJobMain();
            //frm1.Fillgrid();
            //frm1.Show();
            
        }
        public void cleargrid()
        {
                    for (int i = datagridconfighidden.Rows.Count - 1; i >= 0; i--)
                    {
                        datagridconfighidden.Rows.RemoveAt(i);
                    }
        }
        private void frmJobSchedule_FormClosing(object sender, FormClosingEventArgs e)
        {
            timerJobStatus.Stop();

        }

        //private void btnMailSearch_Click(object sender, EventArgs e)
        //{

        //    string mailgroupname = txtEditMailGroup.Text.ToString();


        //    DataSet ds = new DataSet();
        //    DataTable dt = new DataTable();
        //    try
        //    {
        //        if ((mailgroupname != null) && (mailgroupname != ""))
        //        {


        //            if ((mailgroupname != null) && (mailgroupname != ""))
        //            {
        //                JobConfigWinService.JobConfigServiceClient maillist = new JobConfigWinService.JobConfigServiceClient();



        //                ds.Tables.Clear();
        //                ds = maillist.GetMailingList(mailgroupname, Program.globaljobid);

        //                if (ds.Tables[0].Rows.Count < 1)
        //                {
        //                    MessageBox.Show("No Matches Found.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
        //                }
        //                MAIL_GROUP_NAME.FieldName = ds.Tables[0].Columns[1].ToString();


        //                USER_MAIL_ID.FieldName = ds.Tables[0].Columns[2].ToString();
        //                //JOB_NAME.FieldName = ds.Tables[0].Columns[1].ToString();

        //                gcMailing.DataSource = ds.Tables[0];
        //            }
        //            else
        //            {
        //                MessageBox.Show("No Matches Found.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
        //            }
        //        }
        //        else
        //        {
        //            MessageBox.Show("Please Enter a JobGroup Name.", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show("An error occurred while trying to retrieve.", "Log", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        _log.Error(ex.Message, ex);
        //    }
        //}
        private void button1_Click(object sender, EventArgs e)
        {
            //txtEditMailGroup.Text = "";
            //gcMailing.DataSource = null;

          
            
            using (  frmMailingScreen frmmailscreen = new frmMailingScreen())
            {
                
                frmmailscreen.ShowDialog();
            }
            FillMailingGrid();

            LoadComboMailGroup(Program.globaljobid);
        }
        private void btCreate_Click(object sender, EventArgs e)
        {
            LoadTabpages();
            tabControlPlugin_SelectedIndexChanged(null, null);
        }
        public void LoadTabpages()
        {
            try
            {
                _log.Info("calling LoadTabpages");
                if (numCapture.Value != 0)
                {
                    int i = Convert.ToInt32(numCapture.Value);


                    for (int k = 1; k <= i; k++)
                    {
                        TabPage tp = new TabPage("Capture");
                        string Title = "Capture- " + (tabControlPlugin.TabPages.Count + 1);
                        tp.Text = Title;

                        UserControlCapture ctrlcapture = new UserControlCapture();
                        ctrlcapture.Dock = System.Windows.Forms.DockStyle.Fill;
                        tp.Controls.Add(ctrlcapture);

                        tabControlPlugin.TabPages.Add(tp);
                    }
                }
                if (numTransform.Value != 0)
                {
                    int i = Convert.ToInt32(numTransform.Value);

                    for (int k = 1; k <= i; k++)
                    {
                        TabPage tp = new TabPage("Transform");
                        string Title = "Transform- " + (tabControlPlugin.TabPages.Count + 1);
                        tp.Text = Title;

                        UserControlCapture ctrlcapture = new UserControlCapture();
                        ctrlcapture.Dock = System.Windows.Forms.DockStyle.Fill;
                        ctrlcapture.lblWeb.Visible = false;
                        ctrlcapture.btWebAdd.Visible = false;
                        ctrlcapture.btWebRemove.Visible = false;
                        ctrlcapture.labelWEB.Visible = false;
                        tp.Controls.Add(ctrlcapture);
                        tabControlPlugin.TabPages.Add(tp);
                    }
                }
                if (numLoad.Value != 0)
                {
                    int i = Convert.ToInt32(numLoad.Value);

                    for (int k = 1; k <= i; k++)
                    {
                        TabPage tp = new TabPage("Load");
                        string Title = "Load- " + (tabControlPlugin.TabPages.Count + 1);
                        tp.Text = Title;

                        UserControlCapture ctrlcapture = new UserControlCapture();
                        ctrlcapture.Dock = System.Windows.Forms.DockStyle.Fill;

                        ctrlcapture.lblWeb.Visible = false;
                        ctrlcapture.btWebAdd.Visible = false;
                        ctrlcapture.btWebRemove.Visible = false;
                        ctrlcapture.lblBinary.Visible = false;
                        ctrlcapture.btBinaryAdd.Visible = false;
                        ctrlcapture.btBinaryRemove.Visible = false;

                        ctrlcapture.labelWEB.Visible = false;
                        ctrlcapture.labelBIN.Visible = false;
                        tp.Controls.Add(ctrlcapture);
                        tabControlPlugin.TabPages.Add(tp);
                    }
                }
                numCapture.Value = 0;
                numTransform.Value = 0;
                numLoad.Value = 0;
                tabControlPlugin.SelectedIndex = 0;
            }

            catch (Exception ex)
            {
                MessageBox.Show("An error occurred while trying to Load tabs." + ex.Message, "Config Plugin", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);
            }
        }
        private void btRemove_Click(object sender, EventArgs e)
        {
            tabControlPlugin.TabPages.Remove(tabControlPlugin.SelectedTab);
        }
        private void tabControlPlugin_Selected(object sender, TabControlEventArgs e)
        {
            string index = e.TabPage.Text;
        }
        private void tabControlPlugin_SelectedIndexChanged(object sender, EventArgs e)
        {
            Program.tabselectedvalue = tabControlPlugin.SelectedTab.Text;
            string[] strtab = Program.tabselectedvalue.Split('-');
            if (strtab[0].ToString() == "Capture")
            {
                RefreshCaptureTab();
            }
            if (strtab[0].ToString() == "Transform")
            {

                RefreshTransformTab();
            }
            if (strtab[0].ToString() == "Load")
            {
                RefreshLoadTab();
            }

        }

        public void LoadComboMailGroup(int jobid)
        {
            try
            {
                _log.Info("calling LoadComboMailGroup");
                jobscd.JobId = jobid;
                cmbMailGroupName.Items.Clear();
                DataSet ds = jobref.GetJobMailGroup(jobscd);
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                        {
                            cmbMailGroupName.Items.Add(ds.Tables[0].Rows[i].ItemArray[0].ToString());

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred while trying to Load." + ex.Message, "Mailing", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);
            }
        }

        public void FillMailingGrid()
        {
            try
            {
                _log.Info("calling FillMailingGrid");
                DataSet ds = new DataSet();
                JobConfigWinService.JobConfigServiceClient maillist = new JobConfigWinService.JobConfigServiceClient();
                jobscd = new JobConfigWinService.JobConfigPlugin();
                jobscd.JobId = Program.globaljobid;

                ds.Tables.Clear();
                //ds = maillist.GetMailingListWithJobId(jobscd);
                ds = maillist.GetMailingNameWithStatus(jobscd);
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {


                        MAIL_GROUP_NAME.FieldName = ds.Tables[0].Columns[0].ToString();

                        JOB_MAIL_STATUS.FieldName = ds.Tables[0].Columns[1].ToString();

                        //USER_MAIL_ID.FieldName = ds.Tables[0].Columns[2].ToString();


                        gcMailing.DataSource = ds.Tables[0];
                    }
                    else
                    {
                        gcMailing.DataSource = null;
                    }
                }
            }
            catch (Exception ex)
            {

            }
               
        }

        private void tabJobsOptions_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                _log.Info("calling tabJobsOptions_SelectedIndexChanged");
                if (tabJobsOptions.SelectedTab.Text == "Mailing")
                {

                    FillMailingGrid();

                    LoadComboMailGroup(Program.globaljobid);
                    timerJobStatus.Stop();
                }

                if (tabJobsOptions.SelectedTab.Text == "Job History")
                {
                    timerJobStatus.Start();
                    JobConfigWinService.JobConfigServiceClient jobstatus = new JobConfigWinService.JobConfigServiceClient();
                    jobscd = new JobConfigWinService.JobConfigPlugin();
                    jobscd.JobId = Program.globaljobid;
                    DataSet ds = jobstatus.GetJobStatus(jobscd);

                    STATUS_ID.FieldName = ds.Tables[0].Columns[0].ToString();
                    JOB_ID.FieldName = ds.Tables[0].Columns[1].ToString();
                    SCHEDULE_TIME.FieldName = ds.Tables[0].Columns[2].ToString();
                    START_TIME.FieldName = ds.Tables[0].Columns[3].ToString();
                    END_TIME.FieldName = ds.Tables[0].Columns[4].ToString();
                    CURRENT_STATUS.FieldName = ds.Tables[0].Columns[5].ToString();
                    grdcontrolHistory.DataSource = ds.Tables[0];

                    //datagridconfighidden.Rows.Add("a", "seqno", "enginetype", "dataGrdViewProp.Rows[i].Cells[2].Value", "dataGrdViewProp.Rows[i].Cells[3].Value");
                }
                if (tabJobsOptions.SelectedTab.Text == "Config Plugin")
                {
                    if (flag == false)
                    {
                        LoadPluginConfig();
                        flag = true;
                        if (tabControlPlugin.TabCount > 0)
                        {

                            Program.tabselectedvalue = tabControlPlugin.SelectedTab.Text;
                        }
                    }
                    timerJobStatus.Stop();

                }
                if (tabJobsOptions.SelectedTab.Text == "Trigger")
                {
                    
                    DataSet ds = jobref.GetJobMaster();
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        cmbTriggerJobName.DataSource = ds.Tables[0];
                        cmbTriggerJobName.ValueMember = ds.Tables[0].Columns[0].ColumnName.ToString();
                        cmbTriggerJobName.DisplayMember = ds.Tables[0].Columns[1].ColumnName.ToString();
                        
                    }
                    RefreshTriggerGrid();
                    //timerJobStatus_Tick.Stop();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred while trying to Load Tabs." + ex.Message, "Job History", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);
            }

        }
        public void RefreshTriggerGrid()
        {
            try
            {
                _log.Info("calling RefreshTriggerGrid");
                jobscd.JobId = Program.globaljobid;
                DataSet ds = jobref.GetTrigger(jobscd);
                if(ds.Tables.Count>0)
                {

                    TRIG_ID.FieldName = ds.Tables[0].Columns[0].ToString();
                    TRIGGER_JOB_ID.FieldName = ds.Tables[0].Columns[2].ToString();
                    STATUS.FieldName = ds.Tables[0].Columns[3].ToString();
                    TRIGGER_JOB_NAME.FieldName = ds.Tables[0].Columns[4].ToString();
                    gridTrigger.DataSource = ds.Tables[0];
                   
                }
                cmbStatus.Text = "";
                cmbTriggerJobName.SelectedIndex = 0;

            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred while trying to refresh Trigger grid." + ex.Message, "Job History", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);
            }
        }

        public void RefreshCaptureTab()
        {
            try
            {
                _log.Info("calling RefreshCaptureTab");
                jobscd = new JobConfigWinService.JobConfigPlugin();
                jobscd.JobId = Program.globaljobid;
                DataSet ds = jobref.GetJobPluginConfigWithJobId(jobscd);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                    {
                        if (ds.Tables[0].Rows[i].ItemArray[3].ToString() == "Capture")
                        {
                            int PluginId = Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[0].ToString());
                            jobscd.PluginId = PluginId;
                            UserControlCapture ctrlcapture = new UserControlCapture();
                            ctrlcapture.Dock = System.Windows.Forms.DockStyle.Fill;
                            ctrlcapture.txtpluginId.Text = ds.Tables[0].Rows[i].ItemArray[0].ToString();
                            ctrlcapture.txtPulgin.Text = ds.Tables[0].Rows[i].ItemArray[4].ToString();
                            ctrlcapture.txtSeqNo.Text = ds.Tables[0].Rows[i].ItemArray[2].ToString();

                            DataSet dscapture = jobref.GetJobPluginPropWitPluginId(jobscd);

                            int row = dscapture.Tables[0].Rows.Count - 1;
                            if (dscapture.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k <= dscapture.Tables[0].Rows.Count - 1; k++)
                                {
                                    if (ctrlcapture.dataGrdViewProp.Rows.Count < dscapture.Tables[0].Rows.Count)
                                    {

                                        ctrlcapture.dataGrdViewProp.Rows.Add();
                                    }
                                }
                            }
                            for (int r = 0; r <= row; r++)
                            {

                                ctrlcapture.dataGrdViewProp.Rows[r].Cells[0].Value = dscapture.Tables[0].Rows[r].ItemArray[0];
                                ctrlcapture.dataGrdViewProp.Rows[r].Cells[1].Value = dscapture.Tables[0].Rows[r].ItemArray[1];
                                ctrlcapture.dataGrdViewProp.Rows[r].Cells[2].Value = dscapture.Tables[0].Rows[r].ItemArray[2];
                                ctrlcapture.dataGrdViewProp.Rows[r].Cells[3].Value = dscapture.Tables[0].Rows[r].ItemArray[3];

                            }
                            ctrlcapture.lblDataFilter.Visible = false;
                            ctrlcapture.labelDataFilter.Visible = false;
                            ctrlcapture.btDataFilter.Visible = false;
                            for (int j = 0; j <= dscapture.Tables[0].Rows.Count - 1; j++)
                            {
                                if (dscapture.Tables[0].Rows[j].ItemArray[2].ToString().ToUpper() == "XSLT")
                                {
                                    ctrlcapture.lblConfig.Text = dscapture.Tables[0].Rows[j].ItemArray[3].ToString();
                                    ctrlcapture.txtPropId_config.Text = dscapture.Tables[0].Rows[j].ItemArray[0].ToString();
                                }
                                if (dscapture.Tables[0].Rows[j].ItemArray[2].ToString().ToUpper() == "WEBCHANGE")
                                {
                                    ctrlcapture.lblWeb.Text = dscapture.Tables[0].Rows[j].ItemArray[3].ToString();
                                    ctrlcapture.txtPropId_webchange.Text = dscapture.Tables[0].Rows[j].ItemArray[0].ToString();

                                }
                                if (dscapture.Tables[0].Rows[j].ItemArray[2].ToString().ToUpper() == "BINARY")
                                {
                                    ctrlcapture.lblBinary.Text = dscapture.Tables[0].Rows[j].ItemArray[3].ToString();
                                    ctrlcapture.txtPropId_binary.Text = dscapture.Tables[0].Rows[j].ItemArray[0].ToString();
                                }
                            }

                        }
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred while trying to retrieve." + ex.Message, "Job Schedule", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);
            }
        }
        public void RefreshTransformTab()
        {
            try
            {
                _log.Info("calling RefreshTransformTab");
                jobscd = new JobConfigWinService.JobConfigPlugin();
                jobscd.JobId = Program.globaljobid;
                DataSet ds = jobref.GetJobPluginConfigWithJobId(jobscd);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                    {
                        if (ds.Tables[0].Rows[i].ItemArray[3].ToString() == "Transform")
                        {

                            int PluginId = Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[0].ToString());
                            jobscd.PluginId = PluginId;
                            UserControlCapture ctrltransform = new UserControlCapture();
                            ctrltransform.Dock = System.Windows.Forms.DockStyle.Fill;
                            ctrltransform.txtpluginId.Text = ds.Tables[0].Rows[i].ItemArray[0].ToString();
                            ctrltransform.txtPulgin.Text = ds.Tables[0].Rows[i].ItemArray[4].ToString();
                            ctrltransform.txtSeqNo.Text = ds.Tables[0].Rows[i].ItemArray[2].ToString();

                            DataSet dstranform = jobref.GetJobPluginPropWitPluginId(jobscd);
                            int row = dstranform.Tables[0].Rows.Count - 1;

                            if (dstranform.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k <= dstranform.Tables[0].Rows.Count - 1; k++)
                                {
                                    if (ctrltransform.dataGrdViewProp.Rows.Count < dstranform.Tables[0].Rows.Count)
                                    {

                                        ctrltransform.dataGrdViewProp.Rows.Add();
                                    }
                                }
                            }
                            for (int r = 0; r <= row; r++)
                            {
                                //ctrltransform.dataGrdViewProp.Rows.Add();
                                ctrltransform.dataGrdViewProp.Rows[r].Cells[0].Value = dstranform.Tables[0].Rows[r].ItemArray[0];
                                ctrltransform.dataGrdViewProp.Rows[r].Cells[1].Value = dstranform.Tables[0].Rows[r].ItemArray[1];
                                ctrltransform.dataGrdViewProp.Rows[r].Cells[2].Value = dstranform.Tables[0].Rows[r].ItemArray[2];
                                ctrltransform.dataGrdViewProp.Rows[r].Cells[3].Value = dstranform.Tables[0].Rows[r].ItemArray[3];

                            }
                            //end
                            ctrltransform.lblWeb.Visible = false;
                            ctrltransform.btWebAdd.Visible = false;
                            ctrltransform.btWebRemove.Visible = false;
                            ctrltransform.labelWEB.Visible = false;

                            for (int j = 0; j <= dstranform.Tables[0].Rows.Count - 1; j++)
                            {
                                if (dstranform.Tables[0].Rows[j].ItemArray[2].ToString().ToUpper() == "BINARY")
                                {
                                    ctrltransform.lblBinary.Text = dstranform.Tables[0].Rows[j].ItemArray[3].ToString();
                                    ctrltransform.txtPropId_binary.Text = dstranform.Tables[0].Rows[j].ItemArray[0].ToString();
                                }
                                if (dstranform.Tables[0].Rows[j].ItemArray[2].ToString().ToUpper() == "XSLT")
                                {
                                    ctrltransform.lblConfig.Text = dstranform.Tables[0].Rows[j].ItemArray[3].ToString();
                                    ctrltransform.txtPropId_config.Text = dstranform.Tables[0].Rows[j].ItemArray[0].ToString();
                                }
                                if (dstranform.Tables[0].Rows[j].ItemArray[2].ToString().ToUpper() == "DATAFILTER")
                                {
                                    ctrltransform.lblDataFilter.Text = dstranform.Tables[0].Rows[j].ItemArray[3].ToString();
                                    ctrltransform.txtprop_datafilterId.Text = dstranform.Tables[0].Rows[j].ItemArray[0].ToString();


                                }

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred while trying to Load Tabs." + ex.Message, "Job History", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);
            }
        }
        public void RefreshLoadTab()
        {
            try
            {
                _log.Info("calling RefreshLoadTab");
                jobscd = new JobConfigWinService.JobConfigPlugin();
                jobscd.JobId = Program.globaljobid;
                DataSet ds = jobref.GetJobPluginConfigWithJobId(jobscd);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                    {
                        if (ds.Tables[0].Rows[i].ItemArray[3].ToString() == "Load")
                        {

                            int PluginId = Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[0].ToString());
                            jobscd.PluginId = PluginId;
                            UserControlCapture ctrlLoad = new UserControlCapture();
                            ctrlLoad.Dock = System.Windows.Forms.DockStyle.Fill;
                            ctrlLoad.txtpluginId.Text = ds.Tables[0].Rows[i].ItemArray[0].ToString();
                            ctrlLoad.txtPulgin.Text = ds.Tables[0].Rows[i].ItemArray[4].ToString();
                            ctrlLoad.txtSeqNo.Text = ds.Tables[0].Rows[i].ItemArray[2].ToString();

                            DataSet dsload = jobref.GetJobPluginPropWitPluginId(jobscd);
                            int row = dsload.Tables[0].Rows.Count - 1;
                            if (dsload.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k <= dsload.Tables[0].Rows.Count - 1; k++)
                                {
                                    if (ctrlLoad.dataGrdViewProp.Rows.Count < dsload.Tables[0].Rows.Count)
                                    {

                                        ctrlLoad.dataGrdViewProp.Rows.Add();
                                    }
                                }
                            }

                            for (int r = 0; r <= row; r++)
                            {
                                //ctrlLoad.dataGrdViewProp.Rows.Add();
                                ctrlLoad.dataGrdViewProp.Rows[r].Cells[0].Value = dsload.Tables[0].Rows[r].ItemArray[0];
                                ctrlLoad.dataGrdViewProp.Rows[r].Cells[1].Value = dsload.Tables[0].Rows[r].ItemArray[1];
                                ctrlLoad.dataGrdViewProp.Rows[r].Cells[2].Value = dsload.Tables[0].Rows[r].ItemArray[2];
                                ctrlLoad.dataGrdViewProp.Rows[r].Cells[3].Value = dsload.Tables[0].Rows[r].ItemArray[3];

                            }
                            //end
                            //ctrlLoad.lblConfig.Visible = false;
                            //ctrlLoad.btConfigAdd.Visible = false;
                            //ctrlLoad.btConfigremove.Visible = false;
                            ctrlLoad.lblWeb.Visible = false;
                            ctrlLoad.btWebAdd.Visible = false;
                            ctrlLoad.btWebRemove.Visible = false;
                            ctrlLoad.lblBinary.Visible = false;
                            ctrlLoad.btBinaryAdd.Visible = false;
                            ctrlLoad.btBinaryRemove.Visible = false;
                            ctrlLoad.labelWEB.Visible = false;
                            //ctrlLoad.labelCONFIG.Visible = false;
                            ctrlLoad.labelBIN.Visible = false;

                            for (int j = 0; j <= dsload.Tables[0].Rows.Count - 1; j++)
                            {
                                if (dsload.Tables[0].Rows[j].ItemArray[2].ToString().ToUpper() == "XSLT")
                                {
                                    ctrlLoad.lblConfig.Text = dsload.Tables[0].Rows[j].ItemArray[3].ToString();
                                    ctrlLoad.txtPropId_config.Text = dsload.Tables[0].Rows[j].ItemArray[0].ToString();
                                }

                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred while trying to Load Tabs." + ex.Message, "Job History", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);
            }
        }
        public void LoadPluginConfig()
        {
            try
            {
                _log.Info("calling LoadPluginConfig");
                jobscd = new JobConfigWinService.JobConfigPlugin();
                jobscd.JobId = Program.globaljobid;
                int capt_cnt = 0;
                int trans_cnt = 0;
                int load_cnt = 0;

                DataSet ds = jobref.GetJobPluginConfigWithJobId(jobscd);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                    {
                        if (ds.Tables[0].Rows[i].ItemArray[3].ToString() == "Capture")
                        {
                            TabPage tp = new TabPage("Capture");
                            int PluginId = Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[0].ToString());
                            jobscd.PluginId = PluginId;
                            string Title = "Capture- " + ds.Tables[0].Rows[i].ItemArray[2].ToString();
                            tp.Text = Title;

                            UserControlCapture ctrlcapture = new UserControlCapture();
                            ctrlcapture.Dock = System.Windows.Forms.DockStyle.Fill;
                            ctrlcapture.txtpluginId.Text = ds.Tables[0].Rows[i].ItemArray[0].ToString();
                            ctrlcapture.txtPulgin.Text = ds.Tables[0].Rows[i].ItemArray[4].ToString();
                            ctrlcapture.txtSeqNo.Text = ds.Tables[0].Rows[i].ItemArray[2].ToString();
                            Program.globalPlugin_ConfigId = Convert.ToInt32(ctrlcapture.txtpluginId.Text);

                            tp.Controls.Add(ctrlcapture);

                            DataSet dscapture = jobref.GetJobPluginPropWitPluginId(jobscd);

                            int row = dscapture.Tables[0].Rows.Count - 1;
                            if (dscapture.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k <= dscapture.Tables[0].Rows.Count - 1; k++)
                                {
                                    //if (dscapture.Tables[0].Rows[k].ItemArray[2].ToString() != "Binary" && dscapture.Tables[0].Rows[k].ItemArray[2].ToString() != "WebChange" && dscapture.Tables[0].Rows[k].ItemArray[2].ToString() != "XSLT")
                                    //{
                                    if (ctrlcapture.dataGrdViewProp.Rows.Count <= dscapture.Tables[0].Rows.Count)
                                    {

                                        ctrlcapture.dataGrdViewProp.Rows.Add();
                                        capt_cnt = capt_cnt + 1;
                                    }
                                    //}
                                }
                            }

                            for (int r = 0; r <= row; r++)
                            {
                                //for (int l = 0; l <= capt_cnt - 1; l++)
                                //{

                                //if (dscapture.Tables[0].Rows[r].ItemArray[2].ToString().ToUpper() != "BINARY" && dscapture.Tables[0].Rows[r].ItemArray[2].ToString().ToUpper() != "WEBCHANGE" && dscapture.Tables[0].Rows[r].ItemArray[2].ToString().ToUpper() != "XSLT")
                                //{
                                ctrlcapture.dataGrdViewProp.Rows[r].Cells[0].Value = dscapture.Tables[0].Rows[r].ItemArray[0];
                                ctrlcapture.dataGrdViewProp.Rows[r].Cells[1].Value = dscapture.Tables[0].Rows[r].ItemArray[1];
                                ctrlcapture.dataGrdViewProp.Rows[r].Cells[2].Value = dscapture.Tables[0].Rows[r].ItemArray[2];
                                ctrlcapture.dataGrdViewProp.Rows[r].Cells[3].Value = dscapture.Tables[0].Rows[r].ItemArray[3];
                                //}

                                //}
                            }
                            ctrlcapture.lblDataFilter.Visible = false;
                            ctrlcapture.labelDataFilter.Visible = false;
                            ctrlcapture.btDataFilter.Visible = false;

                            for (int j = 0; j <= dscapture.Tables[0].Rows.Count - 1; j++)
                            {
                                if (dscapture.Tables[0].Rows[j].ItemArray[2].ToString().ToUpper() == "XSLT" || dscapture.Tables[0].Rows[j].ItemArray[2].ToString().ToUpper() == "CODENAME" || dscapture.Tables[0].Rows[j].ItemArray[2].ToString().ToUpper() == "QUERY")
                                {
                                    ctrlcapture.lblConfig.Text = dscapture.Tables[0].Rows[j].ItemArray[3].ToString();
                                    ctrlcapture.txtPropId_config.Text = dscapture.Tables[0].Rows[j].ItemArray[0].ToString();

                                }
                                if (dscapture.Tables[0].Rows[j].ItemArray[2].ToString().ToUpper() == "WEBCHANGE")
                                {
                                    ctrlcapture.lblWeb.Text = dscapture.Tables[0].Rows[j].ItemArray[3].ToString();
                                    ctrlcapture.txtPropId_webchange.Text = dscapture.Tables[0].Rows[j].ItemArray[0].ToString();


                                }
                                if (dscapture.Tables[0].Rows[j].ItemArray[2].ToString().ToUpper() == "BINARY")
                                {
                                    ctrlcapture.lblBinary.Text = dscapture.Tables[0].Rows[j].ItemArray[3].ToString();
                                    ctrlcapture.txtPropId_binary.Text = dscapture.Tables[0].Rows[j].ItemArray[0].ToString();

                                }
                            }

                            tabControlPlugin.TabPages.Add(tp);

                            //RefreshCaptureTab();

                        }
                        else if (ds.Tables[0].Rows[i].ItemArray[3].ToString() == "Transform")
                        {

                            TabPage tp = new TabPage("Transform");
                            int PluginId = Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[0].ToString());
                            jobscd.PluginId = PluginId;
                            string Title = "Transform- " + ds.Tables[0].Rows[i].ItemArray[2].ToString();
                            tp.Text = Title;

                            UserControlCapture ctrltransform = new UserControlCapture();
                            ctrltransform.Dock = System.Windows.Forms.DockStyle.Fill;
                            ctrltransform.txtpluginId.Text = ds.Tables[0].Rows[i].ItemArray[0].ToString();
                            ctrltransform.txtPulgin.Text = ds.Tables[0].Rows[i].ItemArray[4].ToString();
                            ctrltransform.txtSeqNo.Text = ds.Tables[0].Rows[i].ItemArray[2].ToString();
                            tp.Controls.Add(ctrltransform);
                            DataSet dstranform = jobref.GetJobPluginPropWitPluginId(jobscd);
                            int row = dstranform.Tables[0].Rows.Count - 1;

                            if (dstranform.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k <= dstranform.Tables[0].Rows.Count - 1; k++)
                                {
                                    //if (dstranform.Tables[0].Rows[k].ItemArray[2].ToString().ToUpper() != "BINARY" && dstranform.Tables[0].Rows[k].ItemArray[2].ToString().ToUpper() != "WEBCHANGE" && dstranform.Tables[0].Rows[k].ItemArray[2].ToString().ToUpper() != "XSLT")
                                    //{
                                    if (ctrltransform.dataGrdViewProp.Rows.Count <= dstranform.Tables[0].Rows.Count)
                                    {

                                        ctrltransform.dataGrdViewProp.Rows.Add();
                                        trans_cnt = trans_cnt + 1;
                                    }
                                    //}
                                }
                            }
                            //for (int l = 0; l <= trans_cnt - 1; l++)
                            //{
                            for (int r = 0; r <= row; r++)
                            {
                                
                                ctrltransform.dataGrdViewProp.Rows[r].Cells[0].Value = dstranform.Tables[0].Rows[r].ItemArray[0];
                                ctrltransform.dataGrdViewProp.Rows[r].Cells[1].Value = dstranform.Tables[0].Rows[r].ItemArray[1];
                                ctrltransform.dataGrdViewProp.Rows[r].Cells[2].Value = dstranform.Tables[0].Rows[r].ItemArray[2];
                                ctrltransform.dataGrdViewProp.Rows[r].Cells[3].Value = dstranform.Tables[0].Rows[r].ItemArray[3];
                                //}

                            }
                            //}
                            //end
                            ctrltransform.lblWeb.Visible = false;
                            ctrltransform.btWebAdd.Visible = false;
                            ctrltransform.btWebRemove.Visible = false;
                            ctrltransform.labelWEB.Visible = false;

                            for (int j = 0; j <= dstranform.Tables[0].Rows.Count - 1; j++)
                            {
                                if (dstranform.Tables[0].Rows[j].ItemArray[2].ToString().ToUpper() == "BINARY")
                                {
                                    ctrltransform.lblBinary.Text = dstranform.Tables[0].Rows[j].ItemArray[3].ToString();
                                    ctrltransform.txtPropId_binary.Text = dstranform.Tables[0].Rows[j].ItemArray[0].ToString();


                                }
                                if (dstranform.Tables[0].Rows[j].ItemArray[2].ToString().ToUpper() == "XSLT" || dstranform.Tables[0].Rows[j].ItemArray[2].ToString().ToUpper() == "CODENAME" || dstranform.Tables[0].Rows[j].ItemArray[2].ToString().ToUpper() == "QUERY")
                                {
                                    ctrltransform.lblConfig.Text = dstranform.Tables[0].Rows[j].ItemArray[3].ToString();
                                    ctrltransform.txtPropId_config.Text = dstranform.Tables[0].Rows[j].ItemArray[0].ToString();
                                }
                               
                                if (dstranform.Tables[0].Rows[j].ItemArray[2].ToString().ToUpper() == "DATAFILTER")
                                {
                                    ctrltransform.lblDataFilter.Text = dstranform.Tables[0].Rows[j].ItemArray[3].ToString();
                                    ctrltransform.txtprop_datafilterId.Text = dstranform.Tables[0].Rows[j].ItemArray[0].ToString();


                                }
                                

                            }
                            tabControlPlugin.TabPages.Add(tp);
                            //RefreshTransformTab();


                        }
                        else if (ds.Tables[0].Rows[i].ItemArray[3].ToString() == "Load")
                        {
                            TabPage tp = new TabPage("Load");
                            int PluginId = Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[0].ToString());
                            jobscd.PluginId = PluginId;
                            string Title = "Load- " + ds.Tables[0].Rows[i].ItemArray[2].ToString();
                            tp.Text = Title;

                            UserControlCapture ctrlLoad = new UserControlCapture();
                            ctrlLoad.Dock = System.Windows.Forms.DockStyle.Fill;
                            ctrlLoad.txtpluginId.Text = ds.Tables[0].Rows[i].ItemArray[0].ToString();
                            ctrlLoad.txtPulgin.Text = ds.Tables[0].Rows[i].ItemArray[4].ToString();
                            ctrlLoad.txtSeqNo.Text = ds.Tables[0].Rows[i].ItemArray[2].ToString();
                            tp.Controls.Add(ctrlLoad);
                            DataSet dsload = jobref.GetJobPluginPropWitPluginId(jobscd);
                            int row = dsload.Tables[0].Rows.Count - 1;
                            if (dsload.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k <= dsload.Tables[0].Rows.Count - 1; k++)
                                {
                                    //if (dsload.Tables[0].Rows[k].ItemArray[2].ToString().ToUpper() != "BINARY" && dsload.Tables[0].Rows[k].ItemArray[2].ToString().ToUpper() != "WEBCHANGE" && dsload.Tables[0].Rows[k].ItemArray[2].ToString().ToUpper() != "XSLT")
                                    //{
                                    if (ctrlLoad.dataGrdViewProp.Rows.Count <= dsload.Tables[0].Rows.Count)
                                    {

                                        ctrlLoad.dataGrdViewProp.Rows.Add();
                                        load_cnt = load_cnt + 1;
                                    }
                                    //}
                                }
                            }

                            //for (int l = 0; l <= load_cnt - 1; l++)
                            //{
                            for (int r = 0; r <= row; r++)
                            {
                                //ctrltransform.dataGrdViewProp.Rows.Add();
                                //if (dsload.Tables[0].Rows[r].ItemArray[2].ToString() != "Binary" && dsload.Tables[0].Rows[r].ItemArray[2].ToString() != "Webchange" && dsload.Tables[0].Rows[r].ItemArray[2].ToString() != "XSLT")
                                //{
                                //ctrlLoad.dataGrdViewProp.Rows.Add();
                                ctrlLoad.dataGrdViewProp.Rows[r].Cells[0].Value = dsload.Tables[0].Rows[r].ItemArray[0];
                                ctrlLoad.dataGrdViewProp.Rows[r].Cells[1].Value = dsload.Tables[0].Rows[r].ItemArray[1];
                                ctrlLoad.dataGrdViewProp.Rows[r].Cells[2].Value = dsload.Tables[0].Rows[r].ItemArray[2];
                                ctrlLoad.dataGrdViewProp.Rows[r].Cells[3].Value = dsload.Tables[0].Rows[r].ItemArray[3];
                                //}
                            }
                            //}
                            //end
                            //ctrlLoad.lblConfig.Visible = false;
                            //ctrlLoad.btConfigAdd.Visible = false;
                            //ctrlLoad.btConfigremove.Visible = false;
                            ctrlLoad.lblWeb.Visible = false;
                            ctrlLoad.btWebAdd.Visible = false;
                            ctrlLoad.btWebRemove.Visible = false;
                            ctrlLoad.lblBinary.Visible = false;
                            ctrlLoad.btBinaryAdd.Visible = false;
                            ctrlLoad.btBinaryRemove.Visible = false;
                            ctrlLoad.labelWEB.Visible = false;
                            //ctrlLoad.labelCONFIG.Visible = false;
                            ctrlLoad.labelBIN.Visible = false;
                            ctrlLoad.lblDataFilter.Visible = false;
                            ctrlLoad.labelDataFilter.Visible = false;
                            ctrlLoad.btDataFilter.Visible = false;


                            for (int j = 0; j <= dsload.Tables[0].Rows.Count - 1; j++)
                            {
                                if (dsload.Tables[0].Rows[j].ItemArray[2].ToString().ToUpper() == "XSLT" || dsload.Tables[0].Rows[j].ItemArray[2].ToString().ToUpper() == "CODENAME" || dsload.Tables[0].Rows[j].ItemArray[2].ToString().ToUpper() == "QUERY")
                                {
                                    ctrlLoad.lblConfig.Text = dsload.Tables[0].Rows[j].ItemArray[3].ToString();
                                    ctrlLoad.txtPropId_config.Text = dsload.Tables[0].Rows[j].ItemArray[0].ToString();
                                }
                               
                                //RefreshLoadTab();
                            }
                            tabControlPlugin.TabPages.Add(tp);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred while trying to retrieve." +ex.Message, "Job Schedule", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);
            }
        }
        private void DoRowDoubleClick(GridView view, Point pt)
        {
            try
            {
                _log.Info("calling DoRowDoubleClick");
                GridHitInfo info = view.CalcHitInfo(pt);
                if (info.InRow || info.InRowCell)
                {
                    string statusId = grdViewHistory.GetFocusedDataRow()["Status_Id"].ToString();
                    frmJobLog jlog = new frmJobLog();
                    jlog.FillJobLog(statusId);
                    jlog.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred " + ex.Message, "Job Schedule", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);
            }
        }
        private void grdViewHistory_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                _log.Info("calling grdViewHistory_DoubleClick");
                GridView view = (GridView)sender;
                Point pt = view.GridControl.PointToClient(Control.MousePosition);
                DoRowDoubleClick(view, pt);
                

            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred while trying to double click." + ex.Message, "Job Schedule", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);
            }

        }

        public void LoadPropertydatagrid(int pluginId)
        {
            try
            {
                _log.Info("calling LoadPropertydatagrid");
                jobscd.PluginId = pluginId;
                
                UserControlCapture ctrlcapture = new UserControlCapture();
                DataSet dscapture = jobref.GetJobPluginPropWitPluginId(jobscd);

                ctrlcapture.dataGrdViewProp.DataSource = dscapture.Tables[0];

            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred while trying to retrieve." + ex.Message, "Job Schedule", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);
            }
        }

        private void grdViewHistory_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if ((e.Column.FieldName == "SCHEDULE_TIME") || (e.Column.FieldName == "START_TIME") || (e.Column.FieldName == "END_TIME"))
            {
                if (e.Value.ToString() != "")
                {
                    DateTime dt = Convert.ToDateTime(e.Value.ToString());
                    string sTime = dt.ToString("MM/dd/yyyy hh:mm:ss tt");

                    e.DisplayText = sTime;
                }
            }
        }

        private void btnAddMail_Click(object sender, EventArgs e)
        {
            try
            {
                _log.Info("calling btnAddMail_Click");
                jobscd = new JobConfigWinService.JobConfigPlugin();

                if ((cmbMailGroupName.SelectedItem != null) && (cmbMailGroupName.SelectedItem != "") && (cmbMailGrpStatus.SelectedItem != null) && (cmbMailGrpStatus.SelectedItem != ""))
                {
                    string groupname = cmbMailGroupName.SelectedItem.ToString();
                    string mailGrpStatus = cmbMailGrpStatus.SelectedItem.ToString();
                    jobscd.MailGroupName = groupname;
                    jobscd.JobId = Program.globaljobid;
                    jobscd.MailGroupStatus = mailGrpStatus;

                    int groupexists = jobref.CheckMailGroupWithJobId(jobscd);

                    if (groupexists < 1)
                    {

                        int inserted = jobref.InsertJobMailGroup(jobscd);
                        if (inserted >= 1)
                        {

                            MessageBox.Show("Group Added Successfully", "Mailing", MessageBoxButtons.OK, MessageBoxIcon.Information);


                        }
                        else
                        {
                            MessageBox.Show("An error occurred while trying to Add Mail Group." , "Mailing", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }

                    }
                    else
                    {
                        MessageBox.Show("Group Already Exists.", "Mailing", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                }
                else
                {

                }
          FillMailingGrid();

            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred while trying to Add Mail Group." + ex.Message, "Mailing", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);
            }

        }

        private void cmbGroupName_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void tabControlPlugin_Leave(object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                
                _log.Error(ex.Message, ex);
            }

        }

        private void btAdd_Click(object sender, EventArgs e)
        {
            try
            {
                _log.Info("calling btAdd_Click");
                if (Program.globaljobid != 0)
                {
                    if (cmbStatus.SelectedItem.ToString() != "" && cmbTriggerJobName.SelectedValue.ToString() != "")
                    {
                        jobscd.Status = cmbStatus.SelectedItem.ToString();
                        jobscd.Trigger_Job_Id = Convert.ToInt32(cmbTriggerJobName.SelectedValue.ToString());
                        jobscd.JobId = Program.globaljobid;
                        int iresult = jobref.InsertTrigger(jobscd);
                        if (iresult == 1)
                        {
                            MessageBox.Show("Record added successfully", "Trigger", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            RefreshTriggerGrid();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Please select Status / Job Name ", "Trigger", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
                else
                {
                    MessageBox.Show("Please save the Job first", "Trigger", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred while trying to Add Trigger.-"+ ex.Message, "Mailing", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);
            }
        }

        private void btDelete_Click(object sender, EventArgs e)
        {
            try
            {
                _log.Info("calling btDelete_Click");
                DialogResult dialogResult = MessageBox.Show("Are you sure, you want to delete ?", "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dialogResult == DialogResult.Yes)
                {
                    String strTrig_Id = gridViewTrigger.GetFocusedDataRow()["TRIG_ID"].ToString();
                    jobscd.Trig_Id = Convert.ToInt32(strTrig_Id);
                    int iresult = jobref.DeleteTrigger(jobscd);
                    if (iresult == 1)
                    {
                        MessageBox.Show("Record deleted successfully!", "Trigger", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        RefreshTriggerGrid();
                    }
                    else
                    {
                        MessageBox.Show("An error occurred while trying to delete ","Trigger", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred while trying to delete Trigger.-" + ex.Message, "Trigger", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);
            }

        }

        private void timerJobStatus_Tick(object sender, EventArgs e)
        {
            //jobscd.JobId = Program.globaljobid;
            //DataSet ds = jobref.GetJobStatus(jobscd);
            //grdcontrolHistory.DataSource = ds.Tables[0];
            //grdcontrolHistory.RefreshDataSource();
        }

        public void FillComboMailGroupStatus()
        {
            cmbMailGrpStatus.Items.Add("Job Completed Successfully");
            cmbMailGrpStatus.Items.Add("Job Failed");
            cmbMailGrpStatus.Items.Add("Stale Data on WebSite");
            cmbMailGrpStatus.Items.Add("Job completed successfully with some errors");
            cmbMailGrpStatus.Items.Add("All");
            cmbMailGrpStatus.Items.Add("New Series Found");
        }
        
        private void numDuration_KeyPress(object sender, KeyPressEventArgs e)
        {
            flagschedule = true;
        }

        private void numFrequency_KeyPress(object sender, KeyPressEventArgs e)
        {
            flagschedule = true;
        }

        private void tmEditStartTime_KeyPress(object sender, KeyPressEventArgs e)
        {
            flagschedule = true;
        }

        private void tmEditEndTime_KeyPress(object sender, KeyPressEventArgs e)
        {
            flagschedule = true;
        }

        private void cmbCalendar_SelectedIndexChanged(object sender, EventArgs e)
        {
            flagschedule = true;
        }

        private void rdbYes_CheckedChanged(object sender, EventArgs e)
        {
            if (flagactive == false)
                flagschedule = true;
        }

        private void rdbNo_CheckedChanged(object sender, EventArgs e)
        {
            if(flagactive==false)
                flagschedule = true;
        }

        private void btnRefreshHistory_Click(object sender, EventArgs e)
        {
            jobscd.JobId = Program.globaljobid;
            DataSet ds = jobref.GetJobStatus(jobscd);
            grdcontrolHistory.DataSource = ds.Tables[0];
            grdcontrolHistory.RefreshDataSource();
        }




    }
}
