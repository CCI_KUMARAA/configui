﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace CCI.MIP.JobPlugin.UI
{
    public partial class frmSettings : Form
    {
        private static readonly log4net.ILog _log = log4net.LogManager.GetLogger(typeof(frmSettings));
        JobConfigWinService.JobConfigServiceClient jobref = new JobConfigWinService.JobConfigServiceClient();
        JobConfigWinService.JobConfigPlugin jobscd = new JobConfigWinService.JobConfigPlugin();
        public frmSettings()
        {
            InitializeComponent();
        }

        private void btSearch_Click(object sender, EventArgs e)
        {
            try
            {
                _log.Info("calling btSearch_Click");
                if (txtSeriesname.Text != string.Empty)
                {
                    jobscd.SeriesName = txtSeriesname.Text;
                    DataSet ds = jobref.GetSeriesName(jobscd);
                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            int row = ds.Tables[0].Rows.Count - 1;

                            dtGridSeriesDetails.Rows.Clear();
                            dtGridSeriesDetails.Refresh();


                            for (int r = 0; r <= row; r++)
                            {
                                dtGridSeriesDetails.Rows.Add();
                                dtGridSeriesDetails.Rows[r].Cells[0].Value = ds.Tables[0].Rows[r].ItemArray[0];
                                dtGridSeriesDetails.Rows[r].Cells[1].Value = ds.Tables[0].Rows[r].ItemArray[1];

                            }
                            this.dtGridSeriesDetails.Sort(dtGridSeriesDetails.Columns[0], ListSortDirection.Ascending);
                        }
                        else
                        {
                            dtGridSeriesDetails.Rows.Clear();
                            dtGridSeriesDetails.Refresh();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred while searching." + ex.Message, "Series Name", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);
            }
        }

        private void dtGridSeriesDetails_SelectionChanged(object sender, EventArgs e)
        {
           
        }

        private void dtGridSeriesDetails_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                _log.Info("calling dtGridSeriesDetails_CellContentClick");
                if (e.RowIndex >= 0)
                {
                    DataGridViewRow row = this.dtGridSeriesDetails.Rows[e.RowIndex];
                    txtNewSeriesName.Text = row.Cells[1].Value.ToString();
                    jobscd.Series_Key = Convert.ToInt32( row.Cells[0].Value.ToString());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred while searching." + ex.Message, "Series Name", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);
            }
        }

        private void btSave_Click(object sender, EventArgs e)
        {
            try
            {
                _log.Info("calling btSave_Click");
                if (txtNewSeriesName.Text != string.Empty)
                {
                    jobscd.SeriesName = txtNewSeriesName.Text;
                    
                    int result = jobref.UpdateSeriesName(jobscd);
                    if (result == 1)
                    {
                        MessageBox.Show("Record Updated successfully!", "Series Name", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtNewSeriesName.Enabled = false;
                        btSave.Enabled = false;
                    }
                    else if(result==2)
                    {
                        MessageBox.Show("Series Name already exists!", "Series Name", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    MessageBox.Show("Please enter Series Name!", "Series Name", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
               
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred while searching." + ex.Message, "Series Name", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);
            }
        }
        
        private void dtGridSeriesDetails_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F12)
            {
                txtNewSeriesName.Enabled = true;
                btSave.Enabled = true;
            }
        }


     
    }
}
