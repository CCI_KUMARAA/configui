﻿namespace CCI.MIP.JobPlugin.UI
{
    partial class frmCopyJobName
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCopyJobName));
            this.txtNewJobName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btCopySave = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtNewJobName
            // 
            this.txtNewJobName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNewJobName.Location = new System.Drawing.Point(104, 30);
            this.txtNewJobName.Name = "txtNewJobName";
            this.txtNewJobName.Size = new System.Drawing.Size(294, 20);
            this.txtNewJobName.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label1.Location = new System.Drawing.Point(21, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "Job Name";
            // 
            // btCopySave
            // 
            this.btCopySave.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btCopySave.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btCopySave.Location = new System.Drawing.Point(310, 79);
            this.btCopySave.Name = "btCopySave";
            this.btCopySave.Size = new System.Drawing.Size(88, 26);
            this.btCopySave.TabIndex = 2;
            this.btCopySave.Text = "Copy";
            this.btCopySave.UseVisualStyleBackColor = false;
            this.btCopySave.Click += new System.EventHandler(this.btCopySave_Click);
            // 
            // frmCopyJobName
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(410, 117);
            this.Controls.Add(this.btCopySave);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtNewJobName);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmCopyJobName";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "New Job Name";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtNewJobName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btCopySave;
    }
}