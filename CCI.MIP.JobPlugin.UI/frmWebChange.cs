﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CCI.MIP.JobPlugin.UI
{
    public partial class frmWebChange : Form
    {
        private static readonly log4net.ILog _log = log4net.LogManager.GetLogger(typeof(frmWebChange));

        JobConfigWinService.JobConfigServiceClient jobref = new JobConfigWinService.JobConfigServiceClient();
        JobConfigWinService.JobConfigPlugin jobscd = new JobConfigWinService.JobConfigPlugin();
        public frmWebChange()
        {
            InitializeComponent();
        }

        private void btSave_Click(object sender, EventArgs e)
        {
            try
            {
                _log.Info("calling btSave_Click");
                jobscd = new JobConfigWinService.JobConfigPlugin();

                jobscd.PluginId = Convert.ToInt32(txtPluginid.Text);
                jobscd.PluginConfigId = Convert.ToInt32(txtPluginid.Text);
                jobscd.Jobname = txtJobName.Text;
                jobscd.JobLastUpdate = txtDate.Text;
                jobscd.Description = txtFileFilter.Text;
                jobscd.Previousvalue = "";
                jobscd.PropName = "WebChange";
                jobscd.PropValue = txtJobName.Text;

                int iresult = jobref.InsertJobUpdate(jobscd);
                int iresultplugin = jobref.InsertJobPluginProp(jobscd);
                if (iresult == 1 && iresultplugin==1 )
                {
                    MessageBox.Show("Record inserted successfully!", "Web Change", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred while trying to save." + ex.Message, "Web Change", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);

            }
        }

        private void btUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                _log.Info("calling btUpdate_Click");
                jobscd = new JobConfigWinService.JobConfigPlugin();

                jobscd.PluginId = Convert.ToInt32(txtPluginid.Text);
                jobscd.PluginConfigId = Convert.ToInt32(txtPluginid.Text);
                jobscd.PropId = Convert.ToInt32(txtWebChangeId.Text);
                jobscd.UpdateId = Convert.ToInt32(txtJobUpdateId.Text);
                jobscd.Jobname = txtJobName.Text;
                jobscd.JobLastUpdate = txtDate.Text;
                jobscd.Description = txtFileFilter.Text;
                jobscd.Previousvalue = "";
                jobscd.PropName = "WebChange";
                jobscd.PropValue = txtJobName.Text;

                int iresult = jobref.UpdateJobUpdate(jobscd);
                int iresultprop = jobref.UpdateJobPluginProp(jobscd);
                if (iresult == 1 && iresultprop==1)
                {
                    MessageBox.Show("Record Updated successfully!", "Web Change", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred while trying to update." + ex.Message, "Web Change", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);
            }
        }
    }
}
