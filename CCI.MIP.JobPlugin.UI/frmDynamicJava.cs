﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CCI.MIP.JobPlugin.UI
{
    public partial class frmDynamicJava : Form
    {
        private static readonly log4net.ILog _log = log4net.LogManager.GetLogger(typeof(frmDynamicJava));
        JobConfigWinService.JobConfigServiceClient jobref = new JobConfigWinService.JobConfigServiceClient();
        JobConfigWinService.JobConfigPlugin jobscd = new JobConfigWinService.JobConfigPlugin();
        public frmDynamicJava()
        {
            InitializeComponent();
        }

        private void btDynUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                jobscd = new JobConfigWinService.JobConfigPlugin();
                jobscd.PluginId = Convert.ToInt32(txtDynPluginId.Text);
                jobscd.PluginConfigId = Convert.ToInt32(txtDynPluginId.Text);
                jobscd.PropId = Convert.ToInt32(txtDynPropId_config.Text);
                jobscd.PropName = txtDynPropName.Text;
                jobscd.PropValue = txtDynGUID.Text;
                jobscd.ConfigGUIDkey = txtDynGUID.Text;
                jobscd.Config = txtDynScript.Text;
                jobscd.ConfigId = Convert.ToInt32(txtDynconfigID.Text);

                if (txtDynGUID.Text != "" && txtDynScript.Text != "")
                {
                    int iUpdateGUID = jobref.UpdateGUID(jobscd);
                    int iupdateprop = jobref.UpdateJobPluginProp(jobscd);
                    if (iUpdateGUID == 1 && iupdateprop == 1)
                    {
                        MessageBox.Show("Record Updated successfully!", "Configuration", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        //OnClear();
                        this.Close();


                    }
                }
                else
                {
                    if (txtDynGUID.Text == "")
                    {
                        MessageBox.Show("Please enter GUID", "Configuration", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else if (txtDynScript.Text == "")
                    {
                        MessageBox.Show("Please enter Script", "Configuration", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred while trying to update." +ex.Message, "Configuration", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);

            }
        }

        public void OnClear()
        {
            txtDynconfigID.Text = string.Empty;
            txtDynGUID.Text = string.Empty;
            txtDynScript.Text = string.Empty;
        }
        private void btDynNew_Click(object sender, EventArgs e)
        {


            OnClear();
            btDynUpdate.Visible = false;
            btDynAdd.Visible = true;

        }


        private void btDynAdd_Click(object sender, EventArgs e)
        {
            try
            {
                jobscd = new JobConfigWinService.JobConfigPlugin();
                UserControlCapture uc = new UserControlCapture();
                frmJobSchedule fm = new frmJobSchedule();


                if (txtDynGUID.Text != "" && txtDynScript.Text != "")
                {
                    jobscd.ConfigGUIDkey = txtDynGUID.Text;
                    jobscd.Config = txtDynScript.Text;
                    int iresult = jobref.InsertNewGUID(jobscd);
                    if (iresult == 1)
                    {
                        jobscd.PluginId = Convert.ToInt32(txtDynPluginId.Text);
                        jobscd.PluginConfigId = Convert.ToInt32(txtDynPluginId.Text); ;
                        jobscd.PropName = txtDynPropName.Text;
                        jobscd.PropValue = txtDynGUID.Text;
                        //int iresultprop = jobref.InsertJobPluginProp(jobscd);
                        int iresultprop = 0;
                        //DataSet ds = jobref.GetJobPluginPropWitPluginId(jobscd);

                        //if (ds.Tables.Count > 0)
                        //{
                        if (txtDynPropId_config.Text != "")
                        {
                            jobscd.PropId = Convert.ToInt32(txtDynPropId_config.Text);
                            iresultprop = jobref.UpdateJobPluginProp(jobscd);
                        }
                        else
                        {
                            iresultprop = jobref.InsertJobPluginProp(jobscd);
                        }
                        //}


                        if (iresultprop == 1)
                        {
                            MessageBox.Show("Record inserted successfully!", "Configuration", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            //OnClear();
                            this.Close();
                            //uc.lblConfig.Text = strGUID;
                        }
                    }
                }
                else
                {
                    if (txtDynGUID.Text == "")
                    {
                        MessageBox.Show("Please enter GUID", "Configuration", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else if (txtDynScript.Text == "")
                    {
                        MessageBox.Show("Please enter Script", "Configuration", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred while trying to save.-" + ex.Message, "Configuration", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);
            }
        }
    }
}
