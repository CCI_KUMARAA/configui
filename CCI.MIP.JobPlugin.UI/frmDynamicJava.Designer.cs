﻿namespace CCI.MIP.JobPlugin.UI
{
    partial class frmDynamicJava
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDynamicJava));
            this.groupBoxDyn = new System.Windows.Forms.GroupBox();
            this.txtDynPropName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtDynPropId_config = new System.Windows.Forms.TextBox();
            this.txtDynPluginId = new System.Windows.Forms.TextBox();
            this.txtDynconfigID = new System.Windows.Forms.TextBox();
            this.btDynNew = new System.Windows.Forms.Button();
            this.btDynClear = new System.Windows.Forms.Button();
            this.btDynUpdate = new System.Windows.Forms.Button();
            this.btDynAdd = new System.Windows.Forms.Button();
            this.btSearch = new System.Windows.Forms.Button();
            this.txtDynScript = new System.Windows.Forms.TextBox();
            this.txtDynGUID = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBoxDyn.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxDyn
            // 
            this.groupBoxDyn.Controls.Add(this.txtDynPropName);
            this.groupBoxDyn.Controls.Add(this.label3);
            this.groupBoxDyn.Controls.Add(this.txtDynPropId_config);
            this.groupBoxDyn.Controls.Add(this.txtDynPluginId);
            this.groupBoxDyn.Controls.Add(this.txtDynconfigID);
            this.groupBoxDyn.Controls.Add(this.btDynNew);
            this.groupBoxDyn.Controls.Add(this.btDynClear);
            this.groupBoxDyn.Controls.Add(this.btDynAdd);
            this.groupBoxDyn.Controls.Add(this.btSearch);
            this.groupBoxDyn.Controls.Add(this.txtDynScript);
            this.groupBoxDyn.Controls.Add(this.txtDynGUID);
            this.groupBoxDyn.Controls.Add(this.label2);
            this.groupBoxDyn.Controls.Add(this.label1);
            this.groupBoxDyn.Controls.Add(this.btDynUpdate);
            this.groupBoxDyn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxDyn.Location = new System.Drawing.Point(0, 0);
            this.groupBoxDyn.Name = "groupBoxDyn";
            this.groupBoxDyn.Size = new System.Drawing.Size(557, 548);
            this.groupBoxDyn.TabIndex = 1;
            this.groupBoxDyn.TabStop = false;
            // 
            // txtDynPropName
            // 
            this.txtDynPropName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDynPropName.Enabled = false;
            this.txtDynPropName.Location = new System.Drawing.Point(87, 33);
            this.txtDynPropName.Name = "txtDynPropName";
            this.txtDynPropName.Size = new System.Drawing.Size(82, 20);
            this.txtDynPropName.TabIndex = 0;
            this.txtDynPropName.Text = "JDYN";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 36);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "PROP NAME";
            // 
            // txtDynPropId_config
            // 
            this.txtDynPropId_config.Location = new System.Drawing.Point(496, 66);
            this.txtDynPropId_config.Name = "txtDynPropId_config";
            this.txtDynPropId_config.Size = new System.Drawing.Size(39, 20);
            this.txtDynPropId_config.TabIndex = 5;
            this.txtDynPropId_config.Visible = false;
            // 
            // txtDynPluginId
            // 
            this.txtDynPluginId.Location = new System.Drawing.Point(496, 45);
            this.txtDynPluginId.Name = "txtDynPluginId";
            this.txtDynPluginId.Size = new System.Drawing.Size(39, 20);
            this.txtDynPluginId.TabIndex = 5;
            this.txtDynPluginId.Visible = false;
            // 
            // txtDynconfigID
            // 
            this.txtDynconfigID.Location = new System.Drawing.Point(496, 19);
            this.txtDynconfigID.Name = "txtDynconfigID";
            this.txtDynconfigID.Size = new System.Drawing.Size(39, 20);
            this.txtDynconfigID.TabIndex = 5;
            this.txtDynconfigID.Visible = false;
            // 
            // btDynNew
            // 
            this.btDynNew.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btDynNew.Image = global::CCI.MIP.JobPlugin.UI.Properties.Resources.newtask_16x16;
            this.btDynNew.Location = new System.Drawing.Point(479, 89);
            this.btDynNew.Name = "btDynNew";
            this.btDynNew.Size = new System.Drawing.Size(69, 22);
            this.btDynNew.TabIndex = 4;
            this.btDynNew.Text = "New";
            this.btDynNew.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btDynNew.UseVisualStyleBackColor = true;
            this.btDynNew.Click += new System.EventHandler(this.btDynNew_Click);
            // 
            // btDynClear
            // 
            this.btDynClear.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btDynClear.Image = global::CCI.MIP.JobPlugin.UI.Properties.Resources.cancel_16x16;
            this.btDynClear.Location = new System.Drawing.Point(479, 145);
            this.btDynClear.Name = "btDynClear";
            this.btDynClear.Size = new System.Drawing.Size(69, 22);
            this.btDynClear.TabIndex = 4;
            this.btDynClear.Text = "Clear";
            this.btDynClear.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btDynClear.UseVisualStyleBackColor = true;
            this.btDynClear.Visible = false;
            // 
            // btDynUpdate
            // 
            this.btDynUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btDynUpdate.Image = global::CCI.MIP.JobPlugin.UI.Properties.Resources.save_16x16;
            this.btDynUpdate.Location = new System.Drawing.Point(479, 117);
            this.btDynUpdate.Name = "btDynUpdate";
            this.btDynUpdate.Size = new System.Drawing.Size(69, 22);
            this.btDynUpdate.TabIndex = 4;
            this.btDynUpdate.Text = "Update";
            this.btDynUpdate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btDynUpdate.UseVisualStyleBackColor = true;
            this.btDynUpdate.Visible = false;
            this.btDynUpdate.Click += new System.EventHandler(this.btDynUpdate_Click);
            // 
            // btDynAdd
            // 
            this.btDynAdd.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btDynAdd.Image = global::CCI.MIP.JobPlugin.UI.Properties.Resources.saveandnew_16x16;
            this.btDynAdd.Location = new System.Drawing.Point(479, 117);
            this.btDynAdd.Name = "btDynAdd";
            this.btDynAdd.Size = new System.Drawing.Size(69, 22);
            this.btDynAdd.TabIndex = 4;
            this.btDynAdd.Text = "Save";
            this.btDynAdd.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btDynAdd.UseVisualStyleBackColor = true;
            this.btDynAdd.Visible = false;
            this.btDynAdd.Click += new System.EventHandler(this.btDynAdd_Click);
            // 
            // btSearch
            // 
            this.btSearch.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btSearch.Image = global::CCI.MIP.JobPlugin.UI.Properties.Resources.zoom_16x16;
            this.btSearch.Location = new System.Drawing.Point(466, 17);
            this.btSearch.Name = "btSearch";
            this.btSearch.Size = new System.Drawing.Size(69, 22);
            this.btSearch.TabIndex = 3;
            this.btSearch.Text = "Search";
            this.btSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btSearch.UseVisualStyleBackColor = true;
            this.btSearch.Visible = false;
            // 
            // txtDynScript
            // 
            this.txtDynScript.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDynScript.Location = new System.Drawing.Point(87, 117);
            this.txtDynScript.Multiline = true;
            this.txtDynScript.Name = "txtDynScript";
            this.txtDynScript.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtDynScript.Size = new System.Drawing.Size(386, 412);
            this.txtDynScript.TabIndex = 2;
            // 
            // txtDynGUID
            // 
            this.txtDynGUID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDynGUID.Location = new System.Drawing.Point(87, 73);
            this.txtDynGUID.Name = "txtDynGUID";
            this.txtDynGUID.Size = new System.Drawing.Size(386, 20);
            this.txtDynGUID.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 117);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "SCRIPT";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 73);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "GUID";
            // 
            // frmDynamicJava
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(557, 548);
            this.Controls.Add(this.groupBoxDyn);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmDynamicJava";
            this.Text = "Dynamic Java";
            this.groupBoxDyn.ResumeLayout(false);
            this.groupBoxDyn.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxDyn;
        public System.Windows.Forms.TextBox txtDynPropName;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.TextBox txtDynPropId_config;
        public System.Windows.Forms.TextBox txtDynPluginId;
        public System.Windows.Forms.TextBox txtDynconfigID;
        public System.Windows.Forms.Button btDynNew;
        private System.Windows.Forms.Button btDynClear;
        public System.Windows.Forms.Button btDynUpdate;
        public System.Windows.Forms.Button btDynAdd;
        private System.Windows.Forms.Button btSearch;
        public System.Windows.Forms.TextBox txtDynScript;
        public System.Windows.Forms.TextBox txtDynGUID;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}