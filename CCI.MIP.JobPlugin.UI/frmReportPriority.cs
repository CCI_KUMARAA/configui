﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CCI.MIP.JobPlugin.UI
{
    public partial class frmReportPriority : Form
    {
        JobConfigWinService.JobConfigServiceClient jobref = new JobConfigWinService.JobConfigServiceClient();
        JobConfigWinService.JobConfigPlugin jobscd = new JobConfigWinService.JobConfigPlugin();
        private static readonly log4net.ILog _log = log4net.LogManager.GetLogger(typeof(frmReportPriority));

        public frmReportPriority()
        {
            InitializeComponent();
        }

        private void frmReportPriority_Load(object sender, EventArgs e)
        {
            RefreshGrid();
        }

        private void btUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                _log.Info("calling btUpdate_Click");
                if (txtReportName.Text != "" && txtPriority.Text != "" && txtReportKey.Text != "")
                {
                    jobscd.Report_Name = txtReportName.Text;
                    jobscd.Report_Priority = txtPriority.Text;
                    jobscd.Report_Description = txtDescription.Text;
                    jobscd.Report_key = Convert.ToInt32(txtReportKey.Text);
                    int iresult = jobref.UpdateReport(jobscd);
                    if (iresult == 1)
                    {
                        MessageBox.Show("Record Updated successfully.", "Product", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        RefreshGrid();
                        clearcontrol();
                        btSave.Visible = false;
                        btUpdate.Visible = true;
                    }
                    else
                    {
                        MessageBox.Show("Error occurs while updating.", "Product", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }

                }
                else
                {
                    MessageBox.Show("Please Select Report.", "Product", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred " + ex.Message, "Product", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);

            }
        }

        private void btSave_Click(object sender, EventArgs e)
        {
            try
            {
                _log.Info("calling btSave_Click");
                if (txtReportName.Text != "" && txtPriority.Text != "")
                {
                    jobscd.Report_Name = txtReportName.Text ;
                    jobscd.Report_Priority = txtPriority.Text;
                    jobscd.Report_Description = txtDescription.Text;
                    int iresult = jobref.InsertReport(jobscd);
                    if (iresult == 1)
                    {
                        MessageBox.Show("Record saved successfully.", "Report", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        RefreshGrid();
                        clearcontrol();
                        btSave.Visible = false;
                        btUpdate.Visible = true;
                    }
                    else
                    {
                        MessageBox.Show("Error occurs while saving.", "Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }

                }
                else
                {
                    MessageBox.Show("Please Enter Report & Priority.", "Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred " + ex.Message, "Report", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);

            }
        }

        private void RefreshGrid()
        {
            try
            {
                _log.Info("calling RefreshGrid");
                DataSet dsreport= jobref.GetReport();
                if (dsreport.Tables.Count > 0)
                {
                    if (dsreport.Tables[0].Rows.Count > 0)
                    {
                        Report_Key.FieldName = dsreport.Tables[0].Columns[0].ToString();
                        ReportName.FieldName = dsreport.Tables[0].Columns[1].ToString();
                        Description.FieldName = dsreport.Tables[0].Columns[2].ToString();
                        Priority.FieldName = dsreport.Tables[0].Columns[3].ToString();
                        grdconrolReport.DataSource = dsreport.Tables[0];
                        btSave.Visible = false;
                        btUpdate.Visible = true;

                    }

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred " + ex.Message, "Report", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);

            }
        }

        private void clearcontrol()
        {
            try
            {
                _log.Info("calling clearcontrol");
                txtReportKey.Text = "";
                txtReportName.Text = "";
                txtDescription.Text = "";
                txtPriority.Text = "";
                btSave.Visible = true;
                btUpdate.Visible = false;


            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred " + ex.Message, "Report", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);

            }
        }

        private void btNew_Click(object sender, EventArgs e)
        {
            clearcontrol();
        }

        private void grdViewReport_Click(object sender, EventArgs e)
        {
            txtReportKey.Text = (grdViewReport.GetFocusedDataRow()["Report_Key"].ToString());
            txtReportName.Text = (grdViewReport.GetFocusedDataRow()["Name"].ToString());
            txtPriority.Text = (grdViewReport.GetFocusedDataRow()["Priority"].ToString());
            txtDescription.Text = (grdViewReport.GetFocusedDataRow()["Description"].ToString());
        }
    }
}
