﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;

namespace CCI.MIP.JobPlugin.UI
{
    public partial class frmReplay : Form
    {
        private static readonly log4net.ILog _log = log4net.LogManager.GetLogger(typeof(frmReplay));
    
        JobConfigWinService.JobConfigServiceClient jobref = new JobConfigWinService.JobConfigServiceClient();
        JobConfigWinService.JobConfigPlugin jobscd = new JobConfigWinService.JobConfigPlugin();

        public frmReplay()
        {
            InitializeComponent();
        }

        private void btSelectFile_Click(object sender, EventArgs e)
        {
            try
            {
                _log.Info("calling btSelectFile_Click");
               // string targetPath = @"C:\mnt\marketintel\Replay\";
                //string targetPath = @"\\stmnetapp01\MarketIntelD02\Replay\";
                string targetPath = ConfigurationManager.AppSettings["FilePathReplay"].ToString();

                openReplayFileDialog.Filter = "XML Files (.xml)|*.xml|All Files (*.*)|*.*";
                openReplayFileDialog.FilterIndex = 1;
                openReplayFileDialog.Multiselect = true;

                DialogResult result = openReplayFileDialog.ShowDialog(); // Show the dialog.
                if (result == DialogResult.OK) // Test result.
                {
                    foreach (String file in openReplayFileDialog.FileNames)
                    {


                        txtSelectFile.Text = file ;
                        string sourceFile = txtSelectFile.Text;
                        
                        var sections = sourceFile.Split('\\');
                        var fileName = sections[sections.Length - 1];
                        string destFile = System.IO.Path.Combine(targetPath, fileName);

                        if (!System.IO.Directory.Exists(targetPath))
                        {
                            System.IO.Directory.CreateDirectory(targetPath);
                        }

                        // To copy a file to another location and  
                        // overwrite the destination file if it already exists.
                        System.IO.File.Copy(sourceFile, destFile, true);
                        

                    }
                    lblresult.Text = "File is uploaded to " + targetPath ;
                    jobscd.Jobname = "Replay";
                    int iresult = jobref.InsertJobStatus(jobscd);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred while opening the file." + ex.Message, "CSV Creation", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);
            }
        }
    }
}
