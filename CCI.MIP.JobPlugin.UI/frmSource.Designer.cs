﻿namespace CCI.MIP.JobPlugin.UI
{
    partial class frmSource
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btSave = new System.Windows.Forms.Button();
            this.txtDisplaySource = new System.Windows.Forms.TextBox();
            this.btNew = new System.Windows.Forms.Button();
            this.txtSourceKey = new System.Windows.Forms.TextBox();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.txtSourceName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btUpdate = new System.Windows.Forms.Button();
            this.grdcontrolSource = new DevExpress.XtraGrid.GridControl();
            this.grdViewSource = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.SOURCE_KEY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NAME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DISPLAY_SOURCE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DESCRIPTION = new DevExpress.XtraGrid.Columns.GridColumn();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdcontrolSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewSource)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btSave);
            this.groupBox1.Controls.Add(this.txtDisplaySource);
            this.groupBox1.Controls.Add(this.btNew);
            this.groupBox1.Controls.Add(this.txtSourceKey);
            this.groupBox1.Controls.Add(this.txtDescription);
            this.groupBox1.Controls.Add(this.txtSourceName);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btUpdate);
            this.groupBox1.Controls.Add(this.grdcontrolSource);
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(589, 404);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Source";
            // 
            // btSave
            // 
            this.btSave.BackColor = System.Drawing.Color.Transparent;
            this.btSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btSave.Image = global::CCI.MIP.JobPlugin.UI.Properties.Resources.save_16x16;
            this.btSave.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btSave.Location = new System.Drawing.Point(480, 356);
            this.btSave.Name = "btSave";
            this.btSave.Size = new System.Drawing.Size(85, 30);
            this.btSave.TabIndex = 18;
            this.btSave.Text = "Save";
            this.btSave.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btSave.UseVisualStyleBackColor = false;
            this.btSave.Visible = false;
            this.btSave.Click += new System.EventHandler(this.btSave_Click);
            // 
            // txtDisplaySource
            // 
            this.txtDisplaySource.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDisplaySource.Location = new System.Drawing.Point(152, 292);
            this.txtDisplaySource.Name = "txtDisplaySource";
            this.txtDisplaySource.Size = new System.Drawing.Size(218, 20);
            this.txtDisplaySource.TabIndex = 28;
            // 
            // btNew
            // 
            this.btNew.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btNew.Image = global::CCI.MIP.JobPlugin.UI.Properties.Resources.newtask_16x16;
            this.btNew.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btNew.Location = new System.Drawing.Point(490, 255);
            this.btNew.Name = "btNew";
            this.btNew.Size = new System.Drawing.Size(75, 23);
            this.btNew.TabIndex = 26;
            this.btNew.Text = "New";
            this.btNew.UseVisualStyleBackColor = true;
            this.btNew.Visible = false;
            this.btNew.Click += new System.EventHandler(this.btNew_Click);
            // 
            // txtSourceKey
            // 
            this.txtSourceKey.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSourceKey.Location = new System.Drawing.Point(394, 364);
            this.txtSourceKey.Name = "txtSourceKey";
            this.txtSourceKey.Size = new System.Drawing.Size(42, 20);
            this.txtSourceKey.TabIndex = 24;
            this.txtSourceKey.Visible = false;
            // 
            // txtDescription
            // 
            this.txtDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDescription.Location = new System.Drawing.Point(152, 326);
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(218, 58);
            this.txtDescription.TabIndex = 25;
            // 
            // txtSourceName
            // 
            this.txtSourceName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSourceName.Location = new System.Drawing.Point(152, 258);
            this.txtSourceName.Name = "txtSourceName";
            this.txtSourceName.Size = new System.Drawing.Size(218, 20);
            this.txtSourceName.TabIndex = 23;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(24, 326);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 16);
            this.label3.TabIndex = 20;
            this.label3.Text = "Description";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(24, 292);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(114, 16);
            this.label2.TabIndex = 21;
            this.label2.Text = "Display Source";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(24, 258);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 16);
            this.label1.TabIndex = 19;
            this.label1.Text = "Source Name";
            // 
            // btUpdate
            // 
            this.btUpdate.BackColor = System.Drawing.Color.Transparent;
            this.btUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btUpdate.Image = global::CCI.MIP.JobPlugin.UI.Properties.Resources.saveandnew_16x16;
            this.btUpdate.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btUpdate.Location = new System.Drawing.Point(480, 356);
            this.btUpdate.Name = "btUpdate";
            this.btUpdate.Size = new System.Drawing.Size(85, 30);
            this.btUpdate.TabIndex = 18;
            this.btUpdate.Text = "Update";
            this.btUpdate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btUpdate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btUpdate.UseVisualStyleBackColor = false;
            this.btUpdate.Visible = false;
            this.btUpdate.Click += new System.EventHandler(this.btUpdate_Click);
            // 
            // grdcontrolSource
            // 
            this.grdcontrolSource.Location = new System.Drawing.Point(24, 19);
            this.grdcontrolSource.MainView = this.grdViewSource;
            this.grdcontrolSource.Name = "grdcontrolSource";
            this.grdcontrolSource.Size = new System.Drawing.Size(538, 219);
            this.grdcontrolSource.TabIndex = 0;
            this.grdcontrolSource.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdViewSource});
            // 
            // grdViewSource
            // 
            this.grdViewSource.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.SOURCE_KEY,
            this.NAME,
            this.DISPLAY_SOURCE,
            this.DESCRIPTION});
            this.grdViewSource.GridControl = this.grdcontrolSource;
            this.grdViewSource.Name = "grdViewSource";
            this.grdViewSource.OptionsBehavior.Editable = false;
            this.grdViewSource.OptionsView.ShowGroupPanel = false;
            this.grdViewSource.Click += new System.EventHandler(this.grdViewSource_Click);
            // 
            // SOURCE_KEY
            // 
            this.SOURCE_KEY.Caption = "SOURCE_KEY";
            this.SOURCE_KEY.Name = "SOURCE_KEY";
            // 
            // NAME
            // 
            this.NAME.Caption = "Source Name";
            this.NAME.Name = "NAME";
            this.NAME.Visible = true;
            this.NAME.VisibleIndex = 0;
            // 
            // DISPLAY_SOURCE
            // 
            this.DISPLAY_SOURCE.Caption = "Display Source";
            this.DISPLAY_SOURCE.Name = "DISPLAY_SOURCE";
            this.DISPLAY_SOURCE.Visible = true;
            this.DISPLAY_SOURCE.VisibleIndex = 1;
            // 
            // DESCRIPTION
            // 
            this.DESCRIPTION.Caption = "Description";
            this.DESCRIPTION.Name = "DESCRIPTION";
            this.DESCRIPTION.Visible = true;
            this.DESCRIPTION.VisibleIndex = 2;
            // 
            // frmSource
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(589, 404);
            this.Controls.Add(this.groupBox1);
            this.Name = "frmSource";
            this.Text = "Source";
            this.Load += new System.EventHandler(this.frmSource_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdcontrolSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private DevExpress.XtraGrid.GridControl grdcontrolSource;
        private DevExpress.XtraGrid.Views.Grid.GridView grdViewSource;
        private DevExpress.XtraGrid.Columns.GridColumn SOURCE_KEY;
        private DevExpress.XtraGrid.Columns.GridColumn NAME;
        private DevExpress.XtraGrid.Columns.GridColumn DISPLAY_SOURCE;
        private DevExpress.XtraGrid.Columns.GridColumn DESCRIPTION;
        private System.Windows.Forms.Button btNew;
        private System.Windows.Forms.TextBox txtSourceKey;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.TextBox txtSourceName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btUpdate;
        private System.Windows.Forms.TextBox txtDisplaySource;
        private System.Windows.Forms.Button btSave;
    }
}