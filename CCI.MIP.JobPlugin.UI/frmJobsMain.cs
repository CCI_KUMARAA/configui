﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.ServiceModel.Configuration;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;

namespace CCI.MIP.JobPlugin.UI
{
    public partial class frmJobMain : Form
    {
        private static readonly log4net.ILog _log = log4net.LogManager.GetLogger(typeof(frmJobMain));
        public static JobConfigWinService.JobConfigPlugin objjobplugin = new JobConfigWinService.JobConfigPlugin();
         public static  JobConfigWinService.JobConfigServiceClient jobref = new JobConfigWinService.JobConfigServiceClient();
        public frmJobMain()
        {
            InitializeComponent();
        }
        public void frmJobMain_Load(object sender, EventArgs e)
        {
            Fillgrid();
            string[] strserver=new string[10];
            string strendpoint = ""; ;
            ClientSection clientSettings = ConfigurationManager.GetSection("system.serviceModel/client") as ClientSection;
            
            foreach (ChannelEndpointElement endpoint in clientSettings.Endpoints)
            {
                if (endpoint.Name != "")
                {
                   strendpoint = endpoint.Address.ToString();
                    break;
                }
            }
            strserver = strendpoint.Split(':');
            this.Text = "Configuration UI - Connected to " + strserver[1];
        }
        public void Fillgrid()
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
                        
            try
            {
                _log.Info("calling Fillgrid");
                jobref = new JobConfigWinService.JobConfigServiceClient();
                ds.Tables.Clear();
                ds = jobref.GetJobMaster();
                if (ds.Tables[0].Rows.Count > 0)                                                                                                                                                             
                {

                    JOB_ID.FieldName = ds.Tables[0].Columns[0].ToString();
                    JOB_NAME.FieldName = ds.Tables[0].Columns[1].ToString();
                    DESK.FieldName = ds.Tables[0].Columns[3].ToString();
                    CALENDAR.FieldName = ds.Tables[0].Columns[4].ToString();
                    START_TIME.FieldName = ds.Tables[0].Columns[5].ToString();
                    END_TIME.FieldName = ds.Tables[0].Columns[6].ToString();
                    DURATION.FieldName = ds.Tables[0].Columns[7].ToString();
                    FREQUENCY.FieldName = ds.Tables[0].Columns[8].ToString();
                    ACTIVE.FieldName = ds.Tables[0].Columns[9].ToString();
                    LAST_UPDATE_DATE.FieldName = ds.Tables[0].Columns[10].ToString();
                    gridControlJobs.DataSource = ds.Tables[0];
                }
           }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred while loading grid." + ex.Message, "Job Main", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);
            }
        }
        private void toolbtAdd_Click(object sender, EventArgs e)
        {
            
        }
        private void toolbtEdit_Click(object sender, EventArgs e)
        {
            Editschedule();
            
        }
        
        public void Editschedule()
        {
            try
            {
                _log.Info("calling Editschedule");
                int index = gridViewJobSchedule.GetFocusedDataSourceRowIndex();
                string jobname = gridViewJobSchedule.GetFocusedDataRow()["Job_Name"].ToString();
                objjobplugin.Jobname = jobname;
                Program.JobName = jobname;
                objjobplugin.JobId = Convert.ToInt32(gridViewJobSchedule.GetFocusedDataRow()["Job_Id"].ToString());
                Program.globaljobid = Convert.ToInt32(gridViewJobSchedule.GetFocusedDataRow()["Job_Id"].ToString());
                objjobplugin.Desk = gridViewJobSchedule.GetFocusedDataRow()["DESK"].ToString();
                objjobplugin.Active = gridViewJobSchedule.GetFocusedDataRow()["Active"].ToString();
                objjobplugin.Calendar = gridViewJobSchedule.GetFocusedDataRow()["Calendar"].ToString();
                objjobplugin.Frequency = Convert.ToInt32(gridViewJobSchedule.GetFocusedDataRow()["FREQUENCY"].ToString());
                objjobplugin.StartTime = Convert.ToDateTime(gridViewJobSchedule.GetFocusedDataRow()["START_TIME"].ToString());
                objjobplugin.EndTime = Convert.ToDateTime(gridViewJobSchedule.GetFocusedDataRow()["END_TIME"].ToString());
                objjobplugin.Duration = Convert.ToInt32(gridViewJobSchedule.GetFocusedDataRow()["DURATION"].ToString());
                objjobplugin.LastUpdateDate = System.DateTime.Now;
               
                    frmJobSchedule frmschedule = new frmJobSchedule(jobname, objjobplugin);
                    //frmschedule.txtJobName.Enabled = false;
                    frmschedule.ShowDialog();

                    Fillgrid();
                    gridViewJobSchedule.FocusedRowHandle=index;
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred while retriving job schedule." + ex.Message, "Job Schedule", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);
            }  
         }
        private void gridViewJobSchedule_DoubleClick(object sender, EventArgs e)
        {
            GridView view = (GridView)sender;
            Point pt = view.GridControl.PointToClient(Control.MousePosition);
            var info=DoRowDoubleClick(view, pt);
            
        }
        private GridHitInfo DoRowDoubleClick(GridView view, Point pt)
        {
            GridHitInfo info = view.CalcHitInfo(pt);
            if (info.InRow || info.InRowCell)
            {
                Editschedule();
                return info;
            }
            return null;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            Program.globaljobid = 0;
            using (frmJobSchedule frmschedule = new frmJobSchedule())
            {
                frmschedule.btSave.Visible = true;
                frmschedule.rdbNo.Checked=true;
                frmschedule.tmEditEndTime.EditValue = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 00);

                frmschedule.ShowDialog();
            }
            Fillgrid();
        }

        private void tsBtCSVcreation_Click(object sender, EventArgs e)
        {
            frmMetaDataCSV MetaCSV = new frmMetaDataCSV();
            MetaCSV.ShowDialog();
        }

        private void tsbtUploadNew_Click(object sender, EventArgs e)
        {

            frmUploadSeries upload = new frmUploadSeries();
            upload.ShowDialog();
        }

        private void tsbtReplay_Click(object sender, EventArgs e)
        {
            frmReplay replay = new frmReplay();
            replay.ShowDialog();
        }

        private void toolbtRunNow_Click(object sender, EventArgs e)
        {
            //frmRunNow runnow = new frmRunNow();
            //runnow.ShowDialog();
            try
            {
                _log.Info("calling toolbtRunNow_Click");
                string jobname = gridViewJobSchedule.GetFocusedDataRow()["Job_Name"].ToString();
                objjobplugin.Jobname = jobname;
                if (jobname != null || jobname!="")
                {
                    DialogResult dialogResult =MessageBox.Show("Are you sure to Run Now for -" + jobname , "Run Now", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dialogResult == DialogResult.Yes)
                    {
                        int iresult = jobref.InsertJobStatus(objjobplugin);
                        if (iresult < 0)
                        {
                            //MessageBox.Show("Record added sucessfully", "Run Now", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        }
                    }
                }
                else
                {
                    MessageBox.Show("Please select Job Name.", "Run Now", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred while trying to Load." + ex.Message, "Run Now", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);

            }
        }

        private void toolbtRefresh_Click(object sender, EventArgs e)
        {
            Fillgrid();
        }

        private void gridViewJobSchedule_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column.FieldName == "LAST_UPDATE_DATE") 
            {
                if (e.Value.ToString() != "")
                {
                    DateTime dt = Convert.ToDateTime(e.Value.ToString());
                    string sTime = dt.ToString("MM/dd/yyyy hh:mm:ss tt");

                    e.DisplayText = sTime;
                }
            }
            if ((e.Column.FieldName == "START_TIME") || (e.Column.FieldName == "END_TIME"))
            {
                if (e.Value.ToString() != "")
                {
                    DateTime dt = Convert.ToDateTime(e.Value.ToString());
                    string sTime = dt.ToString("hh:mm:ss tt");

                    e.DisplayText = sTime;
                }
            } 

        }

        private void toolStripBtMailGroup_Click(object sender, EventArgs e)
        {
           
            Program.globaljobid = Convert.ToInt32(gridViewJobSchedule.GetFocusedDataRow()["Job_Id"].ToString());
            frmMailingScreen mail = new frmMailingScreen();
            mail.ShowDialog();

            // to Do

        }

        private void toolbtcopy_Click(object sender, EventArgs e)
        {
            string newJobname = string.Empty;
            frmCopyJobName copyname = new frmCopyJobName();            
            copyname.ShowDialog();
            if (Program.NewJobName != "")
            {
                CopyJobs(Program.NewJobName);
            }
        }
        public void CopyJobs(string newJobName)
        {
            try
            {
                _log.Info("calling CopyJobs");
                objjobplugin = new JobConfigWinService.JobConfigPlugin();
                int previousJobId= Convert.ToInt32(gridViewJobSchedule.GetFocusedDataRow()["Job_Id"].ToString());
                string jobname = gridViewJobSchedule.GetFocusedDataRow()["Job_Name"].ToString();
                //string newJobName= "Copy of "+jobname ;
                objjobplugin.Jobname = newJobName;
                objjobplugin.Active = "NO";//gridViewJobSchedule.GetFocusedDataRow()["Active"].ToString();
                objjobplugin.Calendar = gridViewJobSchedule.GetFocusedDataRow()["Calendar"].ToString();
                objjobplugin.Frequency = Convert.ToInt32(gridViewJobSchedule.GetFocusedDataRow()["FREQUENCY"].ToString());
                objjobplugin.StartTime = Convert.ToDateTime(gridViewJobSchedule.GetFocusedDataRow()["START_TIME"].ToString());
                objjobplugin.EndTime = Convert.ToDateTime(gridViewJobSchedule.GetFocusedDataRow()["END_TIME"].ToString());
                objjobplugin.Duration = Convert.ToInt32(gridViewJobSchedule.GetFocusedDataRow()["DURATION"].ToString());
                objjobplugin.LastUpdateDate = System.DateTime.Now;
                int iresultmaster = jobref.InsertJobMaster(objjobplugin);
                if (iresultmaster == 1)
                {
                    int jobid = jobref.GetJobId(objjobplugin);
                    int NewJobId = jobid;
                    Program.globaljobid = jobid;
                    objjobplugin.JobId = jobid;
                    int iresultschedule = jobref.InsertJobSchedule(objjobplugin);
                    objjobplugin.JobId = previousJobId;
                    DataSet ds = jobref.GetMailingNameWithStatus(objjobplugin);
                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count>0)
                        {
                            for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                            {
                                objjobplugin.JobId=NewJobId;
                                objjobplugin.MailGroupName=ds.Tables[0].Rows[i].ItemArray[0].ToString();
                                objjobplugin.MailGroupStatus = ds.Tables[0].Rows[i].ItemArray[1].ToString();
                                int iresultmailgrp = jobref.InsertJobMailGroup(objjobplugin);
                            }
                        }
                    }
                    objjobplugin.JobId = previousJobId;
                    DataSet dspluginconfig = jobref.GetJobPluginConfigWithJobId(objjobplugin);
                    int[] arrpluginId=new int[1];
                    if (dspluginconfig.Tables.Count > 0)
                    {
                        if (dspluginconfig.Tables[0].Rows.Count > 0)
                        {
                            arrpluginId = new int[dspluginconfig.Tables[0].Rows.Count];
                            for (int i = 0; i <= dspluginconfig.Tables[0].Rows.Count - 1; i++)
                            {
                                objjobplugin.JobId = NewJobId;
                                objjobplugin.Seqnum = Convert.ToInt32(dspluginconfig.Tables[0].Rows[i].ItemArray[2].ToString());
                                objjobplugin.EngineType = (dspluginconfig.Tables[0].Rows[i].ItemArray[3].ToString());
                                objjobplugin.Plugin = (dspluginconfig.Tables[0].Rows[i].ItemArray[4].ToString());
                                int iresultpluginconfig = jobref.InsertJobPluginConfig(objjobplugin);
                                if (iresultpluginconfig == 1)
                                {
                                   
                                     arrpluginId[i]= Convert.ToInt32(dspluginconfig.Tables[0].Rows[i].ItemArray[0].ToString());
                                }
                            }
                            for (int k = 0; k <= arrpluginId.Length-1; k++)
                             {
                            
                                    objjobplugin.PluginId = arrpluginId[k];
                                    DataSet dspluginprop = jobref.GetJobPluginPropWitPluginId(objjobplugin);
                            
                                    objjobplugin.JobId = NewJobId;
                                    DataSet dsnewpluginconfig = jobref.GetJobPluginConfigWithJobId(objjobplugin);

                                    if (dspluginprop.Tables.Count > 0 )
                                    {
                                        if (dspluginprop.Tables[0].Rows.Count > 0 )
                                        {
                                            
                                                for (int j = 0; j <= dspluginprop.Tables[0].Rows.Count - 1; j++)
                                                {
                                                    //insert into jobplugin prop
                                                    objjobplugin.PluginConfigId = Convert.ToInt32(dsnewpluginconfig.Tables[0].Rows[k].ItemArray[0].ToString());
                                                    int seqnumber = Convert.ToInt32(dsnewpluginconfig.Tables[0].Rows[k].ItemArray[2].ToString());
                                                    string pluginname = dsnewpluginconfig.Tables[0].Rows[k].ItemArray[3].ToString();
                                                    string pluginLetter="";
                                                    if (pluginname == "Capture")
                                                    {
                                                        pluginLetter = "C";
                                                    }
                                                    else if (pluginname == "Transform")
                                                    {
                                                        pluginLetter = "T";
                                                    }
                                                    objjobplugin.PropName = (dspluginprop.Tables[0].Rows[j].ItemArray[2].ToString());
                                                    if (dspluginprop.Tables[0].Rows[j].ItemArray[2].ToString().ToUpper() == "BINARY")
                                                    {

                                                        objjobplugin.PropValue = "BIN_" + pluginLetter + "_" + seqnumber + "_" + newJobName;
                                                    }
                                                    else if (dspluginprop.Tables[0].Rows[j].ItemArray[2].ToString().ToUpper() == "WEBCHANGE")
                                                    {
                                                        objjobplugin.PropValue = "WC_" + pluginLetter + "_" + seqnumber + "_" + newJobName;
                                                    }
                                                    else
                                                    {
                                                        objjobplugin.PropValue = dspluginprop.Tables[0].Rows[j].ItemArray[3].ToString();
                                                    }
                                                    
                                                    int iresultprop = jobref.InsertJobPluginProp(objjobplugin);
                                                    //insert into jobupdate
                                                    if (dspluginprop.Tables[0].Rows[j].ItemArray[2].ToString().ToUpper() == "BINARY")
                                                    {
                                                       
                                                        objjobplugin.Jobname = String.Format("BIN_{0}_{1}_{2}", pluginLetter, seqnumber, newJobName);
                                                        objjobplugin.Description = "mnt/marketintel/SourceDocument/Temp.csv";
                                                        objjobplugin.Previousvalue = "mnt/marketintel/SourceDocument/Temp.csv";
                                                        objjobplugin.JobLastUpdate = System.DateTime.Now.ToString();
                                                        int iresultupdate = jobref.InsertJobUpdate(objjobplugin);
                                                    }
                                                    else if (dspluginprop.Tables[0].Rows[j].ItemArray[2].ToString().ToUpper() == "WEBCHANGE")
                                                    {
                                                        objjobplugin.Jobname = dspluginprop.Tables[0].Rows[j].ItemArray[3].ToString();
                                                        DataSet dsupdate = jobref.GetJobUpdatewitName(objjobplugin);
                                                        if (dsupdate.Tables[0].Rows.Count > 0)
                                                        {
                                                            for (int i = 0; i <= dsupdate.Tables[0].Rows.Count - 1; i++)
                                                            {
                                                                objjobplugin.Jobname = "WC_" + pluginLetter + "_" + seqnumber + "_" + newJobName; ;
                                                                objjobplugin.JobLastUpdate = dsupdate.Tables[0].Rows[i].ItemArray[2].ToString();
                                                                objjobplugin.Description = dsupdate.Tables[0].Rows[i].ItemArray[3].ToString();
                                                                objjobplugin.Previousvalue = dsupdate.Tables[0].Rows[i].ItemArray[4].ToString();
                                                                int iresultupdate = jobref.InsertJobUpdate(objjobplugin);

                                                            }
                                                        }
                                                    }
                                                   
                                                }
                                        }
                                    }
                             }
                        
                                
                            }                           

                        }
                    }
                //COmmented for JIRA MIP-114/115
                    //objjobplugin.Jobname = jobname;
                    //DataSet dsjobupdate = jobref.GetJobUpdatewitName(objjobplugin);
                    //if (dsjobupdate.Tables.Count > 0)
                    //{
                    //    if (dsjobupdate.Tables[0].Rows.Count > 0)
                    //    {
                    //        for (int i = 0; i <= dsjobupdate.Tables[0].Rows.Count - 1; i++)
                    //        {
                    //            objjobplugin.Jobname = newJobName;
                    //            objjobplugin.JobLastUpdate = dsjobupdate.Tables[0].Rows[i].ItemArray[2].ToString(); 
                    //            objjobplugin.Description = dsjobupdate.Tables[0].Rows[i].ItemArray[3].ToString();
                    //            objjobplugin.Previousvalue=dsjobupdate.Tables[0].Rows[i].ItemArray[4].ToString();
                    //            int iresultjobupdate = jobref.InsertJobUpdate(objjobplugin);
                    //        }
                    //    }
                    //}

                    Program.NewJobName = "";
                Fillgrid();
            }
            
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred while trying to copy." + ex.Message, "Copy Jobs", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);

            }
        }

        private void toolbtRerun_Click(object sender, EventArgs e)
        {
            try
            {
                _log.Info("calling toolbtRerun_Click");
                string jobname = gridViewJobSchedule.GetFocusedDataRow()["Job_Name"].ToString();
                int selectedjobid=Convert.ToInt32( gridViewJobSchedule.GetFocusedDataRow()["Job_Id"].ToString());
                objjobplugin.Jobname = jobname;
                string strpropname;
                int iresultupdate;
                objjobplugin.JobId=selectedjobid;
                if (jobname != null || jobname != "")
                {
                    DialogResult dialogResult = MessageBox.Show("Are you sure to ReRun -" + jobname, "ReRun", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dialogResult == DialogResult.Yes)
                    {                       
                       
                        iresultupdate = jobref.UpdateReRunBin(objjobplugin);
                        
                        iresultupdate = jobref.UpdateReRunWeb(objjobplugin);

                        int iresult = jobref.InsertJobStatus(objjobplugin);
                        
                    }
                }
                else
                {
                    MessageBox.Show("Please select Job Name.", "ReRun", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred while trying to Load." +ex.Message, "ReRun", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _log.Error(ex.Message, ex);

            }
        }

        private void toolStripbtAbout_Click(object sender, EventArgs e)
        {
            frmAbout fmabt = new frmAbout();
            fmabt.ShowDialog();
        }

        private void toolStripBtSettings_Click(object sender, EventArgs e)
        {
            frmSettings fmsetting = new frmSettings();
            fmsetting.ShowDialog();
        }

        private void updateSeriesNameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmSettings fmsetting = new frmSettings();
            fmsetting.ShowDialog();
        }

        private void addProductGroupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmProductGroup fmproduct = new frmProductGroup();
            fmproduct.ShowDialog();
        }

        private void reportPriorityToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmReportPriority fmreport = new frmReportPriority();
            fmreport.ShowDialog();
        }

        private void addSourceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmSource frSrc = new frmSource();
            frSrc.ShowDialog();
        }

        private void gridViewJobSchedule_CustomColumnSort(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnSortEventArgs e)
        {
            try
            {
                if (e.Column.FieldName == "START_TIME")
                {
                    DateTime dt1 = Convert.ToDateTime(gridViewJobSchedule.GetListSourceRowCellValue(e.ListSourceRowIndex1, "START_TIME"));
                    DateTime dt2 = Convert.ToDateTime(gridViewJobSchedule.GetListSourceRowCellValue(e.ListSourceRowIndex2, "START_TIME"));
                    e.Handled = true;
                    e.Result = System.Collections.Comparer.Default.Compare(dt1,dt2);
                }

                if (e.Column.FieldName == "END_TIME")
                {
                    DateTime dt1 = Convert.ToDateTime(gridViewJobSchedule.GetListSourceRowCellValue(e.ListSourceRowIndex1, "END_TIME"));
                    DateTime dt2 = Convert.ToDateTime(gridViewJobSchedule.GetListSourceRowCellValue(e.ListSourceRowIndex2, "END_TIME"));
                    e.Handled = true;
                    e.Result = System.Collections.Comparer.Default.Compare(dt1, dt2);
                }

            }
            catch (Exception ex)
            { 
            
            }
        }
    }
}
